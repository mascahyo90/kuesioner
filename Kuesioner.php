<?php

class Kuesioner extends CI_Controller {

	var $limit = 20;
	
	public function __construct() {
		parent::__construct();
        $this->load->model('survei/m_kuesioner', 'the_m');
        $this->load->library('sesfilter');
        $this->load->helper('filter');
        $this->load->helper('geturl');		
	}

	public function index() {
		redirect("survei/kuesioner/page");
	}

	public function page() {
        $this->otoritas->rule('R');
        $offset = $this->uri->segment(4, "0");
        $skpd = $this->session->userdata('id_skpd');

        $data["title_panel"] = "Data Survei";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Data Survei');

        $cekKegiatan = $this->the_m->cekDataKegiatan();
        if($cekKegiatan > 0) {
            $getAll = $this->the_m->getAllData($skpd, $this->limit, $offset)->result();
            $jumlah = $this->the_m->getAllData($skpd, "","")->num_rows();
            $data["table"] = $this->_generateTable($getAll);
            $data["jumlah"] = '<p style="margin:2px">Jumlah Data: <b>'.$jumlah.'</b></p>';            
            $data["style_link_survei"] = '';
            $data["notif1"] = 'style="display:none"';

            $curSurveiByTahun = $this->the_m->getCurrentSurverPerTahun($skpd, date('Y'))->num_rows();            

            if($curSurveiByTahun > 0) {                
                $data["style2"] = 'display:none';
                $data["style3"] = '';
            } else {
                $data["style2"] = '';
                $data["style3"] = 'display:none';
            }
        } else {
            $data["jumlah"] = '';
            $data["table"] = '';
            $data["style_link_survei"] = 'style="display:none"';
            $data["notif1"] = '';
        }
        $data["link_survei"] = site_url("survei/kuesioner/add_srv");
        $data["message"] = $this->_show_message();
        $this->layout->render('back', 'survei/kuesioner/index', $data);		
	}

    public function _generateTable($query) {
        $this->load->library('table');
        $tmpl = array('table_open' => '<table class="table table-bordered table-condensed">');
        $this->table->set_template($tmpl);
        $this->table->set_heading(
            array(
                array("data" => 'No', "style" => "width: 10px"),
                'Tahun',
                array("data" => 'Aksi', "style" => "width: 150px")
            )
        );
        $i = 1;
        foreach ($query as $row) {
            $this->table->add_row($i, $row->srv_tahun, array('data' => "
                <a title='Laporan' href='" . site_url("survei/kuesioner/laporan/" . $row->srv_tahun) . "' class='btn btn-xs btn-success'><i class='fa fa-sticky-note-o'></i></a>
                <a title='Survei Ulang' href='" . site_url("survei/kuesioner/add_srv_ulang/" . $row->srv_tahun) . "' class='btn btn-xs btn-info'><i class='fa fa-pencil'></i> Survei Ulang</a>
                ")
            );
            $i++;
        }
        if ($query == null) {
            $cell = array('data' => 'OPD anda belum sekalipun melakukan survei', 'style' => 'text-align:center;', 'colspan' => 3);
            $this->table->add_row($cell);
        }
        return $this->table->generate();
    }    

    public function add_srv() {
        $this->otoritas->rule('R');
        $skpd = $this->session->userdata('id_skpd');
        $tahun_aktif = date('Y');

        $data["title_panel"] = "Data Kuesioner";
        $data["sub_title_panel"] = $tahun_aktif;
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Kuesioner '.$tahun_aktif.'');

        $kegiatan = $this->the_m->getKegiatanBySkpd($skpd)->result();
        $data['tabel_survei'] = $this->_generateTableSurvei($kegiatan);
        $data["message"] = $this->_show_message();
        $data["skpd_id"] = $skpd;
        $data["tahun"] = $tahun_aktif;
        $this->layout->render('back', 'survei/kuesioner/create', $data);
    }

    public function _generateTableSurvei($query) {
        $variabel = $this->the_m->getVariabel()->result();
        $variabel_nil = $this->the_m->getVariabelNilai()->result();

        $html = '<table class="table table-bordered table-condesed">';
        $html .= '<tr>';
        $html .= '<th style="width: 4%">No</th><th style="width: 35%;">Kegiatan</th>';
        foreach ($variabel as $v) {
            $html .= '<th style="width: 300px;">Var '.$v->var_id.'</th>';
        }
        $html .= '</tr>';

        $no = 1;
        foreach ($query as $row) {
            $html .= '<tr>';
            $html .= '<td style="text-align:center">'.$no++.'</td>';
            $html .= '<td>'.$row->keg_nama.'</td>';
            foreach ($variabel as $val) {
                $html .= '<td>';
                $html .= '<select name="srv_nilai['.$row->keg_id.']['.$val->var_id.']" class="form-control input-sm"><option value="0"></option>';
                foreach ($variabel_nil as $v) {
                    $html .= '<option value="'.$v->nvar_nilai.'">'.$v->nvar_nama.'</option>';
                }
                $html .= '</select>';
                $html .= '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';
        return $html;
    }

    public function add_srv_ulang($tahun) {
        $this->otoritas->rule('R');
        $skpd = $this->session->userdata('id_skpd');
        $tahun = $tahun;

        $data["title_panel"] = "Data Kuesioner";
        $data["sub_title_panel"] = $tahun;
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Kuesioner '.$tahun.'');

        $kegiatan = $this->the_m->getKegiatanBySkpd($skpd)->result();
        $data['tabel_survei'] = $this->_generateTableSurveiUlang($kegiatan, $tahun);
        $data["message"] = $this->_show_message();
        $data["skpd_id"] = $skpd;
        $data["tahun"] = $tahun;
        $this->layout->render('back', 'survei/kuesioner/create_ulang', $data);
    }

    public function _generateTableSurveiUlang($query, $tahun) {
        $variabel = $this->the_m->getVariabel()->result();
        $variabel_nil = $this->the_m->getVariabelNilai()->result();

        $html = '<table class="table table-bordered table-condesed">';
        $html .= '<tr>';
        $html .= '<th style="width: 4%">No</th><th style="width: 35%;">Kegiatan</th>';
        foreach ($variabel as $v) {
            $html .= '<th style="width: 300px;">Var '.$v->var_id.'</th>';
        }
        $html .= '</tr>';

        $no = 1;
        foreach ($query as $row) {
            $html .= '<tr>';
            $html .= '<td style="text-align:center">'.$no++.'</td>';
            $html .= '<td>'.$row->keg_nama.'</td>';
            $cekKegId = $this->the_m->cekKegiatanId($row->keg_id, $tahun);
            if($cekKegId > 0) {
                $stle_dropdown = 'disabled="disabled"';
            } else {
                $stle_dropdown = '';
            }
            foreach ($variabel as $val) {
                $html .= '<td>';
                $html .= '<select '.$stle_dropdown.' name="srv_nilai['.$row->keg_id.']['.$val->var_id.']" class="form-control input-sm"><option value="0"></option>';
                foreach ($variabel_nil as $v) {
                    $html .= '<option value="'.$v->nvar_nilai.'">'.$v->nvar_nama.'</option>';
                }
                $html .= '</select>';
                $html .= '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';
        return $html;
    }    

    public function submit() {
        $this->otoritas->rule('C');

        $skpd_id = $this->input->post('srv_skpd_id');
        $tahun = $this->input->post('srv_tahun');
        $nilai = $this->input->post('srv_nilai');

        foreach ($nilai as $key => $value) {
            foreach ($value as $k => $v) {
                $nilBobot = $this->the_m->getBobotVariabel($k);
                $nilai_terbobot = $k * $nilBobot;
                $insert = array(
                    'srv_tahun' => $tahun,
                    'srv_skpd_id' => $skpd_id,
                    'srv_keg_id' => $key,
                    'srv_var_id' => $k,
                    'srv_nilai' => $v,
                    'srv_nilai_terbobot' => $nilai_terbobot
                );
                $q = $this->the_m->create($insert);
            }
        }

        if ($q) {
            $this->session->set_flashdata('success', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('error', $this->ion_auth->errors());
        }
        redirect("survei/kuesioner/page");        

    }

    public function laporan($tahun) {
        $this->otoritas->rule('R');
        $skpd = $this->session->userdata('id_skpd');

        $data["title_panel"] = "Laporan Kuesioner";
        $data["sub_title_panel"] = "Hasil";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Kuesioner', site_url('survei/kuesioner'));
        $this->breadcrumbs->add_crumb('Laporan');

        $getAll = $this->the_m->getLaporan($skpd, $tahun);
        // echo "<pre>";print_r($getAll);die;
        $data["tabel_laporan"] = $this->_generateTableLaporan($getAll);
        $this->layout->render('back', 'survei/kuesioner/laporan', $data);
    }

    public function _generateTableLaporan($query) {
        if(!empty($query)) {
            // $variabel = $this->the_m->getVariabel()->result();
            $html = '<table class="table table-responsive table-bordered table-condesed">';
            $html .= '<tr>';
            $html .= '<th style="width: 4%;vertical-align: middle" rowspan="2">No</th><th rowspan="2" style="width: 50%;vertical-align: middle">Kegiatan</th>';

            foreach($query as $kol) {
                for($i = 1; $i <= 10; $i++) {
                    if(array_key_exists("Var".$i, $kol)) {
                        $html .= '<th style="width: 70px;text-align: center">Var '.$i.'</th>';
                    }
                }
            }
            $html .= '<th rowspan="2" style="width: 6%;text-align: center;vertical-align: middle">Nilai</th>';
            $html .= '</tr>';
            // $html .= '<tr>';
            // foreach ($variabel as $v) {
            //     $html .= '<td style="text-align: center">0</td>';
            // }
            // $html .= '</tr>';
            // $no = 1;
            // foreach ($query as $row) {
            //     $html .= '<tr>';
            //     $html .= '<td>'.$no++.'</td>';
            //     $html .= '<td>'.$row->keg_nama.'</td>';
            //     $html .= '<td style="text-align: center">'.$row->q_nilai_1.'</td>';
            //     $html .= '<td style="text-align: center">'.$row->q_nilai_2.'</td>';
            //     $html .= '<td style="text-align: center">'.$row->q_nilai_3.'</td>';
            //     $html .= '<td style="text-align: center">'.$row->q_nilai_4.'</td>';
            //     $html .= '<td style="text-align: center">'.$row->q_nilai_5.'</td>';
            //     $html .= '<td style="text-align: center">'.round($row->total_nilai, 2).'</td>';
            //     $html .= '</tr>';
            // }
            $html .= '</table>';
            return $html;
        }
    }    

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}