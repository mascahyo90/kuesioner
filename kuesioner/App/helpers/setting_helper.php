<?php

/**
 * Description of Setting_helper
 *
 * @author White Code
 */
if (!function_exists('app_name')) {

    function app_name() {
        $ci = &get_instance();
        $ci->load->model('m_setting');
        $appName = $ci->m_setting->get_app_name();
        return $appName->hasil;
    }

}

if (!function_exists('app_nickname')) {

    function app_nickname() {
        $ci = &get_instance();
        $ci->load->model('m_setting');
        $appName = $ci->m_setting->get_app_nickname();
        return $appName->hasil;
    }

}

if (!function_exists('get_tahun_semester')) {

    function get_tahun_semester() {
        $ci = &get_instance();
        $ci->load->model('m_setting');
        $appName = $ci->m_setting->get_tahun_semester();
        return $appName->hasil;
    }

}

if (!function_exists('get_semester_aktif')) {

    function get_semester_aktif() {
        $ci = &get_instance();
        $ci->load->model('m_setting');
        $appName = $ci->m_setting->get_semester_aktif();
        return $appName->hasil;
    }

}

function tanggal_indonesia($tanggal) {
    $theDate = explode(",", date("w,d,n,Y", strtotime($tanggal)));
    $wday = $theDate[0];
    $hr = $theDate[1];
    $theMonth = $theDate[2];
    $theYear = $theDate[3];
    //Buat daftar nama bulan
    $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember");
    //Buat daftar nama hari dalam bahasa indonesia
    $hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
    $month = intval($theMonth) - 1;
    $days = $wday;
    $tg_angka = $hr;
    $year = $theYear;
    return $tg_angka . ' ' . $bulan[$month] . ' ' . $year;
}

function _pagination($total, $per_page, $url, $uri_segment) {
    $CI = & get_instance();
    $CI->load->library("pagination");
    $config['base_url'] = base_url() . $url;
    if ($_SERVER['QUERY_STRING'] != "") {
        $config['last_url'] = '?' . $_SERVER['QUERY_STRING'];
    }
    $config['uri_segment'] = $uri_segment;
    $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['next_link'] = 'Lanjut &rarr;';
    $config['next_tag_open'] = '<li class="next page">';
    $config['next_tag_close'] = '</li>';

    $config['prev_link'] = '&larr; Sebelum';
    $config['prev_tag_open'] = '<li class="prev page">';
    $config['prev_tag_close'] = '</li>';

    $config['first_link'] = '&laquo; Pertama';
    $config['first_tag_open'] = '<li class="prev page">';
    $config['first_tag_close'] = '</li>';

    $config['last_link'] = 'Terakhir &raquo;';
    $config['last_tag_open'] = '<li class="next page">';
    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li class="page">';
    $config['num_tag_close'] = '</li>';

    $config['num_links'] = 3;
    $config['total_rows'] = $total;
    $config['per_page'] = $per_page;

    $CI->pagination->initialize($config);
    return $CI->pagination->create_links();
}

function format_currency($n = '') {
    return ($n === '') ? '' : 'Rp. ' . number_format((float) $n, 2, '.', ',');
}