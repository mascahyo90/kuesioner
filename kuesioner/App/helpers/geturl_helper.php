<?php

function getVal($val) {
    $q = $_SERVER['QUERY_STRING'];
    if ($q <> "") {
        parse_str($q, $data);
        if (array_key_exists($val, $data)) {
            return $data[$val];
        } else {
            return "";
        }
    } else {
        return "";
    }
}

function ifValExist($val) {
    $q = $_SERVER['QUERY_STRING'];
    if ($q <> "") {
        parse_str($q, $data);
        if (array_key_exists($val, $data)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
