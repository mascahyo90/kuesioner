<?php
/**
 * Created by PhpStorm.
 * User: white
 * Date: 10/08/17
 * Time: 8:14
 */

class M_kuesioner extends CI_Model {

    public $table = 'survei';
    public $pk = 'srv_id';

    public function __construct() {
        parent::__construct();
    }

    function getAllData($skpd, $limit, $offset) {
        if ($limit <> "" && $offset <> "") {
            $limitOffset = "LIMIT $offset,$limit";
        } else {
            $limitOffset = "";
        }

        $q = $this->db->query("
            SELECT 
              srv_id,
              srv_tahun
            FROM
              $this->table 
            WHERE srv_skpd_id = '$skpd'
            GROUP BY srv_tahun
            $limitOffset
        ");
        return $q;
    }

    function create($insert) {
        $q = $this->db->replace($this->table, $insert);
        return $q;
    }

    function getKegiatanBySkpd($skpd) {
        $q = $this->db->get_where('kegiatan', array('keg_skpd_id'=> $skpd, 'keg_status_aktif'=> '1'));
        return $q;
    }

    function cekDataKegiatan() {
        $q = $this->db->get('kegiatan');
        return $q->num_rows();
    }

    function getVariabel() {
        $q = $this->db->get('variabel');
        return $q;
    }

    function getVariabelNilai() {
        $q = $this->db->get('variabel_nilai');
        return $q;
    }

    function getBobotVariabel($var) {
        $this->db->select('var_bobot');
        $q = $this->db->get_where('variabel', array('var_id' => $var));
        return $q->row()->var_bobot;
    }

    function getCurrentSurverPerTahun($id, $tahun) {
        $q = $this->db->get_where($this->table, array('srv_skpd_id'=>$id, 'srv_tahun'=>$tahun));
        return $q;
    }    

    function cekKegiatanId($keg, $tahun) {
        $q = $this->db->get_where($this->table, array('srv_keg_id' => $keg, 'srv_tahun' => $tahun));
        return $q->num_rows();
    }

    function cekDataPerTahun($tahun) {
        $q = $this->db->query("
            SELECT 
              COUNT(*) as total
            FROM
              $this->table 
            WHERE q_tahun = '$tahun'
        ");
        return $q->row();
    }

    function getLaporan($skpd, $tahun) {
        $q = $this->db->query("call rekap_survei(".$skpd.", ".$tahun.")");
        $res = $q->result();
        $q->next_result();
        $q->free_result();
        return $res;
    }

    // function getLaporan($skpd, $tahun) {
    //     $q = $this->db->query("

    //     ");
    //     return $q;
    // }
}