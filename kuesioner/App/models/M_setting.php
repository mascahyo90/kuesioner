<?php

/**
 * Description of M_setting
 *
 * @author White Code
 */
class M_setting extends CI_Model {

    public $table = 'setting';
    public $pk = 'set_atr';

    public function __construct() {
        parent::__construct();
    }

    function get_app_name() {
        $q = $this->db->query("
            SELECT 
              set_val AS hasil 
            FROM
              $this->table 
            WHERE $this->pk = 'sys_name'            
        ");
        return $q->row();
    }

    function get_app_nickname() {
        $q = $this->db->query("
            SELECT 
              set_val AS hasil 
            FROM
              $this->table 
            WHERE $this->pk = 'sys_nickname'            
        ");
        return $q->row();
    }
    
    function get_semester_aktif() {
        $q = $this->db->query("
            SELECT 
              smt_kode AS hasil 
            FROM
               ref_akm_semester
            WHERE smt_is_aktif = '1'            
        ");
        return $q->row();
    }
    
    function get_tahun_semester() {
        $q = $this->db->query("
            SELECT 
              CONCAT(smt_tahun, '/', smt_tahun + 1) AS hasil 
            FROM
               ref_akm_semester
            WHERE smt_is_aktif = '1'            
        ");
        return $q->row();
    }

    function get_all($limit, $offset) {
        if ($limit <> "" && $offset <> "") {
            $limit_offset = "LIMIT $offset,$limit";
        } else {
            $limit_offset = "";
        }

        $q = $this->db->query("
            SELECT 
              * 
            FROM
              $this->table 
            $limit_offset
        ");
        return $q;
    }

    function create($insert) {
        $q = $this->db->insert($this->table, $insert);
        return $q;
    }

    function get_data_by_id($id) {
        $q = $this->db->get_where($this->table, array($this->pk => $id));
        return $q;
    }

    public function update($id, $update) {
        $this->db->where($this->pk, $id);
        $q = $this->db->update($this->table, $update);
        return $q;
    }

    function delete($id) {
        $this->db->where($this->pk, $id);
        $q = $this->db->delete($this->table);
        return $q;
    }

    function getSkpd() {
        $this->db->select('skpd_id as id, skpd_nama as nama');
        $q = $this->db->get('skpd');
        return $q;
    }

}
