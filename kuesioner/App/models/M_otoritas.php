<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_otoritas
 *
 * @author white
 */
class M_otoritas extends CI_Model {

    function get_otoritas($userId) {
        $q = $this->db->query("
            SELECT DISTINCT 
              (a.mnu_id) AS id,
              a.mnu_menu AS module,
              a.mnu_link AS url,
              MAX(b.grpmnu_otoritas) AS rules 
            FROM
              menu a
              LEFT JOIN groups_menu b 
                ON a.mnu_id = b.grpmnu_mnu_id 
            WHERE b.grpmnu_group_id IN 
              (SELECT 
                group_id 
              FROM
                users_groups 
              WHERE user_id = $userId) 
            GROUP BY id            
        ");
        if ($q->num_rows() > 0) {
            $r = $q->result_array();
            $q->free_result();
        } else {
            $r = array();
        }
        return $r;
    }

    function get_otoritas_url($userId, $url) {
        $q = $this->db->query("
            SELECT DISTINCT 
              (a.grpmnu_mnu_id) AS id,
              b.mnu_menu AS module,
              b.mnu_link AS url,
              GROUP_CONCAT(a.grpmnu_otoritas SEPARATOR ', ') AS rules 
            FROM
              groups_menu a 
              LEFT JOIN menu b 
                ON a.grpmnu_mnu_id = b.mnu_id 
            WHERE a.grpmnu_group_id IN 
              (SELECT 
                group_id 
              FROM
                users_groups 
              WHERE user_id = $userId) 
              AND b.mnu_link = '$url' 
            GROUP BY id        
        ");
        if ($q->num_rows() > 0) {
            $r = $q->result_array();
            $q->free_result();
        } else {
            $r = array();
        }
        return $r;
    }

    function get_parent_sidebar($userId) {
        $q = $this->db->query("
            SELECT 
              * 
            FROM
              (SELECT DISTINCT 
                (b.mnu_mnukat_id) AS idkatmenu,
                b.mnu_icon,
                c.mnukat_menu AS katmenu,
                c.mnukat_icon
              FROM
                groups_menu a 
                JOIN menu b 
                  ON b.mnu_id = a.grpmnu_mnu_id 
                JOIN menu_kategori c 
                  ON c.mnukat_id = b.mnu_mnukat_id 
              WHERE a.grpmnu_group_id IN 
                (SELECT 
                  group_id 
                FROM
                  users_groups 
                WHERE user_id = $userId) 
                AND a.grpmnu_otoritas != '0000') AS q    
            GROUP BY katmenu
            #ORDER BY katmenu           
        ");
        return $q;
    }

    function get_child_sidebar($userId, $idKatMenu) {
        $q = $this->db->query("
            SELECT 
              * 
            FROM
              (SELECT DISTINCT 
                (a.grpmnu_mnu_id) AS idgrupmenu,
                b.mnu_mnukat_id AS idkatmenu,
                c.mnukat_menu AS katmenu,
                c.mnukat_icon,
                b.mnu_menu AS modul,
                b.mnu_link AS url,
                b.mnu_icon AS icon,
                b.mnu_order AS mnuorder 
              FROM
                groups_menu a 
                LEFT JOIN menu b 
                  ON a.grpmnu_mnu_id = b.mnu_id 
                LEFT JOIN menu_kategori c 
                  ON b.mnu_mnukat_id = c.mnukat_id 
              WHERE a.grpmnu_group_id IN 
                (SELECT 
                  group_id 
                FROM
                  users_groups 
                WHERE user_id = $userId) 
                AND a.grpmnu_otoritas != '0000') AS q 
            WHERE idkatmenu = $idKatMenu
            ORDER BY mnuorder           
        ");
        return $q;
    }

    function get_groups() {
        $q = $this->db->query("
            
        ");
        if ($q->num_rows() > 0) {
            $r = $q->result_array();
            $q->free_result();
        } else {
            $r = array();
        }
        return $r;

        $sql = 'SELECT groups.*, count(roles.id) AS jml_role
                FROM groups
                LEFT JOIN permissions ON permissions.group_id = groups.id
                LEFT JOIN roles ON roles.id = permissions.role_id
                GROUP BY groups.id';
    }

    function get_roles() {
        $sql = 'SELECT roles.id, roles.name, roles.url, roles.desc, roles_category.category FROM roles JOIN roles_category ON roles_category.id = roles.category_id ORDER BY category_id ASC';
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
        } else {
            $result = array();
        }
        return $result;
    }

    function create_role($dataInsert) {
        $query = $this->db->insert('roles', $dataInsert);
        return $query;
    }

    function get_role_by_id($id) {
        $sql = "SELECT * FROM roles WHERE id = ?";
        $query = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
        } else {
            $result = array();
        }
        return $result;
    }

    function update_role($id, $dataUpdate) {
        $this->db->where('id', $id);
        $update = $this->db->update('roles', $dataUpdate);
        return $update;
    }

    function get_roles_category() {
        $sql = 'SELECT * FROM roles_category ORDER BY category';
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
        } else {
            $result = array();
        }
        return $result;
    }

    function create_role_cat($dataInsert) {
        $query = $this->db->insert('roles_category', $dataInsert);
        return $query;
    }

    function get_role_cat_by($id) {
        $sql = "SELECT * FROM roles_category WHERE id = ?";
        $query = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
        } else {
            $result = array();
        }
        return $result;
    }

    function update_role_cat($id, $dataUpdate) {
        $this->db->where('id', $id);
        $update = $this->db->update('roles_category', $dataUpdate);
        return $update;
    }

    function get_permissions_by_group_id($id) {
        $sql = 'SELECT * FROM permissions WHERE group_id = ?';
        $query = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
        } else {
            $result = array();
        }
        return $result;
    }

    function update_group($id, $dataUpdate) {
        $this->db->where('id', $id);
        $update = $this->db->update('groups', $dataUpdate);
        return $update;
    }

    function delete_groups_role($id) {
        $this->db->where('group_id', $id);
        $delete = $this->db->delete('permissions');
        return $delete;
    }

    function insert_groups_role($dataInsert) {
        $query = $this->db->insert('permissions', $dataInsert);
        return $query;
    }

    function get_pegawai() {
        $sql = 'SELECT * FROM mr_pegawai 
            WHERE nip NOT IN (
                SELECT username FROM users
            )
            ORDER BY nm_pegawai';

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
        } else {
            $result = array();
        }
        return $result;
    }

    function get_pegawai_by($id) {
        $sql = 'SELECT * FROM mr_pegawai 
            WHERE id_mr_pegawai = ?';

        $query = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
        } else {
            $result = array();
        }
        return $result;
    }

    function update_user($dataUpdate) {
        $sql = 'UPDATE mr_pegawai SET email = ? WHERE nip = ?';
        return $this->db->query($sql, $dataUpdate);
    }

}
