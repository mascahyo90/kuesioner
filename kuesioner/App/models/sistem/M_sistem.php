<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_sistem
 *
 * @author My Computer
 */
class M_sistem extends CI_Model {

    function get_kategori_menu() {
        $q = $this->db->get('menu_kategori');
        return $q;
    }

    function create_katmenu($insert) {
        $q = $this->db->insert('menu_kategori', $insert);
        return $q;
    }

    function get_katmenu_by_id($id) {
        $q = $this->db->get_where('menu_kategori', array('mnukat_id' => $id));
        return $q;
    }

    function update_katmenu($id, $update) {
        $this->db->where('mnukat_id', $id);
        $q = $this->db->update('menu_kategori', $update);
        return $q;
    }

    function delete_katmenu($id) {
        $this->db->where('mnukat_id', $id);
        $q = $this->db->delete('menu_kategori');
        return $q;
    }

    function get_menu() {
        $q = $this->db->query("
            SELECT 
              mnu_id,
              mnukat_menu,
              mnu_mnukat_id,
              mnu_menu,
              mnu_link,
              mnu_deskripsi,
              mnu_order,
              mnu_icon 
            FROM
              menu 
              LEFT JOIN menu_kategori 
                ON mnu_mnukat_id = mnukat_id 
            ORDER BY mnu_mnukat_id            
        ");
        return $q;
    }

    function create_menu($insert) {
        $q = $this->db->insert('menu', $insert);
        return $q;
    }

    function get_menu_by_id($id) {
        $q = $this->db->get_where('menu', array('mnu_id' => $id));
        return $q;
    }

    function update_menu($id, $update) {
        $this->db->where('mnu_id', $id);
        $q = $this->db->update('menu', $update);
        return $q;
    }

    function delete_menu($id) {
        $this->db->where('mnu_id', $id);
        $q = $this->db->delete('menu');
        return $q;
    }

    function insert_to_grupmenu($insert_grups_menu) {
        $q = $this->db->insert('groups_menu', $insert_grups_menu);
        return $q;
    }

    function get_groupmenu_by_group_id($id) {
        $q = $this->db->get_where('groups_menu', array('grpmnu_group_id' => $id));
        return $q;
    }

    function update_group($id, $update) {
        $this->db->where('id', $id);
        $q = $this->db->update('groups', $update);
        return $q;
    }

    function delete_group_menu($id) {
        $this->db->where('grpmnu_group_id', $id);
        $q = $this->db->delete('groups_menu');
        return $q;
    }
    
    function delete_pengguna($id) {
        $this->db->where('id', $id);
        $q = $this->db->delete('users');
        return $q;
    }

}
