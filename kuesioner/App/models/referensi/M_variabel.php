<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_variabel
 *
 * @author My Computer
 */
class M_variabel extends CI_Model {

    public $table = 'variabel';
    public $pk = 'var_id';

    public function __construct() {
        parent::__construct();
    }

    function getAllData($nama, $limit, $offset) {
        if ($limit <> "" && $offset <> "") {
            $limitOffset = "LIMIT $offset,$limit";
        } else {
            $limitOffset = "";
        }
        
        if ($nama != "") {
            $filNama = "AND var_nama LIKE '%$nama%'";
        } else {
            $filNama = "";
        }

        $q = $this->db->query("
            SELECT 
              * 
            FROM
              $this->table 
            WHERE $this->pk IS NOT NULL 
              $filNama
            $limitOffset
        ");
        return $q;
    }

    function create($insert) {
        $q = $this->db->insert($this->table, $insert);
        return $q;
    }

    function getDataById($id) {
        $q = $this->db->get_where($this->table, array($this->pk => $id));
        return $q;
    }

    public function update($id, $update) {
        $this->db->where($this->pk, $id);
        $q = $this->db->update($this->table, $update);
        return $q;
    }

    function delete($id) {
        $this->db->where($this->pk, $id);
        $q = $this->db->delete($this->table);
        return $q;
    }

    function getJumlahBobot(){
        $this->db->select_sum('var_bobot');
        $q = $this->db->get($this->table);
        return $q;
    }

}
