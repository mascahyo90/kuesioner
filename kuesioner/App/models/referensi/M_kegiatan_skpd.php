<?php

/**
 * Description of M_kegiatan
 *
 * @author White Code
 */
class M_kegiatan_skpd extends CI_Model {

    public $table = 'kegiatan';
    public $pk = 'keg_id';

    public function __construct() {
        parent::__construct();
    }

    function getAllData($nama, $skpd, $limit, $offset) {
        if ($limit <> "" && $offset <> "") {
            $limitOffset = "LIMIT $offset,$limit";
        } else {
            $limitOffset = "";
        }
        
        if ($nama <> "") {
            $filNama = "AND keg_nama LIKE '%$nama%'";
        } else {
            $filNama = "";
        }

        $q = $this->db->query("
            SELECT 
              keg_id,
              keg_nama,
              skpd_nama,
              keg_tahun,
              IF(keg_status_aktif='0', 'Tidak Aktif', 'Aktif') as status_aktif
            FROM
              $this->table 
              LEFT JOIN skpd 
                ON keg_skpd_id = skpd_id 
            WHERE keg_skpd_id = '$skpd'
              $filNama 
            ORDER BY keg_tahun DESC, keg_id 
            $limitOffset
        ");
        return $q;
    }

    function create($insert) {
        $q = $this->db->insert($this->table, $insert);
        return $q;
    }

    function get_data_by_id($id) {
        $q = $this->db->get_where($this->table, array($this->pk => $id));
        return $q;
    }

    public function update($id, $update) {
        $this->db->where($this->pk, $id);
        $q = $this->db->update($this->table, $update);
        return $q;
    }

    function delete($id) {
        $this->db->where($this->pk, $id);
        $q = $this->db->delete($this->table);
        return $q;
    }

    function getSkpd() {
        $this->db->select('skpd_id as id, skpd_nama as nama');
        $q = $this->db->get('skpd');
        return $q;
    }

}
