<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_spkd
 *
 * @author My Computer
 */
class M_skpd extends CI_Model {

    public $table = 'skpd';
    public $pk = 'skpd_id';

    public function __construct() {
        parent::__construct();
    }

    function getAllData($nama) {
        if ($nama != "") {
            $filNama = "AND skpd_nama LIKE '%$nama%'";
        } else {
            $filNama = "";
        }

        $q = $this->db->query("
            SELECT 
              * 
            FROM
              $this->table 
            WHERE $this->pk IS NOT NULL 
              $filNama
        ");
        return $q;
    }

    function create($insert) {
        $q = $this->db->insert($this->table, $insert);
        return $q;
    }

    function getDataById($id) {
        $q = $this->db->get_where($this->table, array($this->pk => $id));
        return $q;
    }

    public function update($id, $update) {
        $this->db->where($this->pk, $id);
        $q = $this->db->update($this->table, $update);
        return $q;
    }

    function delete($id) {
        $this->db->where($this->pk, $id);
        $q = $this->db->delete($this->table);
        return $q;
    }

}
