<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_pendaftar
 *
 * @author My Computer
 */
class M_pendaftar extends CI_Model {

    public $table = 'pmb_mahasiswa_daftar';
    public $pk = 'mhsd_id';
    private $dbPis;

    public function __construct() {
        parent::__construct();
        $this->dbPis = $this->load->database('pis', TRUE);
    }

    function getAllData($nodaf, $noref, $gelombang, $prodi, $nama, $ujian, $daftar, $limit, $offset) {
        if ($limit <> "" && $offset <> "") {
            $limitOffset = "LIMIT $offset,$limit";
        } else {
            $limitOffset = "";
        }

        if ($nodaf <> "") {
            $filNodaf = "AND mhsd_nodaf = '$nodaf'";
        } else {
            $filNodaf = "";
        }

        if ($noref <> "") {
            $filNoref = "AND mhsd_noref = '$noref'";
        } else {
            $filNoref = "";
        }

        if ($nama != "") {
            $filNama = "AND mhsd_nama_lengkap LIKE '%$nama%'";
        } else {
            $filNama = "";
        }

        if ($ujian <> "") {
            $filUjian = "AND mhsd_status_ujian = '$ujian'";
        } else {
            $filUjian = "";
        }

        if ($gelombang <> "") {
            $filGelombang = "AND mhsd_gel_id = '$gelombang'";
        } else {
            $filGelombang = "";
        }

        if ($prodi <> "") {
            $filProdi = "AND mhsd_prodi_kode = '$prodi'";
        } else {
            $filProdi = "";
        }

        if ($daftar <> "") {
            $filDaftar = "AND mhsd_status_daftar = '$daftar'";
        } else {
            $filDaftar = "";
        }

        $q = $this->db->query("
            SELECT
              mhsd_id,
              mhsd_nodaf,
              mhsd_noref,
              mhsd_nama_lengkap,
              CONCAT(prodi_jenjang, ' - ', prodi_nama) AS prodi,
              mhsd_jk,
              mhsd_tgl_daftar,
              mhsd_status_ujian,
              mhsd_status_registrasi,
              mhsd_biaya_daftar
            FROM
              pmb_mahasiswa_daftar 
              LEFT JOIN ref_akm_program_studi 
                ON mhsd_prodi_kode = prodi_kode 
            WHERE $this->pk IS NOT NULL 
              $filNodaf
              $filNoref
              $filNama 
              $filUjian 
              $filGelombang
              $filProdi
              $filDaftar
            ORDER BY mhsd_tgl_daftar DESC
            $limitOffset
        ");
        return $q;
    }

    function getMaxNodafByProdi($prodi) {
        $this->db->select_max('mhsd_nodaf');
        $q = $this->db->get_where($this->table, array('mhsd_prodi_kode' => $prodi));
        return $q;
    }

    function create($insert) {
        $q = $this->db->insert($this->table, $insert);
        return $q;
    }

    function createMahasiswaToPis($insert) {
        $q = $this->dbPis->insert('ref_akm_mahasiswa', $insert);
        return $q;
    }

    function createAlamat($insert_alamat) {
        $q = $this->db->insert('pmb_mahasiswa_alamat', $insert_alamat);
        return $q;
    }

    function createUser($insert_user) {
        $q = $this->db->insert('users', $insert_user);
        return $q;
    }

    function createGroup($insert_group) {
        $q = $this->db->insert('users_groups', $insert_group);
        return $q;
    }

    function getDataById($id) {
        $q = $this->db->get_where($this->table, array($this->pk => $id));
        return $q;
    }

    function getDataAlamatById($id) {
        $q = $this->db->get_where('pmb_mahasiswa_alamat', array('mhsalmt_mhsd_id' => $id));
        return $q;
    }

    public function update($id, $update) {
        $this->db->where($this->pk, $id);
        $q = $this->db->update($this->table, $update);
        return $q;
    }

    public function updateAlamat($id, $update_alamat) {
        $this->db->where('mhsalmt_mhsd_id', $id);
        $q = $this->db->update('pmb_mahasiswa_alamat', $update_alamat);
        return $q;
    }

    public function updatePIS($id, $update) {
        $this->dbPis->where($this->pk, $id);
        $q = $this->dbPis->update('ref_akm_mahasiswa', $update);
        return $q;
    }

    function getDetailData($id) {
        $q = $this->db->query("
            SELECT 
              *
            FROM
              $this->table 
              LEFT JOIN ref_akm_gelombang 
                ON mhsd_gel_id = gel_id 
              LEFT JOIN ref_akm_jenis_gelombang 
                ON gel_jgel_id = jgel_id
              LEFT JOIN ref_akm_program_studi 
                ON mhsd_prodi_kode = prodi_kode                
              LEFT JOIN ref_akm_semester 
                ON gel_smt_kode = smt_kode 
              LEFT JOIN ref_umum_agama 
                ON mhsd_agm_id = agm_id 
              LEFT JOIN ref_umum_jenis_smta 
                ON mhsd_jsmta_id = jsmta_id 
              LEFT JOIN ref_umum_jurusan_smta 
                ON mhsd_jursmta_id = jursmta_id 
            WHERE $this->pk = '$id'            
        ");
        return $q;
    }

    function getDetailAlamat($id) {
        $q = $this->db->query("
            SELECT 
              * 
            FROM
              pmb_mahasiswa_alamat 
              LEFT JOIN ref_umum_kota 
                ON mhsalmt_kota_kode = kota_kode 
              LEFT JOIN ref_umum_propinsi 
                ON mhsalmt_kode_propinsi = kode_propinsi 
            WHERE mhsalmt_mhsd_id = '$id'            
        ");
        return $q;
    }

    function getSumberInformasiByID($id) {
        $q = $this->db->query("
            SELECT 
              sinfo_id,
              sinfo_nama
            FROM
              ref_umum_sumber_informasi  
            WHERE sinfo_id IN($id)            
        ");
        return $q;
    }

    function multiDelete($id) {
        if ($id <> "") {
            $filterId = "WHERE $this->pk IN ($id)";
        } else {
            $filterId = "";
        }
        $this->db->query("
            DELETE FROM $this->table
            $filterId
            ");
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function printKartuPendaftaran($id) {
        $q = $this->db->query("
            SELECT 
              mhsd_nodaf,
              mhsd_noref,
              mhsd_kelas,
              mhsd_tgl_daftar,
              mhsd_nama_lengkap,
              mhsd_tgl_ujian,
              jgel_nama,
              CONCAT(prodi_jenjang, ' - ', prodi_nama) AS prodi,
              CONCAT(smt_tahun, '/', smt_tahun + 1) AS ta,
              mhsd_foto
            FROM
              pmb_mahasiswa_daftar 
              LEFT JOIN ref_akm_program_studi 
                ON mhsd_prodi_kode = prodi_kode 
              LEFT JOIN ref_akm_gelombang 
                ON mhsd_gel_id = gel_id 
              LEFT JOIN ref_akm_jenis_gelombang 
                ON gel_jgel_id = jgel_id 
              LEFT JOIN ref_akm_semester 
                ON gel_smt_kode = smt_kode 
            WHERE $this->pk = '$id'
        ");
        return $q;
    }

    function getSemester() {
        $q = $this->db->query("
            SELECT 
              smt_kode AS id,
              CONCAT(smt_tahun, '/', smt_tahun + 1) AS semester,
              IF(
                RIGHT(smt_kode, 1) = '1',
                'Ganjil',
                IF(RIGHT(smt_kode, 1) = '2', 'Genap', '')
              ) AS periode
            FROM
              ref_akm_semester 
            ORDER BY smt_kode DESC                
        ");
        return $q;
    }

    function getGelombang($id) {
        $q = $this->db->query("
            SELECT 
              gel_id AS id,
              jgel_nama AS nama 
            FROM
              ref_akm_gelombang 
              LEFT JOIN ref_akm_jenis_gelombang 
                ON gel_jgel_id = jgel_id 
            WHERE gel_smt_kode = '$id' 
            ORDER BY gel_id DESC                
        ");
        return $q;
    }

    function getGelombangAktif() {
        $q = $this->db->query("
            SELECT 
              gel_id AS id,
              jgel_nama AS nama 
            FROM
              ref_akm_gelombang 
              LEFT JOIN ref_akm_jenis_gelombang 
                ON gel_jgel_id = jgel_id 
            WHERE gel_smt_kode = (SELECT smt_kode FROM ref_akm_semester WHERE smt_is_aktif = '1') 
            ORDER BY gel_id DESC                
        ");
        return $q;
    }

    function getProdi() {
        $this->db->select("prodi_kode as id, CONCAT(prodi_jenjang,' - ',prodi_nama) AS nama");
        $q = $this->db->get('ref_akm_program_studi');
        return $q;
    }

    function getAgama() {
        $this->db->select("agm_id as id, agm_nama AS nama");
        $q = $this->db->get('ref_umum_agama');
        return $q;
    }

    function getJenisSmta() {
        $this->db->select("jsmta_id as id, jsmta_nama AS nama");
        $q = $this->db->get('ref_umum_jenis_smta');
        return $q;
    }

    function getJurusanSmta($id) {
        $this->db->select("jursmta_id as id, jursmta_nama AS nama");
        $q = $this->db->get_where('ref_umum_jurusan_smta', array('jursmta_jsmta_id' => $id));
        return $q;
    }

    function getPropinsi() {
        $this->db->select("kode_propinsi as id, nama_propinsi AS nama");
        $q = $this->db->get('ref_umum_propinsi');
        return $q;
    }

    function getKota($id) {
        $this->db->select("kota_kode as id, kota_nama AS nama");
        $q = $this->db->get_where('ref_umum_kota', array('kota_kode_propinsi' => $id));
        return $q;
    }

    function getSumberInformasi() {
        $this->db->select("sinfo_id as id, sinfo_nama AS nama");
        $q = $this->db->get('ref_umum_sumber_informasi');
        return $q;
    }

    function getCurrentStatusLulus($id) {
        $this->db->select("mhsd_id as id, mhsd_status_ujian as status");
        $q = $this->db->get_where($this->table, array($this->pk => $id));
        return $q;
    }

    function getCurrentStatusRegistrasi($id) {
        $this->db->select("mhsd_id as id, mhsd_status_registrasi as status");
        $q = $this->db->get_where($this->table, array($this->pk => $id));
        return $q;
    }

    function getBiayaPendaftaranMhs($id) {
        $this->db->select("mhsd_id as id, mhsd_biaya_daftar as nominal");
        $q = $this->db->get_where($this->table, array($this->pk => $id));
        return $q;
    }

}
