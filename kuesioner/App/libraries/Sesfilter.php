<?php

/**
 * Description of Sesfilter
 *
 * @author teguholica, rewrite by white code
 */
class Sesfilter {

    var $ci;
    var $sesName = "sesFilter"; //default nama session filter
    var $isLock = false; //mematikan fungsi lock page

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function setSesFilterName($name, $lock = null) {
        $this->sesName = $name;
        if ($this->ci->session->userdata("sesFilterLock") != $lock && $lock != null) {
            $this->ci->session->unset_userdata($this->sesName);
            $this->ci->session->set_userdata("sesFilterLock", $lock);
            $this->isLock = true;
        }
    }

    public function setFilter($var, $val) {
        if ($this->ci->session->userdata($this->sesName) == "") {
            $this->ci->session->set_userdata($this->sesName, array($var => $val));
        } else {
            $arrSes = $this->ci->session->userdata($this->sesName);
            $arrSes[$var] = $val;
            $this->ci->session->set_userdata($this->sesName, $arrSes);
        }
    }

    public function setFilterFromPost($var, $post) {
        $val = $this->ci->input->post($post);
        if ($this->ci->session->userdata($this->sesName) == "") {
            $this->ci->session->set_userdata($this->sesName, array($var => $val));
        } else {
            $arrSes = $this->ci->session->userdata($this->sesName);
            $arrSes[$var] = $val;
            $this->ci->session->set_userdata($this->sesName, $arrSes);
        }
    }

    public function getFilter($var) {
        if ($this->ci->session->userdata($this->sesName) == "") {
            return "";
        } else {
            $arrSes = $this->ci->session->userdata($this->sesName);
            if (array_key_exists($var, $arrSes)) {
                $r = $arrSes[$var];
                if ($r == "all") {
                    $r = "";
                }
                return $r;
            } else {
                return "";
            }
        }
    }

    public function isFilterNull($var) {
        if ($this->ci->session->userdata($this->sesName) == "") {
            return false;
        } else {
            $arrSes = $this->ci->session->userdata($this->sesName);
            if (array_key_exists($var, $arrSes)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function resetFilter() {
        $this->ci->session->unset_userdata($this->sesName);
    }

}
