<?php

/**
 * author white code
 */
class MY_Form_validation extends CI_Form_validation {

    function __construct($rule = array()) {
        parent::__construct($rule);
        $this->set_error_delimiters('', '<br />');
        $this->set_message('required', 'Kolom <b>%s</b> Harus Diisi');
        $this->set_message('valid_email', 'Alamat Email Tidak Valid');
        $this->set_message('is_natural', 'Kolom <b>%s</b> Hanya Boleh Berisi Angka Positif');
        $this->set_message('alpha_dash', 'Kolom <b>%s</b> Hanya Boleh Berisi Alfanumerik,karakter,garis bawah, dashed');
        $this->set_message('decimal', 'Kolom <b>%s</b> Hanya Boleh Berisi Angka Desimal');
    }

}

?>