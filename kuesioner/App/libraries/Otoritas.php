<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Otoritas
 *
 * @author white
 * @ref Khoiruddin
 */
class Otoritas {

    private $CI;

    public function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->library('ion_auth');
        $this->CI->load->model('m_otoritas');
    }

    public function multi_in_array($value, $arr2ay) {
        foreach ($arr2ay AS $item) {
            if (!is_array($item)) {
                if ($item == $value) {
                    return true;
                }
                continue;
            }

            if (in_array($value, $item)) {
                return true;
            } else if ($this->multi_in_array($value, $item)) {
                return true;
            }
        }
        return false;
    }

    protected function array_multi_sum($arr2ay, $property) {
        $total = '';
        foreach ($arr2ay as $key => $value) {
            if (is_array($value)) {
                $total += $this->array_multi_sum($value, $property);
            } else if ($key == $property) {
                $total += $value;
            }
        }
        return $total;
    }

    public function rule($param = '') {
        $url = $this->CI->security->xss_clean($this->CI->uri->segment(1) . "/" . $this->CI->uri->segment(2));
        $user = $this->CI->ion_auth->user()->row();
        if (!$this->CI->ion_auth->logged_in()) {
//            $userId = '';
            redirect('auth/login', 'refresh');
        } else {
            $userId = $user->id;
        }
        $otoRules = $this->CI->m_otoritas->get_otoritas($userId);
        if (!$this->CI->ion_auth->logged_in()) {
            //jika user belum login
            redirect('auth/login', 'refresh');
        } elseif (!$this->multi_in_array($url, $otoRules)) { //jika tidak punya hak/authority
            echo "tidak punya akses";
            die();
//            show_404();
        } else {
            $otoUrl = $this->CI->m_otoritas->get_otoritas_url($userId, $url);
            //menjadikan field user_role array CRUD
            foreach ($otoUrl as $row) {
                //user rule dalam per role, dipisahkan dengan koma
                $userRule = $row['rules'];
            }
            //menjadikan array 2 dimensi per role
            $arr2ayMulti = array_map(
                    function ($_) {
                return str_split($_);
            }, explode(',', $userRule)
            );
            //menjumlahkan array multi,
            //array ke 0 / Create
            $total1 = $this->array_multi_sum($arr2ayMulti, '0');
            //array ke 1 / Read
            $total2 = $this->array_multi_sum($arr2ayMulti, '1');
            //array ke 2 / Update
            $total3 = $this->array_multi_sum($arr2ayMulti, '2');
            //array ke 3 / Delete
            $total4 = $this->array_multi_sum($arr2ayMulti, '3');

            //logical yang simple, pahamlah kamu
            if ($total1 != '0') {
                $n1 = '1';
            } else {
                $n1 = '0';
            }
            if ($total2 != '0') {
                $n2 = '1';
            } else {
                $n2 = '0';
            }
            if ($total3 != '0') {
                $n3 = '1';
            } else {
                $n3 = '0';
            }
            if ($total4 != '0') {
                $n4 = '1';
            } else {
                $n4 = '0';
            }
            //hasil logical
            $userRules = $n1 . $n2 . $n3 . $n4;
            //menjadikan array dari hasil logical
            $typeRules = array(
                'C' => substr($userRules, 0, 1),
                'R' => substr($userRules, 1, 1),
                'U' => substr($userRules, 2, 1),
                'D' => substr($userRules, 3, 1)
            );
            if (!array_key_exists($param, $typeRules)) {
                show_404();
            } else {
                //atau jika permission di database/field user_rule tidak sama dengan 1 (misal 'C' != 1)
                if ($typeRules[$param] != '1') {
                    //maka ke halaman error
                    show_404();
                }
            }
        }
    }

    function sidebar() {
        $user = $this->CI->ion_auth->user()->row();
        $url = $this->CI->security->xss_clean($this->CI->uri->segment(1) . '/' . $this->CI->uri->segment(2));
        $parent = $this->CI->m_otoritas->get_parent_sidebar($user->id);
        if ($parent->num_rows() > 0) {
            foreach ($parent->result_array() as $key => $value) {
                $child = $this->CI->m_otoritas->get_child_sidebar($user->id, $value['idkatmenu']);
                if ($child->num_rows() > 0) {
                    echo '<li class="treeview';
                    foreach ($child->result_array() as $val) {
                        $parent_url = $val['url'];
                        if ($url == $parent_url) {
                            echo ' active';
                        }
                    }
                    echo '">';
                    echo '<a href="#"><i class="' . $value['mnukat_icon'] . '"></i>';
                    echo '<span>';
                    $parent_menu = array();
                    foreach ($child->result_array() as $mnu) {
                        if (in_array($mnu['katmenu'], $parent_menu)) {
                            continue;
                        }
                        $parent_menu[] = $mnu['katmenu'];
                        echo ucfirst($mnu['katmenu']);
                    }
                    echo '</span>';
                    echo '<i class="fa fa-angle-left pull-right"></i></a>';
                    echo '<ul class="treeview-menu">';
                    foreach ($child->result_array() as $row) {
                        echo'<li ';
                        if ($url == $row['url']) {
                            echo 'class="current"';
                        }
                        echo '>';
                        echo '<a href="' . site_url($row['url']) . '"><i class="' . $row['icon'] . '"></i>' . $row['modul'] . '</a></li>';
                    }
                    echo '</ul>';
                    echo '</li>';
                }
            }
        }
    }

}
