<?php

/**
 * Description of Layout
 *
 * @author White Code
 */
class Layout {

    var $layouts = array();

    function set($yield, $view) {
        $this->layouts[$yield] = $view;
    }

    public function render($template = '', $resource = '', $view = array(), $return = FALSE) {
        $this->ci = &get_instance();
        $this->set('konten', $this->ci->load->view($resource, $view, TRUE));
        return $this->ci->load->view($template, $this->layouts, $return);
    }

}
