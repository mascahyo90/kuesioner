<?php

/**
 * Description of Dashboard
 *
 * @author White Code
 */
class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            redirect('dashboard/dashboard/page');
        }
    }

    public function page() {
        $data["title_panel"] = "Dashboard";
        $data["sub_title_panel"] = "Control Panel";
        $this->layout->render('back', 'dashboard/index', $data);
    }

}
