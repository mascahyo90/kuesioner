<?php
/**
 * Created by PhpStorm.
 * User: white
 * Date: 10/08/17
 * Time: 7:34
 */

class Kuesioner extends CI_Controller {

    var $limit = 20;

    public function __construct() {
        parent::__construct();
        $this->load->model('survei/m_kuesioner', 'the_m');
        $this->load->library('sesfilter');
        $this->load->helper('filter');
        $this->load->helper('geturl');
    }

    public function index() {
        redirect("survei/kuesioner/page");
    }

    public function page() {
        $this->otoritas->rule('R');
        $offset = $this->uri->segment(4, "0");
        $skpd = $this->session->userdata('id_skpd');

        $data["title_panel"] = "Data Survei";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Data Survei');

        // $getAll = $this->the_m->getAllData($skpd, $this->limit, $offset)->result();
        // $jumlah = $this->the_m->getAllData($skpd, "","")->num_rows();
        // $data["table"] = $this->_generateTable($getAll);
        // $data["jumlah"] = $jumlah;
        // $data["message"] = $this->_show_message();
        $data["link_survei"] = site_url("survei/kuesioner/add_srv");
        $this->layout->render('back', 'survei/kuesioner/index', $data);
    }

    public function _generateTable($query) {
        $this->load->library('table');
        $tmpl = array('table_open' => '<table class="table table-bordered table-condensed">');
        $this->table->set_template($tmpl);
        $this->table->set_heading(
            array(
                array("data" => 'No', "style" => "width: 10px"),
                'Tahun',
                array("data" => 'Aksi', "style" => "width: 75px")
            )
        );
        $i = 1;
        foreach ($query as $row) {
            $this->table->add_row($i, $row->q_tahun, array('data' => "
                <a title='Laporan' href='" . site_url("survei/kuesioner/laporan/" . $row->q_tahun) . "' class='btn btn-xs btn-success'><i class='fa fa-sticky-note-o'></i></a>")
            );
            $i++;
        }
        if ($query == null) {
            $cell = array('data' => 'SKPD anda belum sekalipun melakukan survei', 'style' => 'text-align:center;', 'colspan' => 3);
            $this->table->add_row($cell);
        }
        return $this->table->generate();
    }

    public function add_srv() {
        $this->otoritas->rule('R');
        $skpd = $this->session->userdata('id_skpd');
        $tahun_aktif = date('Y');

        $data["title_panel"] = "Data Kuesioner";
        $data["sub_title_panel"] = $tahun_aktif;
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Kuesioner');

        $kegiatan = $this->the_m->getKegiatanBySkpd($skpd)->result();
        $data['tabel_survei'] = $this->_generateTableSurvei($kegiatan);
        $data["message"] = $this->_show_message();
        $data["skpd_id"] = $skpd;
        $data["tahun"] = $tahun_aktif;
        $this->layout->render('back', 'survei/kuesioner/create', $data);
    }

    // public function add() {
    //     $this->otoritas->rule('R');
    //     $skpd = $this->session->userdata('id_skpd');
    //     $tahun_aktif = date('Y');

    //     $data["title_panel"] = "Data Kuesioner";
    //     $data["sub_title_panel"] = "";
    //     $this->breadcrumbs->clear();
    //     $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
    //     $this->breadcrumbs->add_crumb('Kuesioner');

    //     $kegiatan = $this->the_m->getKegiatanBySkpd($skpd, $tahun_aktif)->result();
    //     $data['tabel_survei'] = $this->_generateTableSurvei($kegiatan);
    //     $data["message"] = $this->_show_message();
    //     $data["skpd_id"] = $skpd;
    //     $data["tahun"] = $tahun_aktif;
    //     $this->layout->render('back', 'survei/kuesioner/create', $data);
    // }

    public function _generateTableSurvei($query) {

        $cekDataPertahun = $this->the_m->cekDataPerTahun(date('Y'));

        if($cekDataPertahun->total > 0) {
            $html = 'Anda sudah melakukan survei sebelumnya, silakan lihat laporan hasil survei per tahun';
//            return $html;
        } else {
            $variabel = $this->the_m->getVariabel()->result();

            $html = '<table class="table table-bordered table-condesed">';
            $html .= '<tr>';
            $html .= '<th style="width: 4%">No</th><th style="width: 50%;">Kegiatan</th>';
            foreach ($variabel as $v) {
                $html .= '<th style="width: 70px;">'.$v->var_label.'</th>';
            }
            $html .= '</tr>';

            $no = 1;
            foreach ($query as $row) {
                $html .= '<tr>';
                $html .= '<td>'.$no++.'</td>';
                $html .= '<td><input type="hidden" value="'.$row->keg_id.'" name="q_keg_id[]">'.$row->keg_nama.'</td>';
                foreach ($variabel as $val) {
                    $html .= '<td><input type="hidden" name="q_var_id[]" value="'.$val->var_id.'"> ';
                    $html .= '<select name="q_nilai_'.$val->var_id.'[]" class="form-control input-sm"><option value="0"></option>';
                    for($i = 1; $i <= 5; $i++) {
                        $html .= '<option value="'.$i.'">'.$i.'</option>';
                    }
                    $html .= '</select>';
                    $html .= '</td>';
                }
                $html .= '</tr>';
            }
            $html .= '</table>';
        }
        return $html;
    }

    public function submit() {
        $this->otoritas->rule('C');

        $keg_id = $this->input->post('q_keg_id');
        $skpd_id = $this->input->post('q_skpd_id');
        $tahun = $this->input->post('q_tahun');

        $v1 = $this->input->post('q_nilai_1');
        $v2 = $this->input->post('q_nilai_2');
        $v3 = $this->input->post('q_nilai_3');
        $v4 = $this->input->post('q_nilai_4');
        $v5 = $this->input->post('q_nilai_5');

        $insert = array();
        foreach ($keg_id as $key => $value) {
            $insert[] = array(
                'q_skpd_id' => $skpd_id,
                'q_keg_id' => $keg_id[$key],
                'q_tahun' => $tahun,
                'q_nilai_1' => $v1[$key]*0.35,
                'q_nilai_2' => $v2[$key]*0.25,
                'q_nilai_3' => $v3[$key]*0.20,
                'q_nilai_4' => $v4[$key]*0.10,
                'q_nilai_5' => $v5[$key]*0.10
            );
        }
        $q = $this->db->insert_batch('kuesioner', $insert);
        if ($q) {
            $this->session->set_flashdata('success', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('error', $this->ion_auth->errors());
        }
        redirect("survei/kuesioner/page");
    }

    public function laporan($tahun) {
        $this->otoritas->rule('R');
        $skpd = $this->session->userdata('id_skpd');

        $data["title_panel"] = "Laporan Kuesioner";
        $data["sub_title_panel"] = "Hasil";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Kuesioner', site_url('survei/kuesioner'));
        $this->breadcrumbs->add_crumb('Laporan');

        $getAll = $this->the_m->getLaporan($skpd, $tahun)->result();
        $data["tabel_laporan"] = $this->_generateTableLaporan($getAll);
        $this->layout->render('back', 'survei/kuesioner/laporan', $data);
    }

    public function _generateTableLaporan($query) {
        if(!empty($query)) {
            $variabel = $this->the_m->getVariabel()->result();
            $html = '<table class="table table-responsive table-bordered table-condesed">';
            $html .= '<tr>';
            $html .= '<th style="width: 4%;vertical-align: middle" rowspan="2">No</th><th rowspan="2" style="width: 50%;vertical-align: middle">Kegiatan</th>';
            foreach ($variabel as $v) {
                $html .= '<th style="width: 70px;text-align: center">'.$v->var_label.'</th>';
            }
            $html .= '<th rowspan="2" style="width: 6%;text-align: center;vertical-align: middle">Nilai</th>';
            $html .= '</tr>';
            $html .= '<tr>';
            foreach ($variabel as $v) {
                $html .= '<td style="text-align: center">'.$v->var_bobot.'</td>';
            }
            $html .= '</tr>';
            $no = 1;
            foreach ($query as $row) {
                $html .= '<tr>';
                $html .= '<td>'.$no++.'</td>';
                $html .= '<td>'.$row->keg_nama.'</td>';
                $html .= '<td style="text-align: center">'.$row->q_nilai_1.'</td>';
                $html .= '<td style="text-align: center">'.$row->q_nilai_2.'</td>';
                $html .= '<td style="text-align: center">'.$row->q_nilai_3.'</td>';
                $html .= '<td style="text-align: center">'.$row->q_nilai_4.'</td>';
                $html .= '<td style="text-align: center">'.$row->q_nilai_5.'</td>';
                $html .= '<td style="text-align: center">'.round($row->total_nilai, 2).'</td>';
                $html .= '</tr>';
            }
            $html .= '</table>';
            return $html;
        }
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }
}