<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Portal
 *
 * @author My Computer
 */
class Portal extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('portal/m_portal', 'the_m');
    }

    public function index() {
        $data["portal_title"] = "PMB AMIKOM SURAKARTA";
        $data["portal_description"] = "Website resmi penerimaan mahasiswa baru AMIKOM Surakarta";
        $data["menu_informasi"] = $this->the_m->getMenuInformasi()->result();
        $data["link_pendaftaran"] = site_url('portal/daftar_online');
        $data["link_cetak_pendaftaran"] = site_url('portal/cetak_kartu');
        $data["link_petunjuk_pendaftaran"] = site_url('portal/petunjuk');
        $data["syarat_daftar"] = $this->the_m->getSyaratPendaftaran()->row();
        $data["gelombang"] = $this->the_m->getGelombang(get_semester_aktif())->result();
        $this->layout->render('portal', 'portal/index', $data);
    }

    public function agenda() {
        $data["portal_title"] = "Daftar Agenda " . get_tahun_semester() . " - PMB AMIKOM SURAKARTA";
        $data["portal_description"] = "daftar agenda " . get_tahun_semester() . " penerimaan mahasiswa baru AMIKOM Surakarta";
        $data["menu_informasi"] = $this->the_m->getMenuInformasi()->result();
        $data["list"] = $this->the_m->getListAgenda()->result();
        $this->layout->render('portal', 'portal/list_agenda', $data);
    }

    public function detail_agenda($slug) {
        $detail = $this->the_m->getDetailAgenda($slug)->row();
        if (!empty($detail->agd_isi)) {
            $desc = $detail->agd_isi;
        } else {
            $desc = '';
        }
        $data["portal_title"] = "Agenda " . $desc . " - PMB AMIKOM SURAKARTA";
        $data["portal_description"] = "agenda " . $desc . " - SPMB AMIKOM Surakarta";
        $data["menu_informasi"] = $this->the_m->getMenuInformasi()->result();
        $data["detail"] = $detail;
        $this->layout->render('portal', 'portal/detail_agenda', $data);
    }

    public function detail_informasi($slug) {
        $detail = $this->the_m->getDetailInformasi($slug)->row();
        if (!empty($detail->info_isi)) {
            $desc = $detail->info_isi;
        } else {
            $desc = '';
        }
        $data["portal_title"] = "Informasi " . $desc . " - PMB AMIKOM SURAKARTA";
        $data["portal_description"] = "informasi " . $desc . " - SPMB AMIKOM Surakarta";
        $data["menu_informasi"] = $this->the_m->getMenuInformasi()->result();
        $data["detail"] = $detail;
        $this->layout->render('portal', 'portal/detail_informasi', $data);
    }

    public function daftar_online() {
        $this->load->library('form_validation');
        $data["portal_title"] = "Pendaftaran Online - PMB AMIKOM SURAKARTA";
        $data["portal_description"] = "form pendaftaran online - SPMB AMIKOM Surakarta";
        $data["menu_informasi"] = $this->the_m->getMenuInformasi()->result();

        $gelombang = $this->the_m->getGelombangByDate(date("Y-m-d"))->row();
        $data["gelombang"] = $gelombang;
        $data["get_prodi"] = $this->the_m->getProdi()->result();
        $data["get_agama"] = $this->the_m->getAgama()->result();
        $data["get_propinsi"] = $this->the_m->getPropinsi()->result();
        $data["get_kota"] = site_url('portal/get_kota');
        $data["get_jenis_smta"] = $this->the_m->getJenisSmta()->result();
        $data["get_jurusan_smta"] = site_url('portal/get_jurusan_smta');
        $data["get_sumber_informasi"] = $this->the_m->getSumberInformasi()->result();

        $this->form_validation->set_rules('mhsd_prodi_kode', 'Pilihan Program Studi', 'required');
        $this->form_validation->set_rules('mhsd_kelas', 'Pilihan Kelas', 'required');
        $this->form_validation->set_rules('mhsd_nodin', 'NIK', 'required');
        $this->form_validation->set_rules('mhsd_nama_lengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('mhsd_tmp_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('mhsd_tgl_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('mhsd_jk', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('mhsd_agm_id', 'Skpd', 'required');
        $this->form_validation->set_rules('mhsd_notelp', 'No. Telepon', 'required|numeric');
        $this->form_validation->set_rules('mhsd_jsmta_id', 'Jenis Sekolah', 'required');
        $this->form_validation->set_rules('mhsd_jursmta_id', 'Jurusan Sekolah', 'required');
        $this->form_validation->set_rules('mhsd_nama_sekolah', 'Nama Sekolah', 'required');
        $this->form_validation->set_rules('mhsd_tahun_lulus', 'Tahun Lulus', 'required');
        $this->form_validation->set_rules('mhsd_nilai', 'NEM', 'required');
        $this->form_validation->set_rules('mhsalmt_alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('mhsalmt_kota_kode', 'Kegiatan/Kabupaten', 'required');
        $this->form_validation->set_rules('mhsalmt_kode_propinsi', 'Variabel', 'required');
        $this->form_validation->set_rules('mhsalmt_kode_pos', 'Kode Pos', 'numeric');

        // Nomor Pendaftaran
        $prodi = $this->input->post('mhsd_prodi_kode');
        $maxNodaf = $this->the_m->getMaxNodafByProdi($prodi)->row();
        $periode = date('ym');
        if (!empty($maxNodaf)) {
            $current = substr($maxNodaf->mhsd_nodaf, -4);
            $nodaf = $periode . $prodi . sprintf("%04s", $current + 1);
        } else {
            $nodaf = $periode . $prodi . '0001';
        }

        $noref = $this->_nomor_referensi(8);
        //Tanggal Ujian
        $tglDaftar = date('Y-m-d');
        $tglUjian = date('Y-m-d', strtotime($tglDaftar . ' + 3 days'));

        //Sumber Informasi
        $info = $this->input->post('mhsd_sinfo_id');
        if (isset($info) OR ! empty($info)) {
            $informasi = implode(",", $info);
        } else {
            $informasi = "NULL";
        }

        if ($this->form_validation->run() == TRUE) {
            $insert = array(
                'mhsd_nodaf' => $nodaf,
                'mhsd_noref' => $noref,
                'mhsd_gel_id' => $this->input->post('mhsd_gel_id'),
                'mhsd_prodi_kode' => $this->input->post('mhsd_prodi_kode'),
                'mhsd_kelas' => $this->input->post('mhsd_kelas'),
                'mhsd_tgl_daftar' => $tglDaftar,
                'mhsd_warga_negara' => $this->input->post('mhsd_warga_negara'),
                'mhsd_identitas' => 'K',
                'mhsd_nodin' => $this->input->post('mhsd_nodin'),
                'mhsd_nama_lengkap' => $this->input->post('mhsd_nama_lengkap'),
                'mhsd_tmp_lahir' => $this->input->post('mhsd_tmp_lahir'),
                'mhsd_tgl_lahir' => $this->input->post('mhsd_tgl_lahir'),
                'mhsd_jk' => $this->input->post('mhsd_jk'),
                'mhsd_agm_id' => $this->input->post('mhsd_agm_id'),
                'mhsd_jsmta_id' => $this->input->post('mhsd_jsmta_id'),
                'mhsd_nama_sekolah' => $this->input->post('mhsd_nama_sekolah'),
                'mhsd_jursmta_id' => $this->input->post('mhsd_jursmta_id'),
                'mhsd_tahun_lulus' => $this->input->post('mhsd_tahun_lulus'),
                'mhsd_nilai' => $this->input->post('mhsd_nilai'),
                'mhsd_notelp' => $this->input->post('mhsd_notelp'),
                'mhsd_tgl_ujian' => $tglUjian,
                'mhsd_status_daftar' => '1',
                'mhsd_foto' => 'no_image.jpg',
                'mhsd_sinfo_id' => $informasi
            );
            $q = $this->the_m->create($insert);
            $this->the_m->createToPis($insert);
            $id = $this->db->insert_id();
            $insert_alamat = array(
                'mhsalmt_mhsd_id' => $id,
                'mhsalmt_jenis' => 'A',
                'mhsalmt_alamat' => $this->input->post('mhsalmt_alamat'),
                'mhsalmt_kecamatan' => $this->input->post('mhsalmt_kecamatan'),
                'mhsalmt_kode_propinsi' => $this->input->post('mhsalmt_kode_propinsi'),
                'mhsalmt_kota_kode' => $this->input->post('mhsalmt_kota_kode'),
                'mhsalmt_kode_pos' => $this->input->post('mhsalmt_kode_pos')
            );
            $q = $this->the_m->createAlamat($insert_alamat);
            if ($q) {
//                redirect("portal/print_kartu/" . $nodaf);
                redirect("portal/detail_daftar/" . $nodaf);
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->layout->render('portal', 'portal/daftar', $data);
        }
    }

    public function detail_daftar($id) {
        $data["portal_title"] = "Detail Pendaftaran Online - PMB AMIKOM SURAKARTA";
        $data["portal_description"] = "form detail pendaftaran online - SPMB AMIKOM Surakarta";
        $data["menu_informasi"] = $this->the_m->getMenuInformasi()->result();
        $data["nodaf"] = $id;
        $this->layout->render('portal', 'portal/detail_daftar', $data);
    }

    public function print_kartu($id) {
        $this->load->library('dompdf_gen');
        $print = $this->the_m->printKartuPendaftaran($id)->row();

        //Kelas
        if ($print->mhsd_kelas == '1') {
            $kelas = 'Pagi';
        } else if ($print->mhsd_kelas == '2') {
            $kelas = 'Sore';
        }
        $data["kelas"] = $kelas;
        $data["print"] = $print;
        $html = $this->layout->render('print', 'admisi/pendaftar/kartu_pendaftaran', $data, TRUE);
        $file = 'Kartu_Pendaftaran_' . $print->mhsd_nodaf;
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream($file . '.pdf', array("Attachment" => 0));
    }

    public function cetak_kartu() {
        $data["portal_title"] = "Cetak Kartu Pendaftaran Mahasiswa Baru - PMB AMIKOM SURAKARTA";
        $data["portal_description"] = "cetak kartu pendaftaran mahasiswa baru - SPMB AMIKOM Surakarta";
        $data["menu_informasi"] = $this->the_m->getMenuInformasi()->result();
        $this->layout->render('portal', 'portal/cetak_kartu', $data);
    }

    public function cetak() {
        $id = $this->input->post('mhsd_nodaf');
        $this->load->library('dompdf_gen');
        $print = $this->the_m->printKartuPendaftaran($id)->row();

        //Kelas
        if ($print->mhsd_kelas == '1') {
            $kelas = 'Pagi';
        } else if ($print->mhsd_kelas == '2') {
            $kelas = 'Sore';
        }
        $data["kelas"] = $kelas;
        $data["print"] = $print;
        $html = $this->layout->render('print', 'admisi/pendaftar/kartu_pendaftaran', $data, TRUE);
        $file = 'Kartu_Pendaftaran_' . $print->mhsd_nodaf;
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream($file . '.pdf', array("Attachment" => 0));
    }

    function get_kota($id) {
        $get_kota = $this->the_m->getKota($id)->result();
        $data = "<option value=''>-- Pilih --</option>";
        foreach ($get_kota as $val) {
            $data .= "<option value='" . $val->id . "' " . set_select('mhsalmt_kota_kode', $val->id) . ">" . $val->nama . "</option>";
        }
        echo $data;
    }

    function get_jurusan_smta($id) {
        $get = $this->the_m->getJurusanSmta($id)->result();
        $data = "<option value=''>-- Pilih --</option>";
        foreach ($get as $val) {
            $data .= "<option value='" . $val->id . "' " . set_select('mhsd_jursmta_id', $val->id) . ">" . $val->nama . "</option>";
        }
        echo $data;
    }

    function _nomor_referensi($length) {
        $random = "";
        srand((double) microtime() * 1000000);

        $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
        $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
        $data .= "0FGH45OP89";
        for ($i = 0; $i < $length; $i++) {
            $random .= substr($data, (rand() % (strlen($data))), 1);
        }
        return $random;
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}
