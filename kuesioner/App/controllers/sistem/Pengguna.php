<?php

/**
 * Description of Pengguna
 *
 * @author White Code
 */
class Pengguna extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sistem/m_sistem', 'the_m');
    }

    public function index() {
        $this->otoritas->rule("R");
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        } else {
            redirect('sistem/pengguna/page');
        }
    }

    public function page() {
        $this->otoritas->rule("R");
        $data["title_panel"] = "Manajemen Pengguna";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Pengguna');
        $data["users"] = $this->ion_auth->users()->result();
        foreach ($data['users'] as $k => $user) {
            $data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
        }
        $data["tambah"] = site_url('auth/create_user');
        $data["message"] = $this->_show_message();
        $this->layout->render('back', 'auth/list_user', $data);
    }

    public function delete($id) {
        $this->otoritas->rule("D");
        $q = $this->the_m->delete_pengguna($id);
        if ($q) {
            $this->session->set_flashdata('success', 'Data Berhasil dihapus');
        } else {
            $this->session->set_flashdata('error', 'Data Gagal Dihapus, Error: ' . $this->ion_auth->errors());
        }
        redirect("sistem/pengguna/page", 'refresh');
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}
