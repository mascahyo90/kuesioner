<?php

/**
 * Description of Menu
 *
 * @author White Code
 */
class Menu extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sistem/m_sistem', 'the_m');
        $this->load->library('form_validation');
    }

    public function index() {
        $this->otoritas->rule('R');
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        } else {
            redirect('sistem/menu/page');
        }
    }

    public function page() {
        $this->otoritas->rule('R');
        $data["title_panel"] = "Manajemen Menu";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Menu');
        $data["list"] = $this->the_m->get_menu()->result();
        $data["tambah"] = site_url('sistem/menu/add');
        $data["message"] = $this->_show_message();
        $this->layout->render('back', 'auth/list_menu', $data);
    }

    public function add() {
        $this->otoritas->rule('C');
        $data["title_panel"] = "Manajemen Menu";
        $data["sub_title_panel"] = "Tambah Data";
        $data["title_box"] = "Tambah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Menu', site_url('sistem/menu'));
        $this->breadcrumbs->add_crumb('Tambah Data');

        $data["list_parent"] = $this->the_m->get_kategori_menu()->result();

        $this->form_validation->set_rules('mnu_menu', 'Menu', 'required');
        $this->form_validation->set_rules('mnu_link', 'URL', 'required');

        if ($this->form_validation->run() == TRUE) {
            $insert = array(
                'mnu_mnukat_id' => $this->input->post('mnu_mnukat_id'),
                'mnu_menu' => $this->input->post('mnu_menu'),
                'mnu_link' => strtolower($this->input->post('mnu_link')),
                'mnu_deskripsi' => $this->input->post('mnu_deskripsi'),
                'mnu_order' => $this->input->post('mnu_order'),
                'mnu_icon' => $this->input->post('mnu_icon')
            );
            $q = $this->the_m->create_menu($insert);
            if ($q) {
                $this->session->set_flashdata('success', 'Data Berhasil Ditambah');
                redirect("sistem/menu/page", 'refresh');
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $data['mnu_mnukat_id'] = array(
                'name' => 'mnu_mnukat_id',
                'id' => 'mnu_mnukat_id',
                'type' => 'options',
                'value' => $this->form_validation->set_value('mnu_mnukat_id'),
            );
            $data['mnu_menu'] = array(
                'name' => 'mnu_menu',
                'id' => 'mnu_menu',
                'type' => 'text',
                'value' => $this->form_validation->set_value('mnu_menu'),
            );
            $data['mnu_link'] = array(
                'name' => 'mnu_link',
                'id' => 'mnu_link',
                'type' => 'text',
                'value' => $this->form_validation->set_value('mnu_link'),
            );
            $this->layout->render('back', 'auth/create_menu', $data);
        }
    }

    public function edit($id) {
        $this->otoritas->rule('U');
        if (!$id || empty($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect('sistem/menu', 'refresh');
        }
        $data["title_panel"] = "Manajemen Menu";
        $data["sub_title_panel"] = "Ubah Data";
        $data["title_box"] = "Ubah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Menu', site_url('sistem/menu'));
        $this->breadcrumbs->add_crumb('Ubah Data');

        $menu = $this->the_m->get_menu_by_id($id)->row();
        $data["list_parent"] = $this->the_m->get_kategori_menu()->result();

        $this->form_validation->set_rules('mnu_menu', 'Menu', 'required');
        $this->form_validation->set_rules('mnu_link', 'Link', 'required');

        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $update = array(
                    'mnu_mnukat_id' => $this->input->post('mnu_mnukat_id'),
                    'mnu_menu' => $this->input->post('mnu_menu'),
                    'mnu_link' => strtolower($this->input->post('mnu_link')),
                    'mnu_deskripsi' => $this->input->post('mnu_deskripsi'),
                    'mnu_order' => $this->input->post('mnu_order'),
                    'mnu_icon' => $this->input->post('mnu_icon')
                );
                $q = $this->the_m->update_menu($id, $update);
                if ($q) {
                    $this->session->set_flashdata('success', 'Data berhasil dirubah');
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->errors());
                }
                redirect("sistem/menu/page", 'refresh');
            }
        }
        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $data['update'] = $menu;
        $data['mnu_menu'] = array(
            'name' => 'mnu_menu',
            'id' => 'mnu_menu',
            'type' => 'text',
            'value' => $this->form_validation->set_value('mnu_menu', $menu->mnu_menu),
        );
        $data['mnu_link'] = array(
            'name' => 'mnu_link',
            'id' => 'mnu_link',
            'type' => 'text',
            'value' => $this->form_validation->set_value('mnu_link', $menu->mnu_link),
        );

        $this->layout->render('back', 'auth/edit_menu', $data);
    }

    public function delete($id) {
        $this->otoritas->rule('D');
        $q = $this->the_m->delete_menu($id);
        if ($q) {
            $this->session->set_flashdata('success', 'Data Berhasil Dihapus');
        } else {
            $this->session->set_flashdata('error', $this->ion_auth->errors());
        }
        redirect("sistem/menu/page", 'refresh');
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}
