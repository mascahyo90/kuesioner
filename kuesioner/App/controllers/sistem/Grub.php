<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Grub
 *
 * @author My Computer
 */
class Grub extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->otoritas->rule('R');
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        } else {
            redirect('sistem/grub/page');
        }
    }

    public function page() {
        $this->otoritas->rule('R');
        $data["title_panel"] = "Manajemen Grub";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Grub');
        $data["groups"] = $this->ion_auth->groups()->result();
        $data["tambah"] = site_url('auth/create_group');
        $data["message"] = $this->_show_message();
        $this->layout->render('back', 'auth/list_group', $data);
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}
