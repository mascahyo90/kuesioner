<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Katmenu
 *
 * @author My Computer
 */
class Katmenu extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sistem/m_sistem', 'the_m');
        $this->load->library('form_validation');
    }

    public function index() {
        $this->otoritas->rule('R');
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        } else {
            redirect('sistem/katmenu/page');
        }
    }

    public function page() {
        $this->otoritas->rule('R');
        $data["title_panel"] = "Manajemen Kategori Menu";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Kategori Menu');
        $data["list"] = $this->the_m->get_kategori_menu()->result();
        $data["tambah"] = site_url('sistem/katmenu/add');
        $data["message"] = $this->_show_message();
        $this->layout->render('back', 'auth/list_katmenu', $data);
    }

    public function add() {
        $this->otoritas->rule('C');
        $data["title_panel"] = "Manajemen Kategori Menu";
        $data["sub_title_panel"] = "Tambah Data";
        $data["title_box"] = "Tambah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Kategori Menu', site_url('sistem/katmenu'));
        $this->breadcrumbs->add_crumb('Tambah Data');

        $this->form_validation->set_rules('mnukat_menu', 'Kategori Menu', 'required');
        $this->form_validation->set_rules('mnukat_icon', 'Ikon', 'required');

        if ($this->form_validation->run() == TRUE) {
            $insert = array(
                'mnukat_menu' => $this->input->post('mnukat_menu'),
                'mnukat_icon' => $this->input->post('mnukat_icon')
            );
            $q = $this->the_m->create_katmenu($insert);
            if ($q) {
                $this->session->set_flashdata('success', 'Data Berhasil Ditambah');
                redirect("sistem/katmenu/page", 'refresh');
            } else {
                $this->session->set_flashdata('error', 'Data Gagal Ditambah');
                redirect("sistem/katmenu/page", 'refresh');
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $data['mnukat_menu'] = array(
                'name' => 'mnukat_menu',
                'id' => 'mnukat_menu',
                'type' => 'text',
                'value' => $this->form_validation->set_value('mnukat_menu'),
            );
            $data['mnukat_icon'] = array(
                'name' => 'mnukat_icon',
                'id' => 'mnukat_icon',
                'type' => 'text',
                'value' => $this->form_validation->set_value('mnukat_icon'),
            );
            $this->layout->render('back', 'auth/create_katmenu', $data);
        }
    }

    public function edit($id) {
        $this->otoritas->rule('U');
        // Jika id kategori menu tidak ditemukan
        if (!$id || empty($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect('sistem/katmenu', 'refresh');
        }

        $data["title_panel"] = "Manajemen Kategori Menu";
        $data["sub_title_panel"] = "Ubah Data";
        $data["title_box"] = "Ubah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Kategori Menu', site_url('sistem/katmenu'));
        $this->breadcrumbs->add_crumb('Ubah Data');

        $kmenu = $this->the_m->get_katmenu_by_id($id)->row();

        $this->form_validation->set_rules('mnukat_menu', 'Kategori Menu', 'required');
        $this->form_validation->set_rules('mnukat_icon', 'Ikon', 'required');

        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $update = array(
                    'mnukat_menu' => $this->input->post('mnukat_menu'),
                    'mnukat_icon' => $this->input->post('mnukat_icon')
                );
                $q = $this->the_m->update_katmenu($id, $update);
                if ($q) {
                    $this->session->set_flashdata('success', 'Data Berhasil dirubah');
                } else {
                    $this->session->set_flashdata('error', 'Data Gagal Dirubah, Error: ' . $this->ion_auth->errors());
                }
                redirect("sistem/katmenu/page", 'refresh');
            }
        }
        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $data['update'] = $kmenu;

        $data['mnukat_menu'] = array(
            'name' => 'mnukat_menu',
            'id' => 'mnukat_menu',
            'type' => 'text',
            'value' => $this->form_validation->set_value('mnukat_menu', $kmenu->mnukat_menu),
        );
        $data['mnukat_icon'] = array(
            'name' => 'mnukat_icon',
            'id' => 'mnukat_icon',
            'type' => 'text',
            'value' => $this->form_validation->set_value('mnukat_icon', $kmenu->mnukat_icon),
        );

        $this->layout->render('back', 'auth/edit_katmenu', $data);
    }

    public function delete($id) {
        $this->otoritas->rule('D');
        $q = $this->the_m->delete_katmenu($id);
        if ($q) {
            $this->session->set_flashdata('success', 'Data Berhasil dihapus');
        } else {
            $this->session->set_flashdata('error', 'Data Gagal Dihapus, Error: ' . $this->ion_auth->errors());
        }
        redirect("sistem/katmenu/page", 'refresh');
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}
