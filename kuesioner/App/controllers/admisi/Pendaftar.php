<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pendaftar
 *
 * @author My Computer
 */
class Pendaftar extends CI_Controller {

    var $limit = 30;

    public function __construct() {
        parent::__construct();
        $this->load->model('admisi/m_pendaftar', 'the_m');
        $this->load->library('sesfilter');
        $this->load->helper('filter');
        $this->load->helper('geturl');
        $this->sesfilter->setSesFilterName("admisi_pendaftar", "admisi_pendaftar_lock");
    }

    public function filter() {
        $this->sesfilter->setFilterFromPost("fil_nodaf", "fil_nodaf");
        $this->sesfilter->setFilterFromPost("fil_noref", "fil_noref");
        $this->sesfilter->setFilterFromPost("fil_semester", "fil_semester");
        $this->sesfilter->setFilterFromPost("fil_gelombang", "fil_gelombang");
        $this->sesfilter->setFilterFromPost("fil_prodi", "fil_prodi");
        $this->sesfilter->setFilterFromPost("fil_nama", "fil_nama");
        $this->sesfilter->setFilterFromPost("fil_status_ujian", "fil_status_ujian");
        $this->sesfilter->setFilterFromPost("fil_status_daftar", "fil_status_daftar");
        redirect("admisi/pendaftar/page");
    }

    public function index() {
        $this->sesfilter->resetFilter();
        redirect("admisi/pendaftar/page");
    }

    public function page() {
        $this->otoritas->rule('R');
        $offset = $this->uri->segment(4, "0");
        $data["title_panel"] = "Manajemen Pendaftar";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Pendaftar');

        $nodaf = $this->sesfilter->getFilter("fil_nodaf");
        $noref = $this->sesfilter->getFilter("fil_noref");
        $semester = $this->sesfilter->getFilter("fil_semester");
        $gelombang = $this->sesfilter->getFilter("fil_gelombang");
        $prodi = $this->sesfilter->getFilter("fil_prodi");
        $nama = $this->sesfilter->getFilter("fil_nama");
        $ujian = $this->sesfilter->getFilter("fil_status_ujian");
        $daftar = $this->sesfilter->getFilter("fil_status_daftar");

        $getAll = $this->the_m->getAllData($nodaf, $noref, $gelombang, $prodi, $nama, $ujian, $daftar, $this->limit, $offset)->result();
        $jumlah = $this->the_m->getAllData($nodaf, $noref, $gelombang, $prodi, $nama, $ujian, $daftar, "", "")->num_rows();

        $data["nodaf"] = $nodaf;
        $data["noref"] = $noref;
        $data["semester"] = $semester;
        $data["gelombang"] = $gelombang;
        $data["prodi"] = $prodi;
        $data["nama"] = $nama;
        $data["ujian"] = $ujian;
        $data["daftar"] = $daftar;

        $data["get_semester"] = $this->the_m->getSemester()->result();
        $data["get_prodi"] = $this->the_m->getProdi()->result();
        $data["get_gelombang"] = site_url('admisi/pendaftar/get_gelombang');

        $data["table"] = $this->_generateTable($offset, $getAll);
        $data["jumlah"] = $jumlah;
        $data["link_tambah"] = site_url("admisi/pendaftar/add");
        $data["link_filter"] = site_url("admisi/pendaftar/filter");
        $data["link_status_lulus"] = site_url("admisi/pendaftar/simpan_status_lulus");
        $data["link_bayar_daftar"] = site_url("admisi/pendaftar/simpan_pembayaran");
        $data['link_multi_delete'] = site_url("admisi/pendaftar/multi_delete");
        $data["pagination"] = _pagination($jumlah, $this->limit, "admisi/pendaftar/page", 4);
        $data["message"] = $this->_show_message();
        $this->layout->render('back', 'admisi/pendaftar/index', $data);
    }

    public function _generateTable($offset, $query) {
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="tdata" class="table table-bordered table-condensed">');
        $this->table->set_template($tmpl);
        $this->table->set_heading(
                array(
                    array("data" => form_checkbox('cbPilihParent', "0", FALSE, "class='cbPilihParent'"), "style" => "width:20px"),
                    array("data" => 'No', "style" => "width: 10px"),
                    array("data" => 'No. Daftar', "style" => "width: 100px"),
                    array("data" => 'No. Referensi', "style" => "width: 100px"),
                    'Nama Lengkap',
                    'Program Studi',
                    array("data" => 'JK', "style" => "width: 20px"),
                    'Tanggal Daftar',
                    array("data" => 'Aksi', "style" => "width: 190px")
                )
        );
        $i = $offset + 1;
        foreach ($query as $row) {
            if ($row->mhsd_status_ujian == '0') {
                $style = 'background: #f4bc42';
            } else if ($row->mhsd_status_ujian == '1') {
                $style = 'background: #ff2a00';
            } else if ($row->mhsd_status_ujian == '2') {
                $style = '';
            }
            
            if($row->mhsd_status_registrasi == '1') {
                $url_edit = "";
            } else {
                $url_edit = "<a title='Ubah' href='" . site_url("admisi/pendaftar/edit/" . $row->mhsd_id) . "' class='btn btn-xs btn-success'><i class='fa fa-pencil'></i></a>";
            }

            if ($row->mhsd_biaya_daftar == '' OR $row->mhsd_biaya_daftar == 0 OR empty($row->mhsd_biaya_daftar)) {
                $url_bayar = "<a title='Pembayaran Pendaftaran' data-toggle='modal' data-target='#pembayaran-daftar' href='#' ref='" . site_url("admisi/pendaftar/bayar/" . $row->mhsd_id) . "' class='btn btn-xs bg-teal bayar-daftar'><i class='fa fa-money'></i></a>";
                $url_kw = "";
                $url_lulus = "";
            } else {
                $url_bayar = "";
                $url_kw = "<a style='' target='_blank' title='Cetak Kwitansi' href='" . site_url("admisi/pendaftar/printkw/" . $row->mhsd_id) . "' class='btn btn-xs btn-primary'><i class='fa fa-print'></i></a>";
                $url_lulus = "<a title='Ubah Kelulusan' data-toggle='modal' data-target='#ubah-status-lulus' href='#' ref='" . site_url("admisi/pendaftar/status_lulus/" . $row->mhsd_id) . "' class='btn btn-xs btn-flickr ganti-status-lulus'><i class='fa fa-exchange'></i></a>";
            }

            $no = array('data' => $i, 'style' => $style, 'align' => 'center');
            $nodaf = array('data' => $row->mhsd_nodaf, 'style' => $style);
            $noref = array('data' => $row->mhsd_noref, 'style' => $style);
            $nama = array('data' => $row->mhsd_nama_lengkap, 'style' => $style);
            $prodi = array('data' => $row->prodi, 'style' => $style);
            $jk = array('data' => $row->mhsd_jk, 'style' => $style, 'align' => 'center');
            $tgl = array('data' => tanggal_indonesia($row->mhsd_tgl_daftar), 'style' => $style);
            $this->table->add_row(form_checkbox('cbPilih', $row->mhsd_nodaf, FALSE, "class='cbPilih'"), $no, $nodaf, $noref, $nama, $prodi, $jk, $tgl, array('data' => "
                        $url_lulus
                        $url_edit
                        <a target='_blank' title='Detail' href='" . site_url("admisi/pendaftar/detail/" . $row->mhsd_id) . "' class='btn btn-xs btn-default'><i class='fa fa-search'></i></a>
                        <a target='_blank' title='Cetak Kartu Pendaftaran' href='" . site_url("admisi/pendaftar/printcard/" . $row->mhsd_id) . "' class='btn btn-xs bg-purple'><i class='fa fa-print'></i></a>
                        $url_bayar
                        $url_kw
                        <a title='Hapus' href='#' data-toggle='modal' data-target='#confirm-delete' data-href='" . site_url("admisi/pendaftar/delete/" . $row->mhsd_id) . "' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i></a>")
            );
            $i++;
        }
        if ($query == null) {
            $cell = array('data' => 'Tidak Ada Data', 'style' => 'text-align:center;', 'colspan' => 9);
            $this->table->add_row($cell);
        }
        return $this->table->generate();
    }

    public function add() {
        $this->otoritas->rule('C');
        $this->load->library('form_validation');
        $data["title_panel"] = "Manajemen Pendaftar";
        $data["sub_title_panel"] = "Tambah Data";
        $data["title_box"] = "Tambah Pendaftar";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Pendaftar', site_url('admisi/pendaftar'));
        $this->breadcrumbs->add_crumb('Tambah Data');

        $data["get_semester"] = $this->the_m->getSemester()->result();
        $data["get_prodi"] = $this->the_m->getProdi()->result();
        $data["get_gelombang"] = $this->the_m->getGelombangAktif()->result();
        $data["get_agama"] = $this->the_m->getAgama()->result();
        $data["get_jenis_smta"] = $this->the_m->getJenisSmta()->result();
        $data["get_jurusan_smta"] = site_url('admisi/pendaftar/get_jurusan_smta');
        $data["get_propinsi"] = $this->the_m->getPropinsi()->result();
        $data["get_kota"] = site_url('admisi/pendaftar/get_kota');
        $data["get_sumber_informasi"] = $this->the_m->getSumberInformasi()->result();

        $this->form_validation->set_rules('mhsd_prodi_kode', 'Program Studi', 'required');
        $this->form_validation->set_rules('mhsd_gel_id', 'Gelombang', 'required');
        $this->form_validation->set_rules('mhsd_kelas', 'Kelas', 'required');
        $this->form_validation->set_rules('mhsd_status_daftar', 'Status Pendaftaran', 'required');
        $this->form_validation->set_rules('mhsd_nodin', 'Nomor Identitas', 'required');
        $this->form_validation->set_rules('mhsd_nama_lengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('mhsd_tmp_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('mhsd_tgl_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('mhsd_jsmta_id', 'Jenis Sekolah', 'required');
        $this->form_validation->set_rules('mhsd_nama_sekolah', 'Nama Sekolag', 'required');
        $this->form_validation->set_rules('mhsd_tahun_lulus', 'Tahun Lulus', 'required');
        $this->form_validation->set_rules('mhsd_nilai', 'NEM', 'required');
        $this->form_validation->set_rules('mhsalmt_alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('mhsalmt_kota_kode', 'Kegiatan/Kabupaten', 'required');
        $this->form_validation->set_rules('mhsalmt_kode_propinsi', 'Variabel', 'required');

        //Generate Nomor Pendaftaran
        $prodi = $this->input->post('mhsd_prodi_kode');
        $maxNodaf = $this->the_m->getMaxNodafByProdi($prodi)->row();
        $periode = date('ym');
        if (!empty($maxNodaf)) {
            $current = substr($maxNodaf->mhsd_nodaf, -4);
            $nodaf = $periode . $prodi . sprintf("%04s", $current + 1);
        } else {
            $nodaf = $periode . $prodi . '0001';
        }

        $noref = $this->_nomor_referensi(8);

        //Tanggal Ujian
        $tglDaftar = $this->input->post('mhsd_tgl_daftar');
        $tglUjian = date('Y-m-d', strtotime($tglDaftar . ' + 3 days'));

        //Sumber Informasi
        $info = $this->input->post('mhsd_sinfo_id');
        if (isset($info) OR ! empty($info)) {
            $informasi = implode(",", $info);
        } else {
            $informasi = "NULL";
        }

        if ($this->form_validation->run() == TRUE) {
            $file = $_FILES['userfile']['name'];
            if (empty($file)) {
                $insert = array(
                    'mhsd_nodaf' => $nodaf,
                    'mhsd_noref' => $noref,
                    'mhsd_gel_id' => $this->input->post('mhsd_gel_id'),
                    'mhsd_prodi_kode' => $this->input->post('mhsd_prodi_kode'),
                    'mhsd_kelas' => $this->input->post('mhsd_kelas'),
                    'mhsd_tgl_daftar' => $this->input->post('mhsd_tgl_daftar'),
                    'mhsd_warga_negara' => $this->input->post('mhsd_warga_negara'),
                    'mhsd_identitas' => $this->input->post('mhsd_identitas'),
                    'mhsd_nodin' => $this->input->post('mhsd_nodin'),
                    'mhsd_nama_lengkap' => $this->input->post('mhsd_nama_lengkap'),
                    'mhsd_tmp_lahir' => $this->input->post('mhsd_tmp_lahir'),
                    'mhsd_tgl_lahir' => $this->input->post('mhsd_tgl_lahir'),
                    'mhsd_jk' => $this->input->post('mhsd_jk'),
                    'mhsd_agm_id' => $this->input->post('mhsd_agm_id'),
                    'mhsd_status_nikah' => $this->input->post('mhsd_status_nikah'),
                    'mhsd_jsmta_id' => $this->input->post('mhsd_jsmta_id'),
                    'mhsd_nama_sekolah' => $this->input->post('mhsd_nama_sekolah'),
                    'mhsd_jursmta_id' => $this->input->post('mhsd_jursmta_id'),
                    'mhsd_tahun_lulus' => $this->input->post('mhsd_tahun_lulus'),
                    'mhsd_nilai' => $this->input->post('mhsd_nilai'),
                    'mhsd_tgl_ijasah' => $this->input->post('mhsd_tgl_ijasah'),
                    'mhsd_noijasah' => $this->input->post('mhsd_noijasah'),
                    'mhsd_notelp' => $this->input->post('mhsd_notelp'),
                    'mhsd_email' => $this->input->post('mhsd_email'),
                    'mhsd_website' => $this->input->post('mhsd_website'),
                    'mhsd_tgl_ujian' => $tglUjian,
                    'mhsd_status_daftar' => $this->input->post('mhsd_status_daftar'),
                    'mhsd_foto' => 'no_image.jpg',
                    'mhsd_sinfo_id' => $informasi
                );
            } else {
                $path = dirname(BASEPATH) . DIRECTORY_SEPARATOR;
                $config['upload_path'] = $path . 'Files/Foto/Mahasiswa';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['overwrite'] = true;
                $config['max_size'] = '250';
                $this->load->library('upload', $config);

                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $insert = array(
                        'mhsd_nodaf' => $nodaf,
                        'mhsd_noref' => $noref,
                        'mhsd_gel_id' => $this->input->post('mhsd_gel_id'),
                        'mhsd_prodi_kode' => $this->input->post('mhsd_prodi_kode'),
                        'mhsd_kelas' => $this->input->post('mhsd_kelas'),
                        'mhsd_tgl_daftar' => $this->input->post('mhsd_tgl_daftar'),
                        'mhsd_warga_negara' => $this->input->post('mhsd_warga_negara'),
                        'mhsd_identitas' => $this->input->post('mhsd_identitas'),
                        'mhsd_nodin' => $this->input->post('mhsd_nodin'),
                        'mhsd_nama_lengkap' => $this->input->post('mhsd_nama_lengkap'),
                        'mhsd_tmp_lahir' => $this->input->post('mhsd_tmp_lahir'),
                        'mhsd_tgl_lahir' => $this->input->post('mhsd_tgl_lahir'),
                        'mhsd_jk' => $this->input->post('mhsd_jk'),
                        'mhsd_agm_id' => $this->input->post('mhsd_agm_id'),
                        'mhsd_status_nikah' => $this->input->post('mhsd_status_nikah'),
                        'mhsd_jsmta_id' => $this->input->post('mhsd_jsmta_id'),
                        'mhsd_nama_sekolah' => $this->input->post('mhsd_nama_sekolah'),
                        'mhsd_jursmta_id' => $this->input->post('mhsd_jursmta_id'),
                        'mhsd_tahun_lulus' => $this->input->post('mhsd_tahun_lulus'),
                        'mhsd_nilai' => $this->input->post('mhsd_nilai'),
                        'mhsd_tgl_ijasah' => $this->input->post('mhsd_tgl_ijasah'),
                        'mhsd_noijasah' => $this->input->post('mhsd_noijasah'),
                        'mhsd_notelp' => $this->input->post('mhsd_notelp'),
                        'mhsd_email' => $this->input->post('mhsd_email'),
                        'mhsd_website' => $this->input->post('mhsd_website'),
                        'mhsd_tgl_ujian' => $tglUjian,
                        'mhsd_status_daftar' => $this->input->post('mhsd_status_daftar'),
                        'mhsd_foto' => $data["file_name"],
                        'mhsd_sinfo_id' => $informasi
                    );
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                }
            }
            $q = $this->the_m->create($insert);
            $this->the_m->createMahasiswaToPis($insert);
            if ($q) {
                $id = $this->db->insert_id();
                $insert_alamat = array(
                    'mhsalmt_mhsd_id' => $id,
                    'mhsalmt_jenis' => 'A',
                    'mhsalmt_alamat' => $this->input->post('mhsalmt_alamat'),
                    'mhsalmt_kecamatan' => $this->input->post('mhsalmt_kecamatan'),
                    'mhsalmt_kode_propinsi' => $this->input->post('mhsalmt_kode_propinsi'),
                    'mhsalmt_kota_kode' => $this->input->post('mhsalmt_kota_kode'),
                    'mhsalmt_kode_pos' => $this->input->post('mhsalmt_kode_pos')
                );
                $q = $this->the_m->createAlamat($insert_alamat);
                if ($q) {
                    $salt = '2017';
                    $password = $this->bcrypt->hash($noref);
                    $insert_user = array(
                        'username' => $nodaf,
                        'password' => $password,
                        'salt' => $salt,
                        'last_name' => $this->input->post('mhsd_nama_lengkap')
                    );
                    $r = $this->the_m->createUser($insert_user);
                    if ($r) {
                        $idUser = $this->db->insert_id();
                        $insert_group = array(
                            'user_id' => $idUser,
                            'group_id' => '2'
                        );
                        $r = $this->the_m->createGroup($insert_group);
                        if ($r) {
                            $this->session->set_flashdata('success', 'Data Berhasil Ditambah');
                            redirect("admisi/pendaftar/page", 'refresh');
                        } else {
                            $this->session->set_flashdata('error', 'Data Gagal Ditambah');
                            redirect("admisi/pendaftar/page", 'refresh');
                        }
                    }
                }
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->layout->render('back', 'admisi/pendaftar/create', $data);
        }
    }

    public function edit($id) {
        $this->otoritas->rule("U");
        $this->load->library('form_validation');
        if (!$id || empty($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect("admisi/pendaftar/page", 'refresh');
        }

        $data["title_panel"] = "Manajemen Pendaftar";
        $data["sub_title_panel"] = "Tambah Data";
        $data["title_box"] = "Tambah Pendaftar";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Pendaftar', site_url('admisi/pendaftar'));
        $this->breadcrumbs->add_crumb('Tambah Data');

        $data["get_semester"] = $this->the_m->getSemester()->result();
        $data["get_prodi"] = $this->the_m->getProdi()->result();
        $data["get_gelombang"] = $this->the_m->getGelombangAktif()->result();
        $data["get_agama"] = $this->the_m->getAgama()->result();
        $data["get_jenis_smta"] = $this->the_m->getJenisSmta()->result();
        $data["get_jurusan_smta"] = site_url('admisi/pendaftar/get_jurusan_smta');
        $data["get_propinsi"] = $this->the_m->getPropinsi()->result();
        $data["get_kota"] = site_url('admisi/pendaftar/get_kota');
        $data["get_sumber_informasi"] = $this->the_m->getSumberInformasi()->result();

        $get_data = $this->the_m->getDataById($id)->row();
        $get_data_alamat = $this->the_m->getDataAlamatById($id)->row();

        $this->form_validation->set_rules('mhsd_prodi_kode', 'Program Studi', 'required');
        $this->form_validation->set_rules('mhsd_gel_id', 'Gelombang', 'required');
        $this->form_validation->set_rules('mhsd_kelas', 'Kelas', 'required');
        $this->form_validation->set_rules('mhsd_status_daftar', 'Status Pendaftaran', 'required');
        $this->form_validation->set_rules('mhsd_nodin', 'Nomor Identitas', 'required');
        $this->form_validation->set_rules('mhsd_nama_lengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('mhsd_tmp_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('mhsd_tgl_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('mhsd_jsmta_id', 'Jenis Sekolah', 'required');
        $this->form_validation->set_rules('mhsd_nama_sekolah', 'Nama Sekolag', 'required');
        $this->form_validation->set_rules('mhsd_tahun_lulus', 'Tahun Lulus', 'required');
        $this->form_validation->set_rules('mhsd_nilai', 'NEM', 'required');
        $this->form_validation->set_rules('mhsalmt_alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('mhsalmt_kota_kode', 'Kegiatan/Kabupaten', 'required');
        $this->form_validation->set_rules('mhsalmt_kode_propinsi', 'Variabel', 'required');

        //Sumber Informasi
        $info = $this->input->post('mhsd_sinfo_id');
        if (isset($info) OR ! empty($info)) {
            $informasi = implode(",", $info);
        } else {
            $informasi = "NULL";
        }

        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $file = $_FILES['userfile']['name'];
                if (empty($file)) {
                    $update = array(
                        'mhsd_gel_id' => $this->input->post('mhsd_gel_id'),
                        'mhsd_prodi_kode' => $this->input->post('mhsd_prodi_kode'),
                        'mhsd_kelas' => $this->input->post('mhsd_kelas'),
                        'mhsd_warga_negara' => $this->input->post('mhsd_warga_negara'),
                        'mhsd_identitas' => $this->input->post('mhsd_identitas'),
                        'mhsd_nodin' => $this->input->post('mhsd_nodin'),
                        'mhsd_nama_lengkap' => $this->input->post('mhsd_nama_lengkap'),
                        'mhsd_tmp_lahir' => $this->input->post('mhsd_tmp_lahir'),
                        'mhsd_tgl_lahir' => $this->input->post('mhsd_tgl_lahir'),
                        'mhsd_jk' => $this->input->post('mhsd_jk'),
                        'mhsd_agm_id' => $this->input->post('mhsd_agm_id'),
                        'mhsd_status_nikah' => $this->input->post('mhsd_status_nikah'),
                        'mhsd_jsmta_id' => $this->input->post('mhsd_jsmta_id'),
                        'mhsd_nama_sekolah' => $this->input->post('mhsd_nama_sekolah'),
                        'mhsd_jursmta_id' => $this->input->post('mhsd_jursmta_id'),
                        'mhsd_tahun_lulus' => $this->input->post('mhsd_tahun_lulus'),
                        'mhsd_nilai' => $this->input->post('mhsd_nilai'),
                        'mhsd_tgl_ijasah' => $this->input->post('mhsd_tgl_ijasah'),
                        'mhsd_noijasah' => $this->input->post('mhsd_noijasah'),
                        'mhsd_notelp' => $this->input->post('mhsd_notelp'),
                        'mhsd_email' => $this->input->post('mhsd_email'),
                        'mhsd_website' => $this->input->post('mhsd_website'),
                        'mhsd_status_daftar' => $this->input->post('mhsd_status_daftar'),
                        'mhsd_foto' => 'no_image.jpg',
                        'mhsd_sinfo_id' => $informasi
                    );
                } else {
                    $path = dirname(BASEPATH) . DIRECTORY_SEPARATOR;
                    $config['upload_path'] = $path . 'Files/Foto/Mahasiswa';
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $config['overwrite'] = true;
                    $config['max_size'] = '250';
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload()) {
                        $data = $this->upload->data();
                        $update = array(
                            'mhsd_gel_id' => $this->input->post('mhsd_gel_id'),
                            'mhsd_prodi_kode' => $this->input->post('mhsd_prodi_kode'),
                            'mhsd_kelas' => $this->input->post('mhsd_kelas'),
                            'mhsd_warga_negara' => $this->input->post('mhsd_warga_negara'),
                            'mhsd_identitas' => $this->input->post('mhsd_identitas'),
                            'mhsd_nodin' => $this->input->post('mhsd_nodin'),
                            'mhsd_nama_lengkap' => $this->input->post('mhsd_nama_lengkap'),
                            'mhsd_tmp_lahir' => $this->input->post('mhsd_tmp_lahir'),
                            'mhsd_tgl_lahir' => $this->input->post('mhsd_tgl_lahir'),
                            'mhsd_jk' => $this->input->post('mhsd_jk'),
                            'mhsd_agm_id' => $this->input->post('mhsd_agm_id'),
                            'mhsd_status_nikah' => $this->input->post('mhsd_status_nikah'),
                            'mhsd_jsmta_id' => $this->input->post('mhsd_jsmta_id'),
                            'mhsd_nama_sekolah' => $this->input->post('mhsd_nama_sekolah'),
                            'mhsd_jursmta_id' => $this->input->post('mhsd_jursmta_id'),
                            'mhsd_tahun_lulus' => $this->input->post('mhsd_tahun_lulus'),
                            'mhsd_nilai' => $this->input->post('mhsd_nilai'),
                            'mhsd_tgl_ijasah' => $this->input->post('mhsd_tgl_ijasah'),
                            'mhsd_noijasah' => $this->input->post('mhsd_noijasah'),
                            'mhsd_notelp' => $this->input->post('mhsd_notelp'),
                            'mhsd_email' => $this->input->post('mhsd_email'),
                            'mhsd_website' => $this->input->post('mhsd_website'),
                            'mhsd_status_daftar' => $this->input->post('mhsd_status_daftar'),
                            'mhsd_foto' => $data['file_name'],
                            'mhsd_sinfo_id' => $informasi
                        );
                    } else {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                    }
                }
                $q = $this->the_m->update($id, $update);
                $this->the_m->updatePIS($id, $update);
                if ($q) {
                    $update_alamat = array(
                        'mhsalmt_jenis' => 'A',
                        'mhsalmt_alamat' => $this->input->post('mhsalmt_alamat'),
                        'mhsalmt_kecamatan' => $this->input->post('mhsalmt_kecamatan'),
                        'mhsalmt_kode_propinsi' => $this->input->post('mhsalmt_kode_propinsi'),
                        'mhsalmt_kota_kode' => $this->input->post('mhsalmt_kota_kode'),
                        'mhsalmt_kode_pos' => $this->input->post('mhsalmt_kode_pos')
                    );
                    $q = $this->the_m->updateAlamat($id, $update_alamat);
                }
                if ($q) {
                    $this->session->set_flashdata('success', 'Data Berhasil Dirubah');
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->errors());
                }
                redirect("admisi/pendaftar/page", 'refresh');                
            }
        }
        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $data['update'] = $get_data;
        $data['alamat'] = $get_data_alamat;
        $this->layout->render('back', 'admisi/pendaftar/edit', $data);
    }

    public function detail($id) {
        $this->otoritas->rule("R");
        $data["title_panel"] = "Manajemen Pendaftar";
        $data["sub_title_panel"] = "Detail Data";
        $data["title_box"] = "Detail Pendaftar";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Manajemen Pendaftar', site_url('admisi/pendaftar'));
        $this->breadcrumbs->add_crumb('Detail Data');

        $detail = $this->the_m->getDetailData($id)->row();
        $alamat = $this->the_m->getDetailAlamat($id)->row();
        $informasi = $this->the_m->getSumberInformasiByID($detail->mhsd_sinfo_id)->result();
        $info = array();
        foreach ($informasi as $row) {
            $info[] = $row->sinfo_nama;
        }

        $data["info"] = implode(', ', $info);

        //Identitas
        if ($detail->mhsd_identitas == 'K') {
            $identitas = 'KTP';
        } else if ($detail->mhsd_identitas == 'S') {
            $identitas = 'SIM';
        } else if ($detail->mhsd_identitas == 'P') {
            $identitas = 'Passpor';
        } else if ($detail->mhsd_identitas == 'L') {
            $identitas = 'Lainnya';
        }
        $data["identitas"] = $identitas;

        //Nomor Identitas
        if ($detail->mhsd_nodin == '0') {
            $nodin = '00';
        } else {
            $nodin = $detail->mhds_nodin;
        }
        $data["nodin"] = $nodin;

        // Nomor Telepon
        if ($detail->mhsd_notelp == '0') {
            $telepon = '00';
        } else {
            $telepon = $detail->mhsd_notelp;
        }
        $data["telepon"] = $telepon;

        //Jenis Kelamin
        if ($detail->mhsd_jk == 'L') {
            $jk = 'Laki-Laki';
        } else if ($detail->mhsd_jk == 'P') {
            $jk = 'Perempuan';
        }
        $data["jk"] = $jk;

        // Status Nikah
        if ($detail->mhsd_status_nikah == 'K') {
            $nikah = 'Menikah';
        } else if ($detail->mhsd_status_nikah == 'S') {
            $nikah = 'Single/Belum Menikah';
        } else if ($detail->mhsd_status_nikah == 'J') {
            $nikah = 'Jand';
        } else if ($detail->mhsd_status_nikah == 'D') {
            $nikah = 'Duda';
        } else {
            $nikah = '-';
        }
        $data["nikah"] = $nikah;

        // Status Ujian
        if ($detail->mhsd_status_ujian == '0') {
            $ujian = 'Belum Ujian';
        } else if ($detail->mhsd_status_ujian == '1') {
            $ujian = 'Tidak Lulus';
        } else if ($detail->mhsd_status_ujian == '2') {
            $ujian = 'Lulus';
        }
        $data["ujian"] = $ujian;

        // Status Daftar
        if ($detail->mhsd_status_daftar == '0') {
            $daftar = 'Offline';
        } else if ($detail->mhsd_status_daftar == '1') {
            $daftar = 'Online';
        } else if ($detail->mhsd_status_daftar == '2') {
            $daftar = 'Kolektif';
        }
        $data["daftar"] = $daftar;

        // Status Registrasi
        if ($detail->mhsd_status_registrasi == '0') {
            $registrasi = 'Belum Registrasi';
        } else if ($detail->mhsd_status_registrasi == '1') {
            $registrasi = 'Registrasi';
        }
        $data["registrasi"] = $registrasi;

        // Kelas
        if ($detail->mhsd_kelas == '1') {
            $kelas = 'Pagi';
        } else if ($detail->mhsd_kelas == '2') {
            $kelas = 'Sore';
        }
        $data["kelas"] = $kelas;

        // Tahun Akademik
        $kdSmt = substr($detail->smt_kode, -1);
        if ($kdSmt == '1') {
            $jenis = 'Ganjil';
        } else if ($kdSmt == '2') {
            $jenis = 'Genap';
        }
        $tahun = $jenis . ' - ' . $detail->smt_tahun . '/' . sprintf("%04s", $detail->smt_tahun + 1);
        $data["tahun"] = $tahun;

        //Program Studi
        $prodi = $detail->prodi_jenjang . ' - ' . $detail->prodi_nama;
        $data["prodi"] = $prodi;
        
        //Tanggal Ijasah
        if($detail->mhsd_tgl_ijasah == '0000-00-00' OR $detail->mhsd_tgl_ijasah = '') {
            $data["tgl_ijasah"] = '0000-00-00';
        } else {
            $data["tgl_ijasah"] = tanggal_indonesia($detail->mhsd_tgl_ijasah);
        }
            
        $data["detail"] = $detail;
        $data["alamat"] = $alamat;
        $this->layout->render('back', 'admisi/pendaftar/detail', $data);
    }

    public function status_lulus($id) {
        $this->otoritas->rule("U");
        $status = $this->the_m->getCurrentStatusLulus($id)->row();
        $data["id"] = $status->id;
        $data["status"] = $status->status;
        echo json_encode($data);
    }

    public function bayar($id) {
        $this->otoritas->rule("U");
        $nom = $this->the_m->getBiayaPendaftaranMhs($id)->row();
        if ($nom->nominal == '' OR $nom->nominal == 0 OR empty($nom->nominal)) {
            $jml_nom = 0;
        } else {
            $jml_nom = $nom->nominal;
        }
        $data["nodaf"] = $nom->id;
        $data["nominal"] = $jml_nom;
        echo json_encode($data);
    }

    public function status_registrasi($id) {
        $this->otoritas->rule("U");
        $status = $this->the_m->getCurrentStatusRegistrasi($id)->row();
        $data["idx"] = $status->id;
        $data["status"] = $status->status;
        echo json_encode($data);
    }

    public function simpan_status_lulus() {
        $this->otoritas->rule("U");
        $id = $this->input->post('id');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mhsd_status_ujian', 'Status Kelulusan', 'required');
        if ($this->form_validation->run() == TRUE) {
            $update = array(
                'mhsd_status_ujian' => $this->input->post('mhsd_status_ujian')
            );
            $q = $this->the_m->update($id, $update);
            if ($q) {
                $this->session->set_flashdata('success', 'Status Kelulusan Berhasil Dirubah');
                redirect("admisi/pendaftar/page", 'refresh');
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->layout->render('back', 'admisi/pendaftar/index', $data);
        }
    }

    public function simpan_pembayaran() {
        $this->otoritas->rule("U");
        $nodaf = $this->input->post('nodaf');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mhsd_biaya_daftar', 'Nominal', 'required');
        if ($this->form_validation->run() == TRUE) {
            $nom = $this->input->post('mhsd_biaya_daftar');
            if ($nom == '') {
                $biaya = 0;
            } else {
                $biaya = $nom;
            }
            $update = array(
                'mhsd_biaya_daftar' => $biaya
            );
            $q = $this->the_m->update($nodaf, $update);
            $this->the_m->updatePIS($nodaf, $update);
            if ($q) {
                $this->session->set_flashdata('success', 'Pembayaran Berhasil');
                redirect("admisi/pendaftar/page", 'refresh');
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->layout->render('back', 'admisi/pendaftar/index', $data);
        }
    }

    public function printcard($id) {
        $this->load->library('dompdf_gen');
        $print = $this->the_m->printKartuPendaftaran($id)->row();

        //Kelas
        if ($print->mhsd_kelas == '1') {
            $kelas = 'Pagi';
        } else if ($print->mhsd_kelas == '2') {
            $kelas = 'Sore';
        }
        $data["kelas"] = $kelas;
        $data["print"] = $print;
        $html = $this->layout->render('print', 'admisi/pendaftar/kartu_pendaftaran', $data, TRUE);
        $file = 'kartu_pendaftaran_' . $print->mhsd_nodaf;
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream($file . '.pdf', array("Attachment" => 0));
    }

    public function printkw($id) {
        $print = $this->the_m->printKartuPendaftaran($id)->row();
        $data["print"] = $print;
        $this->layout->render('print', 'admisi/pendaftar/kwitansi', $data);
    }

    public function simpan_status_reg() {
        $this->otoritas->rule("U");
        $id = $this->input->post('idx');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mhsd_status_registrasi', 'Status Registrasi', 'required');
        if ($this->form_validation->run() == TRUE) {
            $update2 = array(
                'mhsd_status_registrasi' => $this->input->post('mhsd_status_registrasi')
            );
            $q = $this->the_m->update($id, $update2);
            if ($q) {
                $this->session->set_flashdata('success', 'Status Registrasi Berhasil Dirubah');
            }
        }
        redirect("admisi/pendaftar/page", 'refresh');
    }

    public function multi_delete() {
        $this->otoritas->rule("D");
        $id = $this->input->post("del");
        $action = $this->input->post("action");
        if ($action == "select" && $id <> "") {
            $q = $this->the_m->multiDelete($id);
            if ($q) {
                $this->session->set_flashdata('success', 'Data Berhasil Dihapus');
            } else {
                $this->session->set_flashdata('error', 'Data Gagal Dihapus');
            }
        } else if ($action == "all") {
            $q = $this->the_m->multiDelete("");
            if ($q) {
                $this->session->set_flashdata('success', 'Semua Data Berhasil Dihapus');
            } else {
                $this->session->set_flashdata('error', 'Semua Data Gagal Dihapus');
            }
        } else {
            $this->session->set_flashdata('error', 'Belum ada data yang dipilih');
        }
        redirect("admisi/pendaftar/page", 'refresh');
    }

    public function get_gelombang() {
        $id = $this->input->post("data");
        $selected = $this->input->post("selected");
        $gelombang = $this->the_m->getGelombang($id)->result();
        $data = array();
        $i = 0;
        foreach ($gelombang as $row) {
            $data[$i]["id"] = $row->id;
            $data[$i]["nama"] = $row->nama;
            if ($row->id == $selected) {
                $data[$i]["selected"] = "selected";
            } else {
                $data[$i]["selected"] = "";
            }
            $i++;
        }
        echo json_encode($data);
    }

    public function get_jurusan_smta() {
        $id = $this->input->post("data");
        $selected = $this->input->post("selected");
        $jurusan = $this->the_m->getJurusanSmta($id)->result();
        $data = array();
        $i = 0;
        foreach ($jurusan as $row) {
            $data[$i]["id"] = $row->id;
            $data[$i]["nama"] = $row->nama;
            if ($row->id == $selected) {
                $data[$i]["selected"] = "selected";
            } else {
                $data[$i]["selected"] = "";
            }
            $i++;
        }
        echo json_encode($data);
    }

    public function get_kota() {
        $id = $this->input->post("data");
        $selected = $this->input->post("selected");
        $kota = $this->the_m->getKota($id)->result();
        $data = array();
        $i = 0;
        foreach ($kota as $row) {
            $data[$i]["id"] = $row->id;
            $data[$i]["nama"] = $row->nama;
            if ($row->id == $selected) {
                $data[$i]["selected"] = "selected";
            } else {
                $data[$i]["selected"] = "";
            }
            $i++;
        }
        echo json_encode($data);
    }

    function _nomor_referensi($length) {
        $random = "";
        srand((double) microtime() * 1000000);

        $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
        $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
        $data .= "0FGH45OP89";
        for ($i = 0; $i < $length; $i++) {
            $random .= substr($data, (rand() % (strlen($data))), 1);
        }
        return $random;
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}
