<?php

/**
 * Description of Kegiatan
 *
 * @author White Code
 */
class Kegiatan extends CI_Controller {

    var $limit = 30;

    public function __construct() {
        parent::__construct();
        $this->load->model('referensi/m_kegiatan', 'the_m');
        $this->load->library('sesfilter');
        $this->load->helper('filter');
        $this->sesfilter->setSesFilterName("referensi_kegiatan", "referensi_kegiatan_lock");
    }

    public function _generateTable($offset, $query) {
        $this->load->library('table');
        $tmpl = array('table_open' => '<table class="table table-bordered">');
        $this->table->set_template($tmpl);
        $this->table->set_heading(
            array(
                array("data" => 'No', "style" => "width: 10px"),
                'SKPD',
                'Kegiatan',
                'Tahun',
                'Status',
                array("data" => 'Aksi', "style" => "width: 75px")
            )
        );
        $i = $offset + 1;
        foreach ($query as $row) {
            $this->table->add_row($i, $row->skpd_nama, $row->keg_nama, $row->keg_tahun, $row->status_aktif, array('data' => "
                        <a title='Ubah' href='" . site_url("referensi/kegiatan/edit/" . $row->keg_id) . "' class='btn btn-xs btn-success'><i class='fa fa-pencil'></i></a>
                        <a title='Hapus' href='#' data-toggle='modal' data-target='#confirm-delete' data-href='" . site_url("referensi/kegiatan/delete/" . $row->keg_id) . "' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i></a>")
            );
            $i++;
        }
        if ($query == null) {
            $cell = array('data' => 'Tidak Ada Data', 'style' => 'text-align:center;', 'colspan' => 6);
            $this->table->add_row($cell);
        }
        return $this->table->generate();
    }

    public function filter() {
        $this->sesfilter->setFilterFromPost("fil_nama", "fil_nama");
        $this->sesfilter->setFilterFromPost("fil_skpd", "fil_skpd");
        redirect("referensi/kegiatan/page");
    }

    public function index() {
        $this->sesfilter->resetFilter();
        redirect("referensi/kegiatan/page");
    }

    public function page() {
        $this->otoritas->rule('R');
        $offset = $this->uri->segment(4, "0");
        $data["title_panel"] = "Referensi Kegiatan";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Referensi Kegiatan');

        $nama = $this->sesfilter->getFilter("fil_nama");
        $skpd = $this->sesfilter->getFilter("fil_skpd");

        $getAll = $this->the_m->getAllData($nama, $skpd, $this->limit, $offset)->result();
        $jumlah = $this->the_m->getAllData($nama, $skpd, "", "")->num_rows();
        $data["table"] = $this->_generateTable($offset, $getAll);
        $data["message"] = $this->_show_message();
        $data["nama"] = $nama;
        $data["skpd"] = $skpd;
        $data["jumlah"] = $jumlah;
        $data["link_tambah"] = site_url("referensi/kegiatan/add");
        $data["link_filter"] = site_url("referensi/kegiatan/filter");
        $data["link_import"] = site_url("referensi/kegiatan/import");
        $data["get_skpd"] = $this->the_m->getSkpd()->result();
        $data["pagination"] = _pagination($jumlah, $this->limit, "referensi/kegiatan/page", 4);
        $this->layout->render('back', 'referensi/kegiatan/index', $data);
    }

    public function add() {
        $this->otoritas->rule('C');
        $this->load->library('form_validation');
        $data["title_panel"] = "Referensi Kegiatan";
        $data["sub_title_panel"] = "Tambah Data";
        $data["title_box"] = "Tambah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard'));
        $this->breadcrumbs->add_crumb('Referensi Kegiatan', site_url('referensi/kegiatan'));
        $this->breadcrumbs->add_crumb('Tambah Data');

        $data["get_skpd"] = $this->the_m->getSkpd()->result();

        $this->form_validation->set_rules('keg_nama', 'Nama Kegiatan', 'required');
        $this->form_validation->set_rules('keg_skpd_id', 'SKPD', 'required');
        $this->form_validation->set_rules('keg_tahun', 'Tahun', 'required|max_length[4]');
        if ($this->form_validation->run() == TRUE) {
            $insert = array(
                'keg_nama' => $this->input->post('keg_nama'),
                'keg_skpd_id' => $this->input->post('keg_skpd_id'),
                'keg_tahun' => $this->input->post('keg_tahun'),
                'keg_status_aktif' => $this->input->post('keg_status_aktif')
            );
            $q = $this->the_m->create($insert);
            if ($q) {
                $this->session->set_flashdata('success', 'Data Berhasil Ditambah');
                redirect("referensi/kegiatan/page", 'refresh');
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->layout->render('back', 'referensi/kegiatan/create', $data);
        }
    }

    public function edit($id) {
        $this->otoritas->rule('U');
        $this->load->library('form_validation');
        // Jika ID tidak ditemukan
        if (!$id || empty($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect("referensi/kegiatan/page", 'refresh');
        }

        $data["title_panel"] = "Referensi Kegiatan";
        $data["sub_title_panel"] = "Ubah Data";
        $data["title_box"] = "Ubah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard'));
        $this->breadcrumbs->add_crumb('Referensi Kegiatan', site_url('referensi/kegiatan'));
        $this->breadcrumbs->add_crumb('Ubah Data');

        $get_data = $this->the_m->get_data_by_id($id)->row();
        $data["get_skpd"] = $this->the_m->getSkpd()->result();

        $this->form_validation->set_rules('keg_nama', 'Nama Kegiatan', 'required');
        $this->form_validation->set_rules('keg_skpd_id', 'SKPD', 'required');
        $this->form_validation->set_rules('keg_tahun', 'Tahun', 'required|max_length[4]');
        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $update = array(
                    'keg_nama' => $this->input->post('keg_nama'),
                    'keg_skpd_id' => $this->input->post('keg_skpd_id'),
                    'keg_tahun' => $this->input->post('keg_tahun'),
                    'keg_status_aktif' => $this->input->post('keg_status_aktif')
                );
                $q = $this->the_m->update($id, $update);
                if ($q) {
                    $this->session->set_flashdata('success', 'Data Berhasil Diupdate');
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->errors());
                }
                redirect("referensi/kegiatan/page", 'refresh');
            }
        }

        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $data['update'] = $get_data;
        $this->layout->render('back', 'referensi/kegiatan/edit', $data);
    }

    public function delete($id) {
        $this->otoritas->rule('D');
        // Jika ID tidak ditemukan
        if (!$id || empty($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect("referensi/kegiatan/page", 'refresh');
        }
        $q = $this->the_m->delete($id);
        if ($q) {
            $this->session->set_flashdata('success', 'Data Berhasil Dihapus');
        } else {
            $this->session->set_flashdata('error', 'Data Gagal Dihapus');
        }
        redirect("referensi/kegiatan/page", 'refresh');
    }

    public function import() {
        $this->load->library("phpexcel");
        $this->otoritas->rule('C');

        $path = dirname(BASEPATH) . DIRECTORY_SEPARATOR;
        $config['upload_path'] = $path . 'Files/template/';
        $config['allowed_types'] = 'xls|csv';
        $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file')) {
            $data = $this->upload->data();
            $objReader = new PHPExcel_Reader_Excel5();
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($path . 'Files/template/' . $data['file_name']);
            $sheet = $objPHPExcel->getActiveSheet();
            $rowIterator = $sheet->getRowIterator();
            $rowSkip = 4;
            foreach ($rowIterator as $row) {
                if ($row->getRowIndex() < $rowSkip)
                    continue;
                $rowIndex = $row->getRowIndex();

                $cell = $sheet->getCell('B' . $rowIndex);
                $opd = $cell->getCalculatedValue();

                $cell = $sheet->getCell('C' . $rowIndex);
                $tahun = $cell->getCalculatedValue();

                $cell = $sheet->getCell('D' . $rowIndex);
                $kegiatan = $cell->getCalculatedValue();

                $insert = array(
                    'keg_skpd_id' => $opd,
                    'keg_tahun' => $tahun,
                    'keg_nama' => $kegiatan
                );
                $q = $this->the_m->create($insert);
                if ($q) {
                    $this->session->set_flashdata('success', 'Data Berhasil Diimport');
                } else {
                    $this->session->set_flashdata('error', 'Data Gagal Diimport');
                }                    
            }                
        } else {
            $this->session->set_flashdata('error', $this->upload->display_errors());
        }
        redirect("referensi/kegiatan/page", 'refresh');
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}
