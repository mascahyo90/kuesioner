<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Variabel
 *
 * @author My Computer
 */
class Variabel extends CI_Controller {

    var $limit = 20;

    public function __construct() {
        parent::__construct();
        $this->load->model('referensi/m_variabel', 'the_m');
        $this->load->library('sesfilter');
        $this->load->helper('filter');
        $this->sesfilter->setSesFilterName("referensi_variabel", "referensi_variabel_lock");
    }

    public function filter() {
        $this->sesfilter->setFilterFromPost("fil_nama", "fil_nama");
        redirect("referensi/variable/page");
    }

    public function index() {
        $this->sesfilter->resetFilter();
        redirect("referensi/variabel/page");
    }

    public function page() {
        $this->otoritas->rule('R');
        $offset = $this->uri->segment(4, "0");
        $data["title_panel"] = "Referensi Variabel";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Referensi Variabel');

        $nama = $this->sesfilter->getFilter("fil_nama");

        $getAll = $this->the_m->getAllData($nama, $this->limit, $offset)->result();
        $jumlah = $this->the_m->getAllData($nama, "", "")->num_rows();
        $jmlBobot = $this->the_m->getJumlahBobot()->row();

        if($jmlBobot->var_bobot == 1 OR $jmlBobot->var_bobot == 100) {
            $data["disabled"] = 'style="display:none"';
        } else {
            $data["disabled"] = '';
        }

        $data["table"] = $this->_generateTable($offset, $getAll);
        $data["nama"] = $nama;
        $data["jumlah"] = $jumlah;
        $data["message"] = $this->_show_message();
        $data["link_filter"] = site_url("referensi/variabel/filter");
        $data["link_tambah"] = site_url("referensi/variabel/add");
        $data["pagination"] = _pagination($jumlah, $this->limit, "referensi/variabel/page", 4);
        $this->layout->render('back', 'referensi/variabel/index', $data);
    }

    public function _generateTable($offset, $query) {
        $this->load->library('table');
        $tmpl = array('table_open' => '<table class="table table-bordered table-condensed">');
        $this->table->set_template($tmpl);
        $this->table->set_heading(
                array(
                    array("data" => 'No', "style" => "width: 10px"),
                    'Variabel',
                    'Bobot',
                    array("data" => 'Aksi', "style" => "width: 75px")
                )
        );
        $i = $offset + 1;
        $total = 0;
        foreach ($query as $row) {
            $this->table->add_row($i, $row->var_nama, array("data" => $row->var_bobot, "style" => "text-align: right"), array('data' => "
                        <a title='Ubah' href='" . site_url("referensi/variabel/edit/" . $row->var_id) . "' class='btn btn-xs btn-success'><i class='fa fa-pencil'></i></a>
                        <a title='Hapus' href='#' data-toggle='modal' data-target='#confirm-delete' data-href='" . site_url("referensi/variabel/delete/" . $row->var_id) . "' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i></a>")
            );
            $i++;
            $total += $row->var_bobot;
        }
        $tot_bobot = $total;

        $this->table->add_row(
            array("data" => "Total", "colspan" => 2, "style" => "text-align: right; font-weight: bold"),
            array("data" => $tot_bobot, "style" => "text-align: right"),
            '&nbsp;',
            '&nbsp;'
        );

        if ($query == null) {
            $cell = array('data' => 'Tidak Ada Data', 'style' => 'text-align:center;', 'colspan' => 4);
            $this->table->add_row($cell);
        }
        return $this->table->generate();
    }

    public function add() {
        $this->otoritas->rule('C');
        $this->load->library('form_validation');
        $data["title_panel"] = "Referensi Variabel";
        $data["sub_title_panel"] = "Tambah Data";
        $data["title_box"] = "Tambah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Referensi Variabel', site_url('referensi/variabel'));
        $this->breadcrumbs->add_crumb('Tambah Data');

        $this->form_validation->set_rules('var_nama', 'Variabel', 'required');
        $this->form_validation->set_rules('var_bobot', 'Bobot', 'required');
        if ($this->form_validation->run() == TRUE) {
            $insert = array(
                'var_nama' => $this->input->post('var_nama'),
                'var_bobot' => $this->input->post('var_bobot')
            );
            $q = $this->the_m->create($insert);
            if ($q) {
                $this->session->set_flashdata('success', 'Data Berhasil Ditambah');
                redirect("referensi/variabel/page", 'refresh');
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->layout->render('back', 'referensi/variabel/create', $data);
        }
    }

    public function edit($id) {
        $this->otoritas->rule('U');
        $this->load->library('form_validation');
        if (!$id || empty($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect("referensi/variabel/page", 'refresh');
        }

        $data["title_panel"] = "Referensi Variabel";
        $data["sub_title_panel"] = "Ubah Data";
        $data["title_box"] = "Ubah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Referensi Variabel', site_url('referensi/variabel'));
        $this->breadcrumbs->add_crumb('Ubah Data');

        $get_data = $this->the_m->getDataById($id)->row();

        $this->form_validation->set_rules('var_nama', 'Variabel', 'required');
        $this->form_validation->set_rules('var_bobot', 'Bobot', 'required');
        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $update = array(
                    'var_nama' => $this->input->post('var_nama'),
                    'var_bobot' => $this->input->post('var_bobot')
                );
                $q = $this->the_m->update($id, $update);
                if ($q) {
                    $this->session->set_flashdata('success', 'Data Berhasil Diupdate');
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->errors());
                }
                redirect("referensi/variabel/page", 'refresh');
            }
        }

        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $data['update'] = $get_data;
        $this->layout->render('back', 'referensi/variabel/edit', $data);
    }

    public function delete($id) {
        $this->otoritas->rule('D');
        if (!$id || empty($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect("referensi/variabel/page", 'refresh');
        }
        $q = $this->the_m->delete($id);
        if ($q) {
            $this->session->set_flashdata('success', 'Data Berhasil Dihapus');
        } else {
            $this->session->set_flashdata('error', 'Data Gagal Dihapus');
        }
        redirect("referensi/variabel/page", 'refresh');
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}
