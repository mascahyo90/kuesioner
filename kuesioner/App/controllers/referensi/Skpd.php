<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Skpd
 *
 * @author My Computer
 */
class Skpd extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('referensi/m_skpd', 'the_m');
        $this->load->library('sesfilter');
        $this->load->helper('filter');
        $this->sesfilter->setSesFilterName("referensi_skpd", "referensi_skpd_lock");
    }

    public function filter() {
        $this->sesfilter->setFilterFromPost("fil_nama", "fil_nama");
        redirect("referensi/skpd/page");
    }

    public function index() {
        $this->sesfilter->resetFilter();
        redirect("referensi/skpd/page");
    }

    public function page() {
        $this->otoritas->rule('R');
        $data["title_panel"] = "Referensi OPD";
        $data["sub_title_panel"] = "";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Referensi OPD');

        $nama = $this->sesfilter->getFilter("fil_nama");

        $getAll = $this->the_m->getAllData($nama)->result();
        $jumlah = $this->the_m->getAllData($nama)->num_rows();
        $data["table"] = $this->_generateTable($getAll);
        $data["skpd"] = $nama;
        $data["jumlah"] = $jumlah;
        $data["message"] = $this->_show_message();
        $data["link_filter"] = site_url("referensi/skpd/filter");
        $data["link_tambah"] = site_url("referensi/skpd/add");
        $this->layout->render('back', 'referensi/skpd/index', $data);
    }

    public function _generateTable($query) {
        $this->load->library('table');
        $tmpl = array('table_open' => '<table class="table table-bordered table-condensed">');
        $this->table->set_template($tmpl);
        $this->table->set_heading(
                array(
                    array("data" => 'No', "style" => "width: 10px"),
                    'Nama SKPD',
                    array("data" => 'Aksi', "style" => "width: 75px")
                )
        );
        $i = 1;
        foreach ($query as $row) {
            $this->table->add_row($i, $row->skpd_nama, array('data' => "
                        <a title='Ubah' href='" . site_url("referensi/skpd/edit/" . $row->skpd_id) . "' class='btn btn-xs btn-success'><i class='fa fa-pencil'></i></a>
                        <a title='Hapus' href='#' data-toggle='modal' data-target='#confirm-delete' data-href='" . site_url("referensi/skpd/delete/" . $row->skpd_id) . "' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i></a>")
            );
            $i++;
        }
        if ($query == null) {
            $cell = array('data' => 'Tidak Ada Data', 'style' => 'text-align:center;', 'colspan' => 5);
            $this->table->add_row($cell);
        }
        return $this->table->generate();
    }

    public function add() {
        $this->otoritas->rule('C');
        $this->load->library('form_validation');
        $data["title_panel"] = "Referensi OPD";
        $data["sub_title_panel"] = "Tambah Data";
        $data["title_box"] = "Tambah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Referensi OPD', site_url('referensi/skpd'));
        $this->breadcrumbs->add_crumb('Tambah Data');

        $this->form_validation->set_rules('skpd_nama', 'SKPD', 'required');
        if ($this->form_validation->run() == TRUE) {
            $insert = array(
                'skpd_nama' => $this->input->post('skpd_nama')
            );
            $q = $this->the_m->create($insert);
            if ($q) {
                $this->session->set_flashdata('success', 'Data Berhasil Ditambah');
                redirect("referensi/skpd/page", 'refresh');
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->layout->render('back', 'referensi/skpd/create', $data);
        }
    }

    public function edit($id) {
        $this->otoritas->rule('U');
        $this->load->library('form_validation');
        if (!$id || empty($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect("referensi/skpd/page", 'refresh');
        }

        $data["title_panel"] = "Referensi OPD";
        $data["sub_title_panel"] = "Ubah Data";
        $data["title_box"] = "Ubah";
        $this->breadcrumbs->clear();
        $this->breadcrumbs->add_crumb('Dashboard', site_url('dashboard/dashboard'));
        $this->breadcrumbs->add_crumb('Referensi OPD', site_url('referensi/skpd'));
        $this->breadcrumbs->add_crumb('Ubah Data');

        $get_data = $this->the_m->getDataById($id)->row();

        $this->form_validation->set_rules('skpd_nama', 'SKPD', 'required');
        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $update = array(
                    'skpd_nama' => $this->input->post('skpd_nama')
                );
                $q = $this->the_m->update($id, $update);
                if ($q) {
                    $this->session->set_flashdata('success', 'Data Berhasil Diupdate');
                } else {
                    $this->session->set_flashdata('error', $this->ion_auth->errors());
                }
                redirect("referensi/skpd/page", 'refresh');
            }
        }

        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $data['update'] = $get_data;
        $this->layout->render('back', 'referensi/skpd/edit', $data);
    }

    public function delete($id) {
        $this->otoritas->rule('D');
        if (!$id || empty($id)) {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect("referensi/skpd/page", 'refresh');
        }
        $q = $this->the_m->delete($id);
        if ($q) {
            $this->session->set_flashdata('success', 'Data Berhasil Dihapus');
        } else {
            $this->session->set_flashdata('error', 'Data Gagal Dihapus');
        }
        redirect("referensi/skpd/page", 'refresh');
    }

    function _show_message() {
        $notifForm = "";
        if ($this->session->flashdata('error') != "") {
            $notifForm .= '<div class="alert alert-danger alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('error');
            $notifForm .= '</div>';
        } else if ($this->session->flashdata('success') != "") {
            $notifForm .= '<div class="alert alert-success alert-dismissable">';
            $notifForm .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            $notifForm .= $this->session->flashdata('success');
            $notifForm .= '</div>';
        }
        return $notifForm;
    }

}
