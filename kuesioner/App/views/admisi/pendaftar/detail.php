<style>
    .table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
        padding: 2px 2px 2px 8px;
    }
    .bg-detail-daftar {
        background: #FFFBEC;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $title_box; ?></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-condensed table-bordered bg-detail-daftar">
                                <tr>
                                    <th colspan="2" style="background-color: #F9E69B;">PENDAFTARAN</th>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Foto</th>
                                    <td>
                                        <img width="113" height="150" src="<?php echo base_url();?>Files/Foto/Mahasiswa/<?php if (!empty($detail->mhsd_foto)) {echo $detail->mhsd_foto;}?>">
                                    </td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Tahun Akademik</th>
                                    <td><?php if (!empty($tahun)) {echo $tahun;}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Angkatan</th>
                                    <td><?php if (!empty($detail->smt_tahun)) {echo $detail->smt_tahun;}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Program Studi</th>
                                    <td><?php if (!empty($prodi)) {echo $prodi;}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">No. Pendaftaran</th>
                                    <td><?php if (!empty($detail->mhsd_nodaf)) {echo $detail->mhsd_nodaf;}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">No. Referensi</th>
                                    <td><?php if (!empty($detail->mhsd_noref)) {echo $detail->mhsd_noref;}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Jenis Pendaftaran</th>
                                    <td><?php if (!empty($daftar)) {echo $daftar;}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Gelombang</th>
                                    <td><?php if (!empty($detail->jgel_nama)) {echo $detail->jgel_nama;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Kelas</th>
                                    <td><?php if (!empty($kelas)) {echo $kelas;}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Tanggal Daftar</th>
                                    <td><?php if (!empty(tanggal_indonesia($detail->mhsd_tgl_daftar))) {echo tanggal_indonesia($detail->mhsd_tgl_daftar);}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Tanggal Ujian</th>
                                    <td><?php if (!empty(tanggal_indonesia($detail->mhsd_tgl_ujian))) {echo tanggal_indonesia($detail->mhsd_tgl_ujian);}?></td>
                                </tr> 
                                <tr>
                                    <th style="width: 180px;">Status Ujian</th>
                                    <td><?php if (!empty($ujian)) {echo $ujian;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Status Registrasi</th>
                                    <td><?php if (!empty($registrasi)) {echo $registrasi;}?></td>
                                </tr> 
                                <tr>
                                    <th colspan="2" style="background-color: #F9E69B;">BIODATA</th>
                                </tr>  
                                <tr>
                                    <th style="width: 180px;">Nama Lengkap</th>
                                    <td><?php if (!empty($detail->mhsd_nama_lengkap)) {echo $detail->mhsd_nama_lengkap;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Tempat Lahir</th>
                                    <td><?php if (!empty($detail->mhsd_tmp_lahir)) {echo $detail->mhsd_tmp_lahir;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Tanggal Lahir</th>
                                    <td><?php if (!empty(tanggal_indonesia($detail->mhsd_tgl_lahir))) {echo tanggal_indonesia($detail->mhsd_tgl_lahir);}?></td>
                                </tr> 
                                <tr>
                                    <th style="width: 180px;">Jenis Kelamin</th>
                                    <td><?php if (!empty($jk)) {echo $jk;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Kewarganegaraan</th>
                                    <td><?php if (!empty($detail->mhsd_warga_negara)) {echo $detail->mhsd_warga_negara;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Identitas</th>
                                    <td><?php if (!empty($identitas)) {echo $identitas;}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Nomor Identitas</th>
                                    <td><?php if (!empty($nodin)) {echo $nodin;}?></td>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Status Pernikahan</th>
                                    <td><?php if (!empty($nikah)) {echo $nikah;}?></td>
                                </tr> 
                                <tr>
                                    <th style="width: 180px;">Agama</th>
                                    <td><?php if (!empty($detail->agm_nama)) {echo $detail->agm_nama;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">No. Telepon</th>
                                    <td><?php if (!empty($telepon)) {echo $telepon;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Email</th>
                                    <td><?php if (!empty($detail->mhsd_email)) {echo $detail->mhsd_email;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Website</th>
                                    <td><?php if (!empty($detail->mhsd_website)) {echo $detail->mhsd_website;}?></td>
                                </tr> 
                                <tr>
                                    <th colspan="2" style="background-color: #F9E69B;">SMTA</th>
                                </tr> 
                                <tr>
                                    <th style="width: 180px;">Jenis SMTA</th>
                                    <td><?php if (!empty($detail->jsmta_nama)) {echo $detail->jsmta_nama;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Jurusan SMTA</th>
                                    <td><?php if (!empty($detail->jursmta_nama)) {echo $detail->jursmta_nama;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Nama SMTA</th>
                                    <td><?php if (!empty($detail->mhsd_nama_sekolah)) {echo $detail->mhsd_nama_sekolah;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Tahun Lulus</th>
                                    <td><?php if (!empty($detail->mhsd_tahun_lulus)) {echo $detail->mhsd_tahun_lulus;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Nilai</th>
                                    <td><?php if (!empty($detail->mhsd_nilai)) {echo $detail->mhsd_nilai;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Tanggal Ijasah</th>
                                    <td><?php echo $tgl_ijasah;?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Nomor Ijasah</th>
                                    <td><?php if (!empty($detail->mhsd_noijasah)) {echo $detail->mhsd_noijasah;}?></td>
                                </tr>  
                                <tr>
                                    <th colspan="2" style="background-color: #F9E69B;">ALAMAT</th>
                                </tr> 
                                <tr>
                                    <th style="width: 180px;">Alamat</th>
                                    <td><?php if (!empty($alamat->mhsalmt_alamat)) {echo $alamat->mhsalmt_alamat;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Kecamatan</th>
                                    <td><?php if (!empty($alamat->mhsalmt_kecamatan)) {echo $alamat->mhsalmt_kecamatan;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Kota/Kabupaten</th>
                                    <td><?php if (!empty($alamat->kota_nama)) {echo $alamat->kota_nama;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Propinsi</th>
                                    <td><?php if (!empty($alamat->nama_propinsi)) {echo $alamat->nama_propinsi;}?></td>
                                </tr>                                
                                <tr>
                                    <th style="width: 180px;">Kode Pos</th>
                                    <td><?php if (!empty($alamat->mhsalmt_kode_pos)) {echo $alamat->mhsalmt_kode_pos;}?></td>
                                </tr> 
                                <tr>
                                    <th colspan="2" style="background-color: #F9E69B;">INFORMASI</th>
                                </tr>
                                <tr>
                                    <th style="width: 180px;">Sumber Informasi</th>
                                    <td><?php if (!empty($info)) {echo $info;}?></td>
                                </tr>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>