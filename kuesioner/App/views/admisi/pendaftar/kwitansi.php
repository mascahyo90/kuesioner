<table width="600px">
    <tr>
        <td style="width: 10px;">
            <img src="<?php echo base_url(); ?>Assets/v1/bo/img/logo_amikom.jpg" />				
        </td>
        <td style="line-height: 2.5px;font-weight: bold;text-align: center;">
            <p style="font-size: 10px;">AKADEMI MANAJEMEN INFORMATIKA DAN KOMPUTER</p>
            <p style="font-size: 15px;">AMIKOM CIPTA DARMA SURAKARTA</p>
            <p style="font-size: 10px;">Jl. Veteran, Notosuman, Singopura, Kartasura, Sukoharjo. Telp: (0271) 7811501, 7851507</p>
        </td>
    </tr>
</table>
<table width="600px" style="border-top: 2px solid #555;margin-bottom: 5px;"></table>
<table width="600px" style="font-size: 13px;">
    <tr>
        <td colspan="2" style="font-weight: bold;text-align: center;font-size: 14px;"><u>KWITANSI PENDAFTARAN</u></td>
    </tr>
    <tr></tr><tr></tr><tr></tr>
    <tr>
        <td width="30%">Sudah diterima dari</td>			
        <td>: <b><?php echo $print->mhsd_nama_lengkap; ?></b></td>
    </tr>
    <tr>
        <td width="30%">Nomor pendaftaran</td>			
        <td>: <b><?php echo $print->mhsd_nodaf; ?></b></td>
    </tr>
    <tr>
        <td width="30%">Jumlah uang</td>			
        <td>: <?php echo "Rp " . number_format(100000, 2, ',', '.'); ?></td>
    </tr>
    <tr>
        <td width="30%">Untuk pembayaran</td>
        <td>: Biaya Pendaftaran Mahasiswa Baru TA <?php echo get_tahun_semester(); ?></td>
    </tr>
    <tr>
        <td width="30%">Status Tes</td>			
        <td>: <i>LULUS / TIDAK LULUS</i></td>
    </tr>
</table>
<br>
<table width="600px" style="text-align: right;font-size: 12px;">
    <tr>
        <td colspan="3">
            Surakarta, <?php echo tanggal_indonesia(date('Y-m-d')); ?> <br />
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin-right: 45px;margin-top: 0;margin-bottom: 0">Pendaftar</p>
            <br><br><br>
            <p style="margin-right: 20px;margin-top: 0">(............................)</p>
        </td>
        <td>
            <p style="margin-right: 20px;margin-top: 0;margin-bottom: 0">Panitia Pendaftaran</p>
            <br><br><br>
            <p style="margin-right: 20px;margin-top: 0">(............................)</p>
        </td>
        <td>
            <p style="margin-right: 33px;margin-top: 0;margin-bottom: 0">Petugas Ujian</p>
            <br><br><br>
            <p style="margin-right: 20px;margin-top: 0">(............................)</p> 
        </td>
    </tr>
</table>
<hr style="width: 600px;float: left;">
<br>
<table width="600px">
    <tr>
        <td style="width: 10px;">
            <img src="<?php echo base_url(); ?>Assets/v1/bo/img/logo_amikom.jpg" />				
        </td>
        <td style="line-height: 2.5px;font-weight: bold;text-align: center;">
            <p style="font-size: 10px;">AKADEMI MANAJEMEN INFORMATIKA DAN KOMPUTER</p>
            <p style="font-size: 15px;">AMIKOM CIPTA DARMA SURAKARTA</p>
            <p style="font-size: 10px;">Jl. Veteran, Notosuman, Singopura, Kartasura, Sukoharjo. Telp: (0271) 7811501, 7851507</p>
        </td>
    </tr>
</table>
<table width="600px" style="border-top: 2px solid #555;margin-bottom: 5px;"></table>
<table width="600px" style="font-size: 13px;">
    <tr>
        <td colspan="2" style="font-weight: bold;text-align: center;font-size: 14px;"><u>KWITANSI PENDAFTARAN</u></td>
    </tr>
    <tr></tr><tr></tr><tr></tr>
    <tr>
        <td width="30%">Sudah diterima dari</td>			
        <td>: <b><?php echo $print->mhsd_nama_lengkap; ?></b></td>
    </tr>
    <tr>
        <td width="30%">Nomor pendaftaran</td>			
        <td>: <b><?php echo $print->mhsd_nodaf; ?></b></td>
    </tr>
    <tr>
        <td width="30%">Jumlah uang</td>			
        <td>: <?php echo "Rp " . number_format(100000, 2, ',', '.'); ?></td>
    </tr>
    <tr>
        <td width="30%">Untuk pembayaran</td>
        <td>: Biaya Pendaftaran Mahasiswa Baru TA <?php echo get_tahun_semester(); ?></td>
    </tr>
    <tr>
        <td width="30%">Status Tes</td>			
        <td>: <i>LULUS / TIDAK LULUS</i></td>
    </tr>
</table>
<br>
<table width="600px" style="text-align: right;font-size: 12px;">
    <tr>
        <td colspan="3">
            Surakarta, <?php echo tanggal_indonesia(date('Y-m-d')); ?> <br />
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin-right: 45px;margin-top: 0;margin-bottom: 0">Pendaftar</p>
            <br><br><br>
            <p style="margin-right: 20px;margin-top: 0">(............................)</p>
        </td>
        <td>
            <p style="margin-right: 20px;margin-top: 0;margin-bottom: 0">Panitia Pendaftaran</p>
            <br><br><br>
            <p style="margin-right: 20px;margin-top: 0">(............................)</p>
        </td>
        <td>
            <p style="margin-right: 33px;margin-top: 0;margin-bottom: 0">Petugas Ujian</p>
            <br><br><br>
            <p style="margin-right: 20px;margin-top: 0">(............................)</p> 
        </td>
    </tr>
</table>
<br /><br /><a href='javascript:window.print()'>[Cetak]</a>