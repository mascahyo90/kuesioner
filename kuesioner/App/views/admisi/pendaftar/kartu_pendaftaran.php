<style>
    .cetak {
        width: 643px;
    }
    table.satu {
        border-collapse: collapse;
        font-family: arial;
        border-bottom: 2px solid #333;
        margin-bottom: 3px;
        width: 643px;
    }
    table.dua {
        border-collapse: collapse;
        font-family: arial;
        font-size: 14px;
        width: 643px;
    }
    table.tiga {
        border: 1px solid #333;
        font-family: arial;
        font-size: 14px;
        width: 250px;
        margin-bottom: 10px;
    }
    table.empat {
        border-collapse: collapse;
        font-family: arial;
        font-size: 14px;
        width: 643px;
        font-weight: bold;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    table.lima {
        border-collapse: collapse;
        font-family: arial;
        font-size: 13px;
        width: auto;
        line-height: 17px;
    }
    table.enam {
        margin-top: 10px;
        font-family: arial;
        font-size: 13px;
        width: auto;
    }
    table.tujuh {
        margin-top: 10px;
        font-family: arial;
        font-size: 13px;
        width: 643px;
        text-align: right;
    }
    .logo {
        text-align: left;
        width: 80px;
    }
    p.header-logo {
        font-size: 15px;
        font-weight: bold;
        margin-bottom: 5px;
    }
    p.sub-header-logo {
        margin-top: 0px;
        margin-bottom: 10px;
        font-size: 13px;
    }
</style>
<div class="cetak">
    <table class="satu">
        <tr>
            <td class="logo"><img src="<?php echo base_url(); ?>Assets/v1/bo/img/logo_amikom.png"></td>
            <td colspan="2">
                <p class="header-logo" >AMIK CIPTA DARMA SURAKARTA</p>
                <p class="sub-header-logo">Jalan Veteran, Notosuman, Singopuran, Kartasura, Surakarta<br>Telepon: (0271) 7851501,7851507, Website: www.amikomsolo.ac.id</p>
            </td>
        </tr>
    </table>
    <table class="dua">
        <tr>
            <td align="right">Nomor Pendaftaran / Gelombang</td>
        </tr>
    </table>
    <div style="margin-left: 395px;width: 240px;">
        <table class="tiga">
            <tr>
                <td align="center" style="font-weight: bold;"><?php echo $print->mhsd_nodaf; ?> / <?php echo $print->jgel_nama; ?></td>
            </tr>
        </table>
    </div>
    <table class="empat">
        <tr>
            <td align="center">KARTU PENDAFTARAN MAHASISWA BARU <?php echo $print->ta; ?></td>
        </tr>
    </table>
    <table class="lima">
        <tr>
            <td width="110px" rowspan="8">
                <img src="<?php echo base_url(); ?>Files/Foto/Mahasiswa/<?php echo $print->mhsd_foto; ?>" style="width: 88px;height: 103px;" /> 
            </td>
        </tr>
        <tr>
            <td>Nama Lengkap</td>
            <td>: <?php echo $print->mhsd_nama_lengkap; ?></td>
        </tr>
        <tr>
            <td>Nomor Referensi</td>
            <td>: <?php echo $print->mhsd_noref; ?></td>
        </tr>
        <tr>
            <td>Tanggal Pendaftaran</td>
            <td>: <?php echo tanggal_indonesia($print->mhsd_tgl_daftar); ?></td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>: <?php echo $print->prodi; ?></td>
        </tr>
        <tr>
            <td>Kelas</td>
            <td>: <?php echo $kelas; ?></td>
        </tr>
        <tr>
            <td>Jadwal Ujian</td>
            <td>: Maksimal <b><?php echo tanggal_indonesia($print->mhsd_tgl_ujian); ?></b></td>
        </tr>
    </table>
    <table class="enam">
        <tr>
            <td>
                <i><b>Catatan :</b></i>
                <ol>
                    <li>Lakukan pembayaran biaya pendaftaran sebesar <b>Rp. 100.000, 00</b>. Dapat dibayarkan melalui <b>Bank BRI SIMPANG KARTASURA, No. Rekening: 2097-01-000126-56-8 atas nama MOCH HARI PURWIDIANTORO ST MM QQ AMIK CIPTA DARMA </b></li>
                    <li>Cantumkan <b>nomor referensi</b> pada saat pembayaran. Contoh: <b>2TkKSQy - Anggit Puguh Setiawan.</b></li>
                    <li>Bawalah bukti pembayaran dari Bank dan Kartu Pendaftaran untuk di serahkan kepada <b>Panitia PMB AMIKOM Surakarta</b> saat akan mengikuti TES CBT.</li>
                    <li><b>Diharapkan untuk segera mengikuti tes sesuai tanggal tes terjadwal.</b></li>
                    <li>Jika jadwal tes jatuh pada hari Minggu, maka test akan dilaksanakan hari Senin.</li>
                </ol>
            </td>
        </tr>
    </table>
    <table class="tujuh">
        <tr>
            <td>
                Surakarta, <?php echo tanggal_indonesia(date("Y-m-d")); ?> <br />
                <p style="margin-right: 20px;margin-top: 0;margin-bottom: 0">Panitia Pendaftaran</p>
                <img src="<?php echo base_url(); ?>Assets/v1/bo/img/scan.png" style="margin-right: 25px;"/>
                <p style="margin-right: 35px;margin-top: 0">Yeni Fitriati</p>
            </td>
        </tr>
    </table>
</div>