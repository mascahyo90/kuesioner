<script type="text/javascript" src="<?php echo base_url(); ?>Assets/v1/bo/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/datepicker3.css">
<script src="<?php echo base_url(); ?>Assets/v1/bo/js/bootstrap-fileupload.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/bootstrap-fileupload.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/format.css">
<script type="text/javascript" src="<?php echo base_url(); ?>Assets/v1/bo/js/common.js"></script>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissable" <?php
            if (is_string($message)) {
                echo 'style="display:block; margin-bottom:7px;"';
            } else {
                echo 'style="display:none;"';
            }
            ?>>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                <?php echo $message; ?>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-plus-circle"></i> <?php echo $title_box; ?></h3>
                </div>
                <form action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="page-header">Kemahasiswaan</h2>                                
                                <div class="form-group">
                                    <label>Program Studi <sup style="color: red">*)</sup></label>
                                    <select name="mhsd_prodi_kode" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach ($get_prodi as $row): ?>
                                            <option value="<?php echo $row->id; ?>" <?php echo set_select('mhsd_prodi_kode', $row->id); ?>><?php echo $row->nama; ?></option>
                                        <?php endforeach; ?>
                                    </select>                                    
                                </div>
                                <div class="form-group">
                                    <label>Gelombang <sup style="color: red">*)</sup></label>
                                    <select name="mhsd_gel_id" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach ($get_gelombang as $row): ?>
                                            <option value="<?php echo $row->id; ?>" <?php echo set_select('mhsd_gel_id', $row->id); ?>><?php echo $row->nama; ?></option>
                                        <?php endforeach; ?>
                                    </select>                                    
                                </div>
                                <div class="form-group">
                                    <label>Kelas</label>
                                    <select name="mhsd_kelas" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="1">Pagi</option>
                                        <option value="2">Sore</option>
                                    </select>
                                </div>                                
                                <div class="form-group">
                                    <label>Tanggal Pendaftaran <sup style="color: red">*)</sup></label>
                                    <input id="tgl_daftar" type="text" name="mhsd_tgl_daftar" class="form-control" value="<?php echo date('Y-m-d'); ?>">
                                </div>
                                <div class="form-group">
                                    <label>Status Pendaftaran</label>
                                    <select name="mhsd_status_daftar" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="0">Offline</option>
                                        <option value="1">Online</option>
                                        <option value="2">Kolektif</option>
                                    </select>
                                </div>                                
                            </div>
                            <div class="col-md-6">
                                <h2 class="page-header">Foto</h2>
                                <div class="form-group">                                                
                                    <div class="text-center">
                                        <label>Foto</label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-preview thumbnail" style="width: 110px; height: 145px;"></div>
                                            <div>
                                                <span class="btn btn-file btn-success">
                                                    <span class="fileupload-new">Pilih</span>
                                                    <span class="fileupload-exists">Ganti</span>
                                                    <input id="id_foto" type="file" name="userfile" />
                                                </span>
                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Hapus</a>
                                            </div>
                                            <br />
                                            <div class="callout callout-success" style="padding:5px;">
                                                <p>Ukuran Foto Maksimal 250 KB <br>Kosongkan apabila foto tidak diganti</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h2 class="page-header">Biodata</h2>
                            </div>                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kewarganegaraan</label>
                                    <select name="mhsd_warga_negara" class="form-control">
                                        <option value="WNI">Indonesia</option>
                                        <option value="WNA">Asing</option>
                                    </select>
                                </div>                                
                                <div class="form-group">
                                    <label>Identitas</label>
                                    <select name="mhsd_identitas" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="K">KTP</option>
                                        <option value="S">SIM</option>
                                        <option value="P">Passpor</option>
                                        <option value="L">Lainnya</option>
                                    </select>
                                    <div class="callout callout-success" style="padding:5px;margin-top: 5px;">
                                        Pilih <b>Lainnya</b> apabila belum memiliki kartu identitas
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label>Nomor Identitas <sup style="color: red">*)</sup></label>
                                    <input type="text" name="mhsd_nodin" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_nodin'); ?>">
                                    <div class="callout callout-success" style="padding:5px;margin-top: 5px;">
                                        Isikan <b>00</b> jika belum memiliki nomor identitas
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label>Nama Lengkap <sup style="color: red">*)</sup></label>
                                    <input type="text" name="mhsd_nama_lengkap" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_nama_lengkap'); ?>">
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <select name="mhsd_jk" class="form-control">
                                        <option value="L">Laki-Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>                                                                
                            </div>
                            <div class="col-md-6">     
                                <div class="form-group">
                                    <label>Tempat Lahir <sup style="color: red">*)</sup></label>
                                    <input type="text" name="mhsd_tmp_lahir" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_tmp_lahir'); ?>">
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Lahir <sup style="color: red">*)</sup></label>
                                    <input type="text" name="mhsd_tgl_lahir" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_tgl_lahir'); ?>">
                                    <div class="callout callout-success" style="padding:5px;margin-top: 5px;">
                                        Format isian: <b>tahun-bulan-tanggal</b><br>
                                        Contoh isian: <b>1990-10-28</b>
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label>Agama</label>
                                    <select name="mhsd_agm_id" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach ($get_agama as $row): ?>
                                            <option value="<?php echo $row->id; ?>" <?php echo set_select('mhsd_agm_id', $row->id); ?>><?php echo $row->nama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status Nikah</label>
                                    <select name="mhsd_status_nikah" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="S">Single/Belum Menikah</option>
                                        <option value="K">Menikah</option>
                                        <option value="D">Duda</option>
                                        <option value="J">Janda</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h2 class="page-header">SMTA</h2>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jenis Sekolah <sup style="color: red">*)</sup></label>
                                    <select id="jenis_smta" name="mhsd_jsmta_id" class="form-control">
                                        <option value="">-- Pilih --</option>                                        
                                        <?php foreach ($get_jenis_smta as $row): ?>
                                            <option value="<?php echo $row->id; ?>" <?php echo set_select('mhsd_jsmta_id', $row->id); ?>><?php echo $row->nama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Jurusan Sekolah <sup style="color: red">*)</sup></label>
                                    <select id="jurusan_smta" name="mhsd_jursmta_id" class="form-control"></select>
                                </div>                                
                                <div class="form-group">
                                    <label>Nama Sekolah <sup style="color: red">*)</sup></label>
                                    <input type="text" name="mhsd_nama_sekolah" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_nama_sekolah'); ?>">
                                </div>                                
                                <div class="form-group">
                                    <label>Tahun Lulus <sup style="color: red">*)</sup></label>
                                    <input type="text" name="mhsd_tahun_lulus" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_tahun_lulus'); ?>">
                                    <div class="callout callout-success" style="padding:5px;margin-top: 5px;">
                                        Isikan <b>0000</b> jika belum lulus
                                    </div>                                    
                                </div>                                                                                      
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>NEM <sup style="color: red">*)</sup></label>
                                    <input type="text" name="mhsd_nilai" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_nilai'); ?>">
                                    <div class="callout callout-success" style="padding:5px;margin-top: 5px;">
                                        Isikan <b>0.00</b> jika belum memiliki NEM/Nilai
                                    </div>                                    
                                </div>                                
                                <div class="form-group">
                                    <label>Tanggal Ijasah</label>
                                    <input id="tgl_ijasah" type="text" name="mhsd_tgl_ijasah" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_tgl_ijasah'); ?>">
                                </div>                                
                                <div class="form-group">
                                    <label>Nomor Ijasah</label>
                                    <input type="text" name="mhsd_noijasah" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_noijasah'); ?>">
                                </div>                                
                            </div>
                            <div class="col-md-12">
                                <h2 class="page-header">Kontak/Alamat</h2>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Alamat <sup style="color: red">*)</sup></label>
                                    <input type="text" name="mhsalmt_alamat" class="form-control" value="<?php echo $this->form_validation->set_value('mhsalmt_alamat'); ?>">
                                </div>
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <input type="text" name="mhsalmt_kecamatan" class="form-control" value="<?php echo $this->form_validation->set_value('mhsalmt_kecamatan'); ?>">
                                </div>
                                <div class="form-group">
                                    <label>Kota/Kabupaten <sup style="color: red">*)</sup></label>
                                    <select id="kota" name="mhsalmt_kota_kode" class="form-control"></select>
                                    <div class="callout callout-success" style="padding:5px;margin-top: 5px;">
                                        Pilih Propinsi terlebih dahulu
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label>Propinsi <sup style="color: red">*)</sup></label>
                                    <select id="propinsi" name="mhsalmt_kode_propinsi" class="form-control">
                                        <option value="">-- Pilih --</option>                                        
                                        <?php foreach ($get_propinsi as $row): ?>
                                            <option value="<?php echo $row->id; ?>" <?php echo set_select('mhsalmt_kode_propinsi', $row->id); ?>><?php echo $row->nama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kode Pos</label>
                                    <input type="text" name="mhsalmt_kode_pos" class="form-control" value="<?php echo $this->form_validation->set_value('mhsalmt_kode_pos'); ?>">
                                </div>
                                <div class="form-group">
                                    <label>No. Telepon (Handphone)</label>
                                    <input type="text" name="mhsd_notelp" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_notelp'); ?>">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="mhsd_email" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_email'); ?>">
                                </div>
                                <div class="form-group">
                                    <label>Website</label>
                                    <input type="text" name="mhsd_website" class="form-control" value="<?php echo $this->form_validation->set_value('mhsd_website'); ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h2 class="page-header">Sumber Informasi</h2>
                                <div class="form-group">
                                    <?php foreach ($get_sumber_informasi as $val): ?>
                                        <div class="checkbox checkbox-success checkbox-circle">
                                            <input type="checkbox" name="mhsd_sinfo_id[]" value="<?php echo $val->id; ?>" id="<?php echo $val->id; ?>" <?php echo set_checkbox('mhsd_sinfo_id[]', $val->id); ?>>
                                            <label for="<?php echo $val->id; ?>">
                                                <?php echo $val->nama; ?>
                                            </label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>                                
                            </div>                            
                        </div>
                    </div>
                    <div class="box-footer">
                        <button id="submit" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    $(function () {
        $('#tgl_daftar').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true
        });
        $('#tgl_lahir').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true
        });
        $('#tgl_ijasah').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true
        });
        makeSublist("jenis_smta", "jurusan_smta", "<?php echo $get_jurusan_smta; ?>", true, false, "");
        makeSublist("propinsi", "kota", "<?php echo $get_kota; ?>", true, false, "");
        $('#submit').click(function () {
            if (window.File && window.FileReader && window.FileList && window.Blob) {
                var fsize = $('#id_foto')[0].files[0].size;
                if (fsize > 252000) {
                    alert("Ukuran foto maksimal 250KB");
                }
            } else {
                alert("Silakan upgrade browser anda ke versi terbaru");
            }
        });
    });
</script>