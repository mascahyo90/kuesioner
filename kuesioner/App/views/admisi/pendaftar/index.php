<script type="text/javascript" src="<?php echo base_url(); ?>Assets/v1/bo/js/common.js"></script>
<style>
    .alert.alert-petunjuk {
        background-color: #FDF1C1;
    }
    .label-lulus {
        background-color: #ff0084;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <form action="<?php echo $link_filter; ?>" method="post">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Pencarian</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nomor Pendaftaran</label>
                                    <input id="fil_nodaf" name="fil_nodaf" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nomor Referensi</label>
                                    <input id="fil_noref" name="fil_noref" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nama Pendaftar</label>
                                    <input id="fil_nama" name="fil_nama" type="text" class="form-control">
                                </div>
                            </div>    
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Status Ujian</label>
                                    <select id="fil_status_ujian" name="fil_status_ujian" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="0">Belum Ujian</option>
                                        <option value="1">Tidak Lulus</option>
                                        <option value="2">Lulus</option>
                                    </select>
                                </div>
                            </div>                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Tahun Akademik</label>
                                    <select id="fil_semester" name="fil_semester" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach ($get_semester as $row): ?>
                                            <option value="<?php echo $row->id; ?>"><?php echo $row->periode; ?> - <?php echo $row->semester; ?></option>
                                        <?php endforeach; ?>
                                    </select>                                    
                                </div>                                
                            </div>                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Gelombang</label>
                                    <select id="fil_gelombang" name="fil_gelombang" class="form-control"></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Program Studi</label>
                                    <select id="fil_prodi" name="fil_prodi" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach ($get_prodi as $row): ?>
                                            <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                        <?php endforeach; ?>
                                    </select>                                    
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Jenis Pendaftaran</label>
                                    <select id="fil_status_daftar" name="fil_status_daftar" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <option value="0">Offline</option>
                                        <option value="1">Online</option>
                                        <option value="2">Kolektif</option>
                                    </select>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm bg-purple"><i class="fa fa-search"></i> Cari</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">
            <?php echo $message; ?>            
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-title pull-left">
                        <a href="<?php echo $link_tambah; ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
                    </div>
                    <div class="box-title pull-right">
                        <?php echo $pagination; ?>
                    </div>
                </div>              
                <div class="box-body table-responsive">
                    <p style="margin:2px">Jumlah Data: <b><?php echo $jumlah; ?></b></p>
                    <?php echo $table; ?>
                    <br>
                    <div class="form-horizontal form-group" style="padding: 0;margin: 0">
                        <label class="col-md-1 control-label" style="text-align: left;vertical-align: middle;width: 50px;">Hapus</label>
                        <div class="col-md-2">
                            <select id="moreHapus" class="form-control">
                                <option value="none">-- Pilih --</option>
                                <option value="0">Terpilih</option>
                                <option value="1">Semua</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?php echo $pagination; ?>                    
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="alert alert-petunjuk">
                <h4><i class="icon fa fa-info"></i> Informasi</h4>
                <ul>
                    <li><b>Warna Baris:</b>
                        <ul>
                            <li>Orange, calon mahasiswa belum ujian</li>
                            <li>Merah, calon mahasiswa tidak lulus ujian</li>
                            <li>Putih, calon mahasiswa lulus ujian</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>        
    </div>
</section>
<div class="modal fade" id="pembayaran-daftar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>                
                <h4 class="modal-title">Pembayaran Biaya Pendaftaran</h4>
            </div>
            <form id="frmBayarDaftar" onsubmit="return validateAdmissionPayment(this)" action="<?php echo $link_bayar_daftar; ?>" role="form" class="form-horizontal" method="post">
                <div class="modal-body">
                    <input type="hidden" name="nodaf" id="nodaf" />
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Nominal</label>
                            <input id="nom_bayar" type="text" name="mhsd_biaya_daftar" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="ubah-status-lulus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>                
                <h4 class="modal-title">Status Kelulusan</h4>
            </div>
            <form id="frmGantiStatus" action="<?php echo $link_status_lulus; ?>" role="form" class="form-horizontal" method="post">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id" />
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Status Kelulusan</label>
                            <select id="status_lulus" name="mhsd_status_ujian" class="form-control">
                                <option value="0">Belum Ujian</option>
                                <option value="1">Tidak Lulus</option>
                                <option value="2">Lulus</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal modal-danger fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                Yakin Ingin Menghapus Data Ini?
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm btn-danger hapus">Hapus</a>
            </div>
        </div>
    </div>
</div>
<script>
    function validateAdmissionPayment(form) {
        var nom_daftar = 100000;
        if (form.mhsd_biaya_daftar.value != nom_daftar) {
            alert("Biaya Pendaftaran Harus 100.000");
            form.mhsd_biaya_daftar.focus();
            return (false);
        }
        return (true);
    }
    $(function () {
        var delVal;
        $("#fil_nodaf").val("<?php echo $nodaf; ?>");
        $("#fil_noref").val("<?php echo $noref; ?>");
        $("#fil_nama").val("<?php echo $nama; ?>");
        $("#fil_status_ujian option").filter(function (index) {
            return $(this).val() == '<?php echo $ujian; ?>';
        }).attr("selected", "selected");
        $("#fil_semester option").filter(function (index) {
            return $(this).val() == '<?php echo $semester; ?>';
        }).attr("selected", "selected");
        makeSublist("fil_semester", "fil_gelombang", "<?php echo $get_gelombang; ?>", false, true, "<?php echo $gelombang; ?>");
        $("#fil_prodi option").filter(function (index) {
            return $(this).val() == '<?php echo $prodi; ?>';
        }).attr("selected", "selected");
        $("#fil_status_daftar option").filter(function (index) {
            return $(this).val() == '<?php echo $daftar; ?>';
        }).attr("selected", "selected");
        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.hapus').attr('href', $(e.relatedTarget).data('href'));
        });
        $(".ganti-status-lulus").click(function () {
            $.ajax({
                url: $(this).attr("ref"),
                success: function (html) {
                    var data = jQuery.parseJSON(html);
                    $("#id").val(data.id);
                    $("#status_lulus option").filter(function (index) {
                        return $(this).val() == data.status;
                    }).attr("selected", "selected");
                }
            });
        });
        $(".bayar-daftar").click(function () {
            $.ajax({
                url: $(this).attr("ref"),
                success: function (html) {
                    var data = jQuery.parseJSON(html);
                    $("#nodaf").val(data.nodaf);
                    $("#nom_bayar").val(data.nominal);
                }
            });
        });
        $(".cbPilihParent").click(function () {
            $(".cbPilih").prop('checked', $(this).prop('checked'));
        });
        $("#moreHapus").change(function () {
            if ($(this).val() == 0) {
                if (confirm("Apakah anda yakin akan menghapus data yang terpilih?")) {
                    n = $(".cbPilih:checked").length;
                    delVal = new Array();
                    for (i = 0; i < n; i++) {
                        delVal.push("'" + $(".cbPilih:checked").eq(i).val() + "'");
                    }
                    delVal = delVal.join(",");
                    postIt('<?php echo $link_multi_delete; ?>', {'del': delVal, 'action': 'select'});
                }
            } else if ($(this).val() == 1) {
                if (confirm("Apakah anda yakin akan menghapus semua data?")) {
                    postIt('<?php echo $link_multi_delete; ?>', {'del': '', 'action': 'all'});
                }
            }
            $(this).val("none");
        });
    });
</script>
