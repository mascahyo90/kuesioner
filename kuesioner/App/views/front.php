<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo $portal_description; ?>">
        <meta name="author" content="amikom surakarta">
        <meta name="keywords" content="pmb,amikom,amikom surakarta,penerimaan mahasiswa baru,amik cipta darma surakarta">
        <title><?php echo $portal_title; ?></title>
        <!-- core CSS -->
        <link href="<?php echo base_url(); ?>Assets/v1/fo/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>Assets/v1/fo/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>Assets/v1/fo/css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>Assets/v1/fo/css/prettyPhoto.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>Assets/v1/fo/css/main.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>Assets/v1/fo/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="">
        <style>
            .alert {
                padding: 5px;
                margin-bottom: 5px;
            }
        </style>
        <script src="<?php echo base_url(); ?>Assets/v1/fo/js/jquery.js"></script>
    </head>
    <body class="homepage">
        <header id="header">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="navbar-brand">
                            <span class="col-md-2 col-sm-2" style="padding-left: 0;">
                                <a href="#"><img src="<?php echo base_url(); ?>Assets/v1/fo/img/logo_amikom.png" alt="logo"></a>                                
                            </span>
                            <span class="col-md-10 col-sm-10">
                                <div class="text-logo">SPMB
                                    <p>AMIKOM CIPTA DARMA SURAKARTA</p>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Beranda</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Agenda <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">
                                    <?php if (!empty($menu_agenda)): ?>
                                        <?php foreach ($menu_agenda as $row): ?>
                                            <li><a href="<?php echo site_url('portal/utama/agenda/' . $row->agn_id); ?>"><?php echo $row->agn_judul; ?></a></li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informasi <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">
                                    <?php if (!empty($menu_konten)): ?>
                                        <?php foreach ($menu_konten as $row): ?>
                                            <li><a href="<?php echo site_url('portal/utama/informasi/' . $row->ktn_id); ?>"><?php echo $row->ktn_judul; ?></a></li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </li>                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pendaftaran <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo $link_pendaftaran; ?>">Pendaftaran Online</a></li>
                                    <li><a href="<?php echo $link_cetak_pendaftaran; ?>">Cetak Formulir</a></li>
                                </ul>
                            </li>                            
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <section id="main-slider" class="no-margin">
            <div class="carousel slide">
                <ol class="carousel-indicators">
                    <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#main-slider" data-slide-to="1"></li>
                    <li data-target="#main-slider" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active" style="background-color: #ffffff;">
                        <div class="container">
                            <div class="row slide-margin">
                                <div class="col-sm-6">
                                    <div class="carousel-content">
                                        <h1 class="animation animated-item-1 color-content">PMB 2016/2017</h1>
                                        <h2 class="animation animated-item-2" style="color: #666666;">Selamat Datang di Website Penerimaan Mahasiswa Baru (PMB) AMIKOM Surakarta</h2>
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs animation animated-item-4">
                                    <div class="slider-img">
                                        <img src="<?php echo base_url(); ?>Assets/v1/fo/img/img1.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background-color: #fff;">
                        <div class="container">
                            <div class="row slide-margin">
                                <div class="col-sm-6">
                                    <div class="carousel-content">
                                        <h1 class="animation animated-item-1 color-content">PMB 2016/2017</h1>
                                        <h2 class="animation animated-item-2" style="color: #666666;">Selamat Datang di Website Penerimaan Mahasiswa Baru (PMB) AMIKOM Surakarta</h2>
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs animation animated-item-4">
                                    <div class="slider-img">
                                        <img src="<?php echo base_url(); ?>Assets/v1/fo/img/img3.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" style="background-color: white;">
                        <div class="container">
                            <div class="row slide-margin">
                                <div class="col-sm-6">
                                    <div class="carousel-content">
                                        <h1 class="animation animated-item-1 color-content">PMB 2016/2017</h1>
                                        <h2 class="animation animated-item-2" style="color: #666666;">Selamat Datang di Website Penerimaan Mahasiswa Baru (PMB) AMIKOM Surakarta</h2>
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs animation animated-item-4">
                                    <div class="slider-img">
                                        <img src="<?php echo base_url(); ?>Assets/v1/fo/img/img4.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="prev hidden-xs" href="#main-slider" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
            <a class="next hidden-xs" href="#main-slider" data-slide="next"><i class="fa fa-chevron-right"></i></a>
        </section>
        <?php echo $konten; ?>
        <section id="bottom">
            <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="widget">
                            <h3>Panduan</h3>
                            <ul>
                                <li><a href="#">Petunjuk Pengisian Form</a></li>
                                <li><a href="#">Alur Pendaftaran</a></li>
                                <li><a href="#">Peta Lokasi</a></li>
                            </ul>
                        </div>    
                    </div><!--/.col-md-3-->
                    <div class="col-md-4 col-sm-6">
                        <div class="widget">
                            <h3>Hubungi Kami</h3>
                            <ul>
                                <li><b>Telepon :</b> (0271) - 7851501, 7851507</li>
                                <li><b>CS 1:</b> 085725285885 (Yeni)</li>
                                <li><b>CS 2:</b> 085645083664 (Margono)</li>
                            </ul>
                            <h4>Sosial Media</h4>
                            <ul>
                                <li><i class="fa fa-facebook"></i>&nbsp;&nbsp;
                                    <a href="https://www.facebook.com/groups/57141654951/" target="_blank">Amikom Surakarta</a>
                                </li>
                                <li><i class="fa fa-twitter"></i>&nbsp;
                                    <a href="https://twitter.com/amikomsolo" target="_blank">@amikomsolo</a>
                                </li>
                                <li><i class="fa fa-instagram"></i>&nbsp;
                                    <a href="https://www.instagram.com/amikomsolo" target="_blank">@amikomsolo</a>
                                </li>
                                <li><b>BBM :</b> 53997BB5</li>
                            </ul>
                        </div>    
                    </div><!--/.col-md-3-->
                    <div class="col-md-4 col-sm-6">
                        <div class="widget">
                            <h3>Sekretariat PMB</h3>
                            <p>
                                Jalan Veteran, Notosuman, Singopuran, Kartasura, Sukoharjo
                            </p>
                        </div>    
                    </div><!--/.col-md-3-->
                </div>
            </div>
        </section><!--/#bottom-->
        <footer id="footer" class="midnight-blue">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        &copy; 2015 Theme From ShapeBootstrap. All Rights Reserved.
                    </div>
                    <div class="col-sm-6">
                        <div class="pull-right">
                            AMIKOM CIPTA DARMA SURAKARTA
                        </div>
                    </div>
                </div>
            </div>
        </footer><!--/#footer-->
        <script src="<?php echo base_url(); ?>Assets/v1/fo/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>Assets/v1/fo/js/jquery.prettyPhoto.js"></script>
        <script src="<?php echo base_url(); ?>Assets/v1/fo/js/jquery.isotope.min.js"></script>
        <script src="<?php echo base_url(); ?>Assets/v1/fo/js/main.js"></script>
        <script src="<?php echo base_url(); ?>Assets/v1/fo/js/wow.min.js"></script>
    </body>
</html>
