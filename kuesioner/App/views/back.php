<?php
if (!$this->ion_auth->logged_in()) {
    $user = "";
    $user_groups = "";
} else {
    $user = $this->ion_auth->user()->row();
    $user_groups = $this->ion_auth->get_users_groups()->result();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Aplikasi Kuesioner</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/skins/_all-skins.min.css">
        <script src="<?php echo base_url(); ?>Assets/v1/bo/js/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo base_url(); ?>Assets/v1/bo/js/jquery-ui.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            $(document).ready(function (e) {
                $.widget.bridge('uibutton', $.ui.button);
            });
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <div class="logo">
                    <span class="logo-mini"><b>APP</b></span>
                    <span class="logo-lg"><b>KUESIONER</b></span>
                </div>
                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url(); ?>Assets/v1/bo/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs">
                                        <?php
                                        if (!$this->ion_auth->logged_in()) {
                                            echo 'Maaf Anda Belum Login';
                                        } else {
                                            echo $user->last_name;
                                        }
                                        ?>
                                    </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url(); ?>Assets/v1/bo/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p>
                                            <?php
                                            if (!$this->ion_auth->logged_in()) {
                                                echo 'Maaf Anda Belum Login';
                                            } else {
                                                echo $user->last_name;
                                            }
                                            ?>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo site_url('auth/logout'); ?>" class="btn btn-default btn-flat">Ganti Password</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo site_url('auth/logout'); ?>" class="btn btn-default btn-flat">Keluar</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url(); ?>Assets/v1/bo/img/logo.png" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>APLIKASI KUESIONER</p>
                            <p>BETA v1.0</p>
                        </div>
                    </div>
                    <!-- sidebar menu -->
                    <ul class="sidebar-menu">
                        <li class="header"><strong>MENU NAVIGASI</strong></li>
                        <li>
                            <a href="<?php echo site_url('dashboard/dashboard'); ?>">
                                <i class="fa fa-home"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <?php echo $this->otoritas->sidebar(); ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo $title_panel; ?>
                        <small><?php echo $sub_title_panel; ?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <?php echo $this->breadcrumbs->output() ?>
                    </ol>
                </section>
                <!-- Main content -->
                <?php echo $konten; ?>
            </div><!-- /.content-wrapper -->
            <footer class="main-footer">
<!--                <b>Aplikasi Kuesioner BAPEDA</b>-->
            </footer>
        </div>
    </body>
    <script src="<?php echo base_url(); ?>Assets/v1/bo/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>Assets/v1/bo/js/app.js"></script>    
</html>