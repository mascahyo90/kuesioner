<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php echo $message; ?>
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-title pull-left">
                        <a href="<?php echo $tambah; ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th style="width: 10px">No</th>
                                <th>Grub</th>
                                <th>Deskripsi</th>
                                <th style="width: 50px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($groups as $grup):
                                ?>
                                <tr>
                                    <td><?php echo ++$i; ?></td>
                                    <td><?php echo htmlspecialchars($grup->name, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo htmlspecialchars($grup->description, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td>
                                        <a href="<?php echo site_url('auth/edit_group/' . $grup->id); ?>" class="btn btn-xs btn-success" title="Ubah Data"><i class="fa fa-pencil"></i></a>                                       
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>