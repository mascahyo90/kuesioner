<script>
    $(function () {
        $("#edit_skpd option").filter(function (index) {
            return $(this).val() == <?php echo $user->skpd_id;?>;
        }).attr("selected", "selected");
    });
</script>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissable" <?php
            if (is_string($message)) {
                echo 'style="display:block; margin-bottom:7px;"';
            } else {
                echo 'style="display:none;"';
            }
            ?>>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                <?php echo $message; ?>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="ion-person"></i> <?php echo $title_box; ?></h3>
                </div>
                <?php echo form_open(uri_string()); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nama Depan <sup style="color:red">*)</sup></label>
                                <?php // echo form_input($first_name, array('class'=> 'form-control'));?>
                                <input type="text" name="first_name" class="form-control" value="<?php if(!empty($user->first_name)){echo $user->first_name;}?>">
                            </div>
                            <div class="form-group">
                                <label>Nama Belakang <sup style="color:red">*)</sup></label>
                                <input type="text" name="last_name" class="form-control" value="<?php if(!empty($user->last_name)){echo $user->last_name;}?>">
                            </div>
                            <div class="form-group">
                                <label>SKPD <sup style="color:red">*)</sup></label>
                                <select id="edit_skpd" name="skpd_id" class="form-control">
                                    <option value="">-- Pilih --</option>
                                    <?php foreach ($get_skpd as $row): ?>
                                        <option value="<?php echo $row->id; ?>" <?php echo set_select('skpd_id', $row->id); ?>><?php echo $row->nama; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Password <sup style="color:red">*)</sup></label>
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi Password <sup style="color:red">*)</sup></label>
                                <input type="password" name="password_confirm" class="form-control" placeholder="Konfirmasi Password">
                            </div>
                            <div class="form-group">
                                <?php if ($this->ion_auth->is_admin()): ?>
                                    <h3>Grup Pengguna</h3>
                                    <?php foreach ($groups as $group): ?>
                                        <div class="checkbox">
                                            <label>
                                                <?php
                                                $gID = $group['id'];
                                                $checked = null;
                                                $item = null;
                                                foreach ($currentGroups as $grp) {
                                                    if ($gID == $grp->id) {
                                                        $checked = ' checked="checked"';
                                                        break;
                                                    }
                                                }
                                                ?>
                                                <input type="checkbox" name="groups[]" value="<?php echo $group['id']; ?>"<?php echo $checked; ?>>
                                                <?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8'); ?>
                                            </label>
                                        </div>
                                    <?php endforeach ?>
                                <?php endif ?> 
                            </div>  
                            <?php echo form_hidden('id', $user->id); ?>
                            <?php echo form_hidden($csrf); ?>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Ubah</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>