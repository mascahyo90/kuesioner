<?php echo form_open("auth/login"); ?>
<div class="form-group has-feedback">
    <input type="text" class="form-control" name="identity" value="" id="identity" placeholder="Username" autofocus="autofocus"/>
    <span class="glyphicon glyphicon-user form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
    <input type="password" class="form-control" name="password" value="" id="password" placeholder="Password" />
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="row">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-instagram btn-block btn-flat">Login</button>
    </div>
</div>
<?php echo form_close(); ?>