<link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/format.css">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissable" <?php
            if (is_string($message)) {
                echo 'style="display:block; margin-bottom:7px;"';
            } else {
                echo 'style="display:none;"';
            }
            ?>>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                <?php echo $message; ?>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-plus-circle"></i> <?php echo $title_box; ?></h3>
                </div>
                <?php echo form_open("auth/create_group"); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nama Grup</label>
                                <input type="text" name="group_name" class="form-control" placeholder="Nama Grup">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <input type="text" name="description" class="form-control" placeholder="Deskripsi grup">
                            </div>
                            <div class="form-group">
                                <label>Menu</label>
                                <?php foreach ($menu as $row): ?>
                                    <div class="checkbox checkbox-primary checkbox-circle">
                                        <input type="checkbox" name="grpmnu_mnu_id[]" value="<?php echo $row->mnu_id; ?>" id="<?php echo $row->mnu_id; ?>">
                                        <label for="<?php echo $row->mnu_id; ?>">
                                            <?php echo $row->mnu_menu; ?>
                                        </label>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>