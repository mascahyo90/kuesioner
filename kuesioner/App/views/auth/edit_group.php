<link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/format.css">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissable" <?php
            if (is_string($message)) {
                echo 'style="display:block; margin-bottom:7px;"';
            } else {
                echo 'style="display:none;"';
            }
            ?>>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                <?php echo $message; ?>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-edit"></i> <?php echo $title_box; ?></h3>
                </div>
                <?php echo form_open(current_url()); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nama Grup</label>
                                <input type="text" name="group_name" class="form-control" value="<?php
                                if (!empty($group->name)) {
                                    echo $group->name;
                                }
                                ?>">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <input type="text" name="group_description" class="form-control" value="<?php
                                if (!empty($group->description)) {
                                    echo $group->description;
                                }
                                ?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-xs-4">Menu</label>
                                <label class="col-xs-8">Otoritas</label>
                                <table class="table table-condensed table-checkbox col-md-12">
                                    <?php if ($menu > 0): ?>
                                        <?php foreach ($menu as $row): ?>
                                            <?php
                                            $mnuId = $row->mnu_id;
                                            $checked = null;
                                            $item = null;
                                            if ($cur_otoritas > 0) {
                                                foreach ($cur_otoritas as $val) {
                                                    if ($mnuId == $val->grpmnu_mnu_id) {
                                                        $checked = ' checked="checked"';
                                                        break;
                                                    }
                                                }
                                            }
                                            ?>
                                            <tr>
                                                <td class="col-xs-4">
                                                    <div class="checkbox checkbox-success checkbox-circle">
                                                        <input type="checkbox" name="mnu_id[]" id="<?php echo $row->mnu_id; ?>" value="<?php echo $row->mnu_id; ?>"<?php echo $checked; ?>/>
                                                        <label for="<?php echo $row->mnu_id; ?>">
                                                            <?php echo $row->mnu_menu; ?>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td class="col-xs-2">
                                                    <div id="cr<?php echo $row->mnu_id; ?>" class="checkbox checkbox-primary checkbox-circle" <?php
                                                    if ($checked != ' checked="checked"') {
                                                        echo 'style="display:none;"';
                                                    }
                                                    ?>>  
                                                        <input type="hidden" id="cre<?php echo $row->mnu_id; ?>" <?php
                                                        if ($checked == ' checked="checked"') {
                                                            echo 'name="c[]"';
                                                        }
                                                        ?> value="<?php
                                                               if (!empty($val->grpmnu_otoritas)) {
                                                                   echo substr($val->grpmnu_otoritas, 0, 1);
                                                               } else {
                                                                   echo '0';
                                                               }
                                                               ?>"/>
                                                        <input type="checkbox" id="c<?php echo $row->mnu_id; ?>" <?php
                                                        if ((isset($val->grpmnu_otoritas)) && $checked == ' checked="checked"') {
                                                            if (substr($val->grpmnu_otoritas, 0, 1) == '1') {
                                                                echo 'checked';
                                                            }
                                                        }
                                                        ?>/>
                                                        <label for="c<?php echo $row->mnu_id; ?>">
                                                            Create
                                                        </label>
                                                    </div>                                                    
                                                </td>
                                                <td class="col-xs-2">
                                                    <div id="re<?php echo $row->mnu_id; ?>" class="checkbox checkbox-info checkbox-circle" <?php
                                                    if ($checked != ' checked="checked"') {
                                                        echo 'style="display:none;"';
                                                    }
                                                    ?>>  
                                                        <input type="hidden" id="rea<?php echo $row->mnu_id; ?>" <?php
                                                        if ($checked == ' checked="checked"') {
                                                            echo 'name="r[]"';
                                                        }
                                                        ?>  value="<?php
                                                               if (!empty($val->grpmnu_otoritas)) {
                                                                   echo substr($val->grpmnu_otoritas, 1, 1);
                                                               } else {
                                                                   echo '0';
                                                               }
                                                               ?>"/>
                                                        <input type="checkbox" id="r<?php echo $row->mnu_id; ?>" <?php
                                                        if (isset($val->grpmnu_otoritas) && $checked == ' checked="checked"') {
                                                            if (substr($val->grpmnu_otoritas, 1, 1) == '1') {
                                                                echo 'checked';
                                                            }
                                                        }
                                                        ?>/>                                        
                                                        <label for="r<?php echo $row->mnu_id; ?>">
                                                            Read
                                                        </label>
                                                    </div>
                                                </td>
                                                <td class="col-xs-2">
                                                    <div id="up<?php echo $row->mnu_id; ?>" class="checkbox checkbox-warning checkbox-circle" <?php
                                                    if ($checked != ' checked="checked"') {
                                                        echo 'style="display:none;"';
                                                    }
                                                    ?>>  
                                                        <input type="hidden" id="upd<?php echo $row->mnu_id; ?>" <?php
                                                        if ($checked == ' checked="checked"') {
                                                            echo 'name="u[]"';
                                                        }
                                                        ?> value="<?php
                                                               if (!empty($val->grpmnu_otoritas)) {
                                                                   echo substr($val->grpmnu_otoritas, 2, 1);
                                                               } else {
                                                                   echo '0';
                                                               }
                                                               ?>"/>
                                                        <input type="checkbox" id="u<?php echo $row->mnu_id; ?>" <?php
                                                        if (isset($val->grpmnu_otoritas) && $checked == ' checked="checked"') {
                                                            if (substr($val->grpmnu_otoritas, 2, 1) == '1') {
                                                                echo 'checked';
                                                            }
                                                        }
                                                        ?>/>                                        
                                                        <label for="u<?php echo $row->mnu_id; ?>">
                                                            Update
                                                        </label>
                                                    </div>
                                                </td>
                                                <td class="col-xs-2">                                      
                                                    <div id="de<?php echo $row->mnu_id; ?>" class="checkbox checkbox-danger checkbox-circle" <?php
                                                    if ($checked != ' checked="checked"') {
                                                        echo 'style="display:none;"';
                                                    }
                                                    ?>>  
                                                        <input type="hidden" id="del<?php echo $row->mnu_id; ?>" <?php
                                                        if ($checked == ' checked="checked"') {
                                                            echo 'name="d[]"';
                                                        }
                                                        ?> value="<?php
                                                               if (!empty($val->grpmnu_otoritas)) {
                                                                   echo substr($val->grpmnu_otoritas, 3, 1);
                                                               } else {
                                                                   echo '0';
                                                               }
                                                               ?>"/>
                                                        <input type="checkbox" id="d<?php echo $row->mnu_id; ?>" <?php
                                                        if (isset($val->grpmnu_otoritas) && $checked == ' checked="checked"') {
                                                            if (substr($val->grpmnu_otoritas, 3, 1) == '1') {
                                                                echo 'checked';
                                                            }
                                                        }
                                                        ?>/>                                        
                                                        <label for="d<?php echo $row->mnu_id; ?>">
                                                            Delete
                                                        </label>
                                                    </div>                                      
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
<?php foreach ($menu as $row): ?>
            $('#<?php echo $row->mnu_id; ?>').click(function () {
                if ($('#<?php echo $row->mnu_id; ?>').is(":checked")) {
                    $('#cr<?php echo $row->mnu_id; ?>').show();
                    $('#re<?php echo $row->mnu_id; ?>').show();
                    $('#up<?php echo $row->mnu_id; ?>').show();
                    $('#de<?php echo $row->mnu_id; ?>').show();

                    $('#cre<?php echo $row->mnu_id; ?>').val('0');
                    $('#rea<?php echo $row->mnu_id; ?>').val('0');
                    $('#upd<?php echo $row->mnu_id; ?>').val('0');
                    $('#del<?php echo $row->mnu_id; ?>').val('0');

                    $('#cre<?php echo $row->mnu_id; ?>').attr('name', 'c[]');
                    $('#rea<?php echo $row->mnu_id; ?>').attr('name', 'r[]');
                    $('#upd<?php echo $row->mnu_id; ?>').attr('name', 'u[]');
                    $('#del<?php echo $row->mnu_id; ?>').attr('name', 'd[]');
                } else {
                    $('#cr<?php echo $row->mnu_id; ?>').hide();
                    $('#re<?php echo $row->mnu_id; ?>').hide();
                    $('#up<?php echo $row->mnu_id; ?>').hide();
                    $('#de<?php echo $row->mnu_id; ?>').hide();

                    $('#c<?php echo $row->mnu_id; ?>').attr('checked', false);
                    $('#r<?php echo $row->mnu_id; ?>').attr('checked', false);
                    $('#u<?php echo $row->mnu_id; ?>').attr('checked', false);
                    $('#d<?php echo $row->mnu_id; ?>').attr('checked', false);

                    $('#cre<?php echo $row->mnu_id; ?>').val('0');
                    $('#rea<?php echo $row->mnu_id; ?>').val('0');
                    $('#upd<?php echo $row->mnu_id; ?>').val('0');
                    $('#del<?php echo $row->mnu_id; ?>').val('0');

                    $('#cre<?php echo $row->mnu_id; ?>').removeAttr('name');
                    $('#rea<?php echo $row->mnu_id; ?>').removeAttr('name');
                    $('#upd<?php echo $row->mnu_id; ?>').removeAttr('name');
                    $('#del<?php echo $row->mnu_id; ?>').removeAttr('name');
                }
            });

            $('#c<?php echo $row->mnu_id; ?>').click(function () {
                if ($(this).is(":checked")) {
                    $('#cre<?php echo $row->mnu_id; ?>').val('1');
                } else {
                    $('#cre<?php echo $row->mnu_id; ?>').val('0');
                }
            });
            $('#r<?php echo $row->mnu_id; ?>').click(function () {
                if ($(this).is(":checked")) {
                    $('#rea<?php echo $row->mnu_id; ?>').val('1');
                } else {
                    $('#rea<?php echo $row->mnu_id; ?>').val('0');
                }
            });
            $('#u<?php echo $row->mnu_id; ?>').click(function () {
                if ($(this).is(":checked")) {
                    $('#upd<?php echo $row->mnu_id; ?>').val('1');
                } else {
                    $('#upd<?php echo $row->mnu_id; ?>').val('0');
                }
            });
            $('#d<?php echo $row->mnu_id; ?>').click(function () {
                if ($(this).is(":checked")) {
                    $('#del<?php echo $row->mnu_id; ?>').val('1');
                } else {
                    $('#del<?php echo $row->mnu_id; ?>').val('0');
                }
            });
<?php endforeach; ?>
    });
</script>