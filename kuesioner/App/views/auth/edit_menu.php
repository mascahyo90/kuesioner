<script>
    $(function () {
        $("#parent_menu option").filter(function (index) {
            return $(this).val() == <?php echo $update->mnu_mnukat_id;?>;
        }).attr("selected", "selected");
    });
</script>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissable" <?php
            if (is_string($message)) {
                echo 'style="display:block; margin-bottom:7px;"';
            } else {
                echo 'style="display:none;"';
            }
            ?>>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                <?php echo $message; ?>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-edit"></i> <?php echo $title_box; ?></h3>
                </div>
                <?php echo form_open(current_url()); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Kategori Menu</label>
                                <select id="parent_menu" name="mnu_mnukat_id" class="form-control">
                                    <?php foreach ($list_parent as $row): ?>
                                        <option value="<?php echo $row->mnukat_id; ?>"><?php echo $row->mnukat_menu; ?></option>
                                    <?php endforeach; ?>
                                </select>                                
                            </div>
                            <div class="form-group">
                                <label>Menu <sup style="color:red">*)</sup></label>
                                <input type="text" name="mnu_menu" class="form-control" value="<?php if (!empty($update->mnu_menu)) {echo $update->mnu_menu;} ?>">
                            </div>
                            <div class="form-group">
                                <label>URL <sup style="color:red">*)</sup></label>
                                <input type="text" name="mnu_link" class="form-control" value="<?php if (!empty($update->mnu_link)) {echo $update->mnu_link;} ?>">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <input type="text" name="mnu_deskripsi" class="form-control" value="<?php if (!empty($update->mnu_deskripsi)) {echo $update->mnu_deskripsi;} ?>">
                            </div>
                            <div class="form-group">
                                <label>Order</label>
                                <input type="text" name="mnu_order" class="form-control" value="<?php if (!empty($update->mnu_order)) {echo $update->mnu_order;} ?>">
                            </div>
                            <div class="form-group">
                                <label>Icon</label>
                                <input type="text" name="mnu_icon" class="form-control" value="<?php if (!empty($update->mnu_icon)) {echo $update->mnu_icon;} ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>
