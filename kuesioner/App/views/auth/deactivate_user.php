<link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/format.css">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-exchange"></i> <?php echo $title_box; ?></h3>
                </div>
                <?php echo form_open("auth/deactivate/" . $user->id); ?>
                <?php echo form_hidden($csrf); ?>
                <?php echo form_hidden(array('id' => $user->id)); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="pad margin no-print">
                            <div class="callout callout-warning" style="margin-bottom: 0!important;">
                                <h4><i class="fa fa-warning"></i> Konfirmasi:</h4>
                                Apakah anda yakin ingin menonaktifkan <b><?php echo $user->username; ?></b>?
                                <div class="form-group" style="margin-top: 10px">
                                    <div class="radio radio-danger radio-inline">
                                        <input type="radio" name="confirm" id="radio1" value="yes" checked="checked"/>
                                        <label for="radio1">
                                            Ya
                                        </label>
                                    </div>
                                    <div class="radio radio-danger radio-inline">
                                        <input type="radio" name="confirm" id="radio2" value="no" checked="checked"/>
                                        <label for="radio2">
                                            Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Proses</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>