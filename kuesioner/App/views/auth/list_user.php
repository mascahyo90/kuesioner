<section class="content">
    <div class="row">
<!--        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <h3 class="box-title">Pencarian</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" placeholder="Username">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nama Depan</label>
                                <input type="text" class="form-control" placeholder="Nama Depan">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nama Belakang</label>
                                <input type="text" class="form-control" placeholder="Nama Belakang">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm bg-purple"><i class="fa fa-search-plus"></i> Cari</button>
                </div>
            </div>
        </div>-->
        <div class="col-md-12">
            <?php echo $message; ?>
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-title pull-left">
                        <a href="<?php echo $tambah; ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 10px">No</th>
                                <th>Nama Lengkap</th>
                                <th>Email</th>
                                <th>Grup</th>
                                <th>Status</th>
                                <th style="width: 75px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($users as $user):
                                ?>
                                <tr>
                                    <td><?php echo ++$i; ?></td>
                                    <td><?php echo htmlspecialchars($user->first_name . ' ' . $user->last_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td>
                                        <?php foreach ($user->groups as $group): ?>
                                            <span class="label label-success"><?php echo $group->name; ?></span><br />
                                        <?php endforeach; ?>
                                    </td>
                                    <td style="width: 15%">
                                        <?php
                                        if ($user->active) {
                                            echo '<a class="label label-success" href="' . site_url('auth/deactivate/' . $user->id) . '">Aktif</a>';
                                        } else {
                                            echo '<a class="label label-success" href="' . site_url('auth/activate/' . $user->id) . '">Non Aktif</a>';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo site_url('auth/edit_user/' . $user->id); ?>" class="btn btn-xs btn-success" title="Ubah Data"><i class="fa fa-pencil"></i></a>
                                        <a href="#" data-toggle="modal" data-target="#confirm-delete" data-href="<?php echo site_url('sistem/pengguna/delete/' . $user->id); ?>" class="btn btn-xs btn-danger" title="Hapus Data"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal modal-danger fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                Yakin Ingin Menghapus Data Ini?
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm btn-danger btn-ok">Hapus</a>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    });
</script>