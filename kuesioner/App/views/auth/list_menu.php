<script>
    $(function () {
        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    });
</script>
<style>
    .label.label-link-menu {
        background-color: #A40DBA;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php echo $message; ?>
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-title pull-left">
                        <a href="<?php echo $tambah; ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th style="width: 10px">No</th>
                                <th>Kategori</th>
                                <th>Menu</th>
                                <th>URL</th>
                                <th>Urutan</th>
                                <th>Ikon</th>
                                <th style="width: 75px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($list as $row):
                                ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo ++$i; ?></td>
                                    <td><span class="label label-success"><?php echo htmlspecialchars($row->mnukat_menu, ENT_QUOTES, 'UTF-8'); ?></span></td>
                                    <td><?php echo htmlspecialchars($row->mnu_menu, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><span class="label label-link-menu"><?php echo $row->mnu_link; ?></span></td>
                                    <td><?php echo htmlspecialchars($row->mnu_order, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo htmlspecialchars($row->mnu_icon, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td>
                                        <a href="<?php echo site_url('sistem/menu/edit/' . $row->mnu_id); ?>" class="btn btn-xs btn-success" title="Ubah Data"><i class="fa fa-edit"></i></a>
                                        <a href="#" data-toggle="modal" data-target="#confirm-delete" data-href="<?php echo site_url('sistem/menu/delete/' . $row->mnu_id); ?>" class="btn btn-xs btn-danger" title="Hapus Data"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal modal-danger fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                Yakin Ingin Menghapus Data Ini?
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>