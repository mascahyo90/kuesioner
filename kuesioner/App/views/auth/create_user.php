<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissable" <?php
            if (is_string($message)) {
                echo 'style="display:block; margin-bottom:7px;"';
            } else {
                echo 'style="display:none;"';
            }
            ?>>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                <?php echo $message; ?>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="ion-plus-circled"></i> <?php echo $title_box; ?></h3>
                </div>
                <?php echo form_open(current_url()); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nama Depan <sup style="color:red">*)</sup></label>
                                <input type="text" name="first_name" class="form-control" placeholder="Nama Depan">
                            </div>
                            <div class="form-group">
                                <label>Nama Belakang <sup style="color:red">*)</sup></label>
                                <input type="text" name="last_name" class="form-control" placeholder="Nama Belakang">
                            </div>
                            <div class="form-group">
                                <label>SKPD <sup style="color:red">*)</sup></label>
                                <select name="skpd_id" class="form-control">
                                    <option value="">-- Pilih --</option>
                                    <?php foreach ($get_skpd as $row): ?>
                                        <option value="<?php echo $row->id; ?>" <?php echo set_select('skpd_id', $row->id); ?>><?php echo $row->nama; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
<!--                            <div class="form-group">-->
<!--                                <label>Email <sup style="color:red">*)</sup></label>-->
<!--                                <input type="text" name="email" class="form-control" placeholder="Email">-->
<!--                            </div>-->
                            <div class="form-group">
                                <label>Username <sup style="color:red">*)</sup></label>
                                <input type="text" name="identity" class="form-control" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <label>Password (Min: 8 karakter)<sup style="color:red">*)</sup></label>
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi Password (Min: 8 karakter) <sup style="color:red">*)</sup></label>
                                <input type="password" name="password_confirm" class="form-control" placeholder="Konfirmasi Password">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>