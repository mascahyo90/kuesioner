<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible" <?php echo $notif1;?>>                
                <h4><i class="icon fa fa-warning"></i>Peringatan!</h4>
                Belum ada kegiatan di OPD anda. Silakan isi data kegiatan <strong>(Referensi > Kegiatan)</strong> terlebih dahulu sebelum melakukan survei.
            </div>        
            <?php echo $message; ?>
            <div class="box box-info" <?php echo $style_link_survei;?>>
                <div class="box-header with-border">
                    <div class="box-title pull-left">
                        <a href="<?php echo $link_survei; ?>" class="btn btn-sm bg-fuchsia" style="<?php echo $style2;?>"><i class="fa fa-pencil"></i> Survei</a>                        
                    </div>
                    <div class="callout callout-info" style="margin-bottom: 0;<?php echo $style3;?>">
                        <h4><i class="icon fa fa-info"></i> Informasi!</h4>
                        <p>Anda sudah melakukan survei sebelumnya. Untuk survei ulang silakan gunakan tombol <strong>Survei Ulang</strong></p>
                    </div>                                        
                </div>
                <div class="box-body table-responsive">
                    <?php echo $jumlah;?>
                    <?php echo $table; ?>                    
                </div>
            </div>
        </div>
    </div>
</section>