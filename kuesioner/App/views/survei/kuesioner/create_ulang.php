<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php echo $message; ?>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <?php echo form_open('survei/kuesioner/submit'); ?>
                <input type="hidden" name="srv_skpd_id" value="<?php echo $skpd_id;?>">
                <input type="hidden" name="srv_tahun" value="<?php echo $tahun;?>">
                <div class="box-body">
                    <div class="callout callout-warning">
                        <h4>PERHATIAN!</h4>
                        <p>
                            Kuesioner hanya diisi sekali dan tidak dapat dirubah, dimohon untuk teliti dan cermat
                            <br>Survei ulang dilakukan jika ada tambahan kegiatan baru
                        </p>
                    </div>
                    <?php echo $tabel_survei;?>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>