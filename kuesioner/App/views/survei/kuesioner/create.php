<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php echo $message; ?>
            <div class="box box-primary">
                <div class="box-header">
<!--                    <h3 class="box-title"><i class="fa fa-plus-circle"></i> --><?php //echo $title_box; ?><!--</h3>-->
                </div>
                <?php echo form_open('survei/kuesioner/submit'); ?>
                <input type="hidden" name="srv_skpd_id" value="<?php echo $skpd_id;?>">
                <input type="hidden" name="srv_tahun" value="<?php echo $tahun;?>">
                <div class="box-body">
                    <div class="callout callout-warning">
                        <h4>PERHATIAN!</h4>
                        <p>Kuesioner hanya diisi sekali dan tidak dapat dirubah, dimohon untuk teliti dan cermat</p>
                    </div>
                    <?php echo $tabel_survei;?>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>