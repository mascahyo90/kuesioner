<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php echo $message; ?>
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-title pull-left">
                        <a href="<?php echo $link_tambah; ?>" class="btn btn-sm bg-fuchsia"><i class="fa fa-pencil"></i> Survei</a>
                    </div>                    
                </div>
                <div class="box-body table-responsive">
                    <p style="margin:2px">Jumlah Data: <b><?php echo $jumlah;?></b></p>
                    <?php echo $table; ?>
                </div>
            </div>
        </div>
    </div>
</section>
