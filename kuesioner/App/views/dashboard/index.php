<style>
    .callout.callout-info {
        background: #3c8dbc !important;
        border-color: #095785;
    }
</style>
<section class="content">
    <div class="row">
        <?php if($this->ion_auth->is_admin()):?>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>Manajemen</h3>
                        <p>SKPD</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person"></i>
                    </div>
                    <a href="<?php echo site_url('referensi/skpd');?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>Manajemen</h3>
                        <p>Kegiatan</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-clipboard"></i>
                    </div>
                    <a href="<?php echo site_url('referensi/kegiatan');?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>Manajemen</h3>
                        <p>Variabel</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-compose"></i>
                    </div>
                    <a href="<?php echo site_url('referensi/variabel');?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php endif;?>
        <?php if(!$this->ion_auth->is_admin()):?>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>Manajemen</h3>
                        <p>Kegiatan</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-clipboard"></i>
                    </div>
                    <a href="<?php echo site_url('referensi/kegiatan_skpd');?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php endif;?>
    </div>
    <div class="row">
    </div>
</section>