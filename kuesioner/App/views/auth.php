<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>APLIKASI SURVEI</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/v1/bo/css/AdminLTE.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .login-page {
                background-color: #00a7d0;
            }
            .login-box-msg {
                font-size: 20px;
            }
        </style>        
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-box-body">
                <div class="login-logo">
                    <div class="img img-responsive img-circle">
                        <img src="<?php echo base_url(); ?>Assets/v1/bo/img/logo.png" width="100px" height="100px">
                    </div>
                </div>
                <?php echo $title_konten; ?>            
                <div class="alert alert-danger alert-dismissable" <?php
                if (is_string($message)) {
                    echo 'style="display:block; margin-bottom:7px;"';
                } else {
                    echo 'style="display:none;"';
                }
                ?>>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                    <?php echo $message; ?>
                </div>                
                <?php echo $konten; ?>
            </div>
        </div>
        <script src="<?php echo base_url(); ?>Assets/v1/bo/js/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo base_url(); ?>Assets/v1/bo/js/bootstrap.min.js"></script>
    </body>
</html>
