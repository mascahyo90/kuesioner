<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <form action="<?php echo $link_filter; ?>" method="post">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Pencarian</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>SKPD</label>
                                    <input id="fil_nama" name="fil_nama" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm bg-purple"><i class="fa fa-search"></i> Cari</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">
            <?php echo $message; ?>
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-title pull-left">
                        <a href="<?php echo $link_tambah; ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
                    </div>                    
                </div>
                <div class="box-body table-responsive">
                    <p style="margin:2px">Jumlah Data: <b><?php echo $jumlah;?></b></p>
                    <?php echo $table; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal modal-danger fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                Yakin Ingin Menghapus Data Ini?
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm btn-danger hapus">Hapus</a>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#fil_nama").val("<?php echo $skpd; ?>");
        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.hapus').attr('href', $(e.relatedTarget).data('href'));
        });
    });
</script>
