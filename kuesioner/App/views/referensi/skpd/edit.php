<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissable" <?php
            if (is_string($message)) {
                echo 'style="display:block; margin-bottom:7px;"';
            } else {
                echo 'style="display:none;"';
            }
            ?>>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                <?php echo $message; ?>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-edit"></i> <?php echo $title_box; ?></h3>
                </div>
                <?php echo form_open(current_url()); ?>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>SKPD <sup style="color: red">*)</sup></label>
                                    <input type="text" name="skpd_nama" class="form-control" value="<?php if (!empty($update->skpd_nama)) {echo $update->skpd_nama;}?>">
                                </div>                            
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>