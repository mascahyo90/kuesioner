<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissable" <?php
            if (is_string($message)) {
                echo 'style="display:block; margin-bottom:7px;"';
            } else {
                echo 'style="display:none;"';
            }
            ?>>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                <?php echo $message; ?>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-edit"></i> <?php echo $title_box; ?></h3>
                </div>
                <?php echo form_open(current_url()); ?>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nama Kegiatan <sup style="color: red">*)</sup></label>
                                    <input type="text" name="keg_nama" class="form-control" value="<?php if (!empty($update->keg_nama)) {echo $update->keg_nama;}?>">
                                </div>
                                <div class="form-group">
                                    <label>Tahun <sup style="color: red">*)</sup></label>
                                    <input type="text" name="keg_tahun" class="form-control" value="<?php if (!empty($update->keg_tahun)) {echo $update->keg_tahun;}?>">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="keg_status_aktif" class="form-control edit_status">
                                        <option value="1">Aktif</option>
                                        <option value="0">Tidak Aktif</option>
                                    </select>                                
                                </div>                                                            
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>
<script>
    $(function() {
        $(".edit_status option").filter(function (index) {
            return $(this).val() == '<?php echo $update->keg_status_aktif; ?>';
        }).attr("selected", "selected"); 
    });
</script>